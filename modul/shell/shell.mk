# RT Shell files.
SHELLSRC = $(MODUL)/shell/shell.c \
           $(MODUL)/shell/shell_cmd.c

SHELLINC = $(MODUL)/shell

# Shared variables
ALLCSRC += $(SHELLSRC)
ALLINC  += $(SHELLINC)
