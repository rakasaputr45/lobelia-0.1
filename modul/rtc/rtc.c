#ifndef __RTC_C__
#define __RTC_C__

#include <string.h>
#include <stdlib.h>

#include "ch.h"
#include "hal.h"
#include "rtc.h"
#include "portconf.h"
static RTCDateTime timespec;
static time_t unix_time;
static time_t start_time;
struct tm *st_time;

/*
 * helper function
 */
time_t GetTimeUnixSec(void) {
  struct tm tim;
  rtcGetTime(&RTCD1, &timespec);
  rtcConvertDateTimeToStructTm(&timespec, &tim, NULL);
  return mktime(&tim);
}

/*
 * helper function
 */
void GetTimeTm(struct tm *timp) {
  rtcGetTime(&RTCD1, &timespec);
  rtcConvertDateTimeToStructTm(&timespec, timp, NULL);
}

/*
 * helper function
 */
void SetTimeUnixSec(time_t unix_time) {
  struct tm tim;
  struct tm *canary;

  /* If the conversion is successful the function returns a pointer
     to the object the result was written into.*/
  canary = localtime_r(&unix_time, &tim);
  osalDbgCheck(&tim == canary);

  rtcConvertStructTmToDateTime(&tim, 0, &timespec);
  rtcSetTime(&RTCD1, &timespec);
}

void start_uptime(void){
	start_time=GetTimeUnixSec();
	GetTimeTm(st_time);
}

int get_uptime(void){
	unix_time=GetTimeUnixSec();
	return unix_time-start_time;
	
}

int get_start_time(){
	return start_time;
}

void get_start_timetm(struct tm * stm){
	memcpy(stm ,st_time,sizeof(struct tm));
}

int interval_kirim_tanfan(int *n,int menit,int detik){
  struct tm *a;
	unsigned int wk=0;
	wk=GetTimeUnixSec();
	a = localtime ((const time_t *)&wk);
	// ss
  printf("\r\nsistem %d banding %d\r\n",(int ) n*menit,a->tm_min);
  if(*n>60){
   *n=(a->tm_min/menit)+1;
	  if(a->tm_min>=60-menit ){
		 *n=0;
	  }
     
  }
	// printf("\r\nsistem %d banding %d\r\n",*n*menit,a->tm_min);
  if(a->tm_min==(*n*menit ) && (a->tm_sec>=detik)){
    *n=*n+1;

		if(a->tm_min>=60-menit ){
			*n=0;
		}
    // printf("\r\nsistem %d banding %d\r\n",*n*menit,a->tm_min);
    return 1;
  }
// *n= *n + 6;
  // n++;
  return 0;
}

#endif
