#ifndef __RTCC_H__
#define __RTCC_H__

void GetTimeTm(struct tm *timp);
void start_uptime(void);
int get_uptime(void);
int get_start_time(void);
void get_start_timetm(struct tm * stm);
void SetTimeUnixSec(time_t unix_time);
time_t GetTimeUnixSec(void);
int interval_kirim_tanfan(int *n,int menit,int detik);
#endif
