#ifndef __MODEM_H__
#define __MODEM_H__


#define __bswap_constant_16(x) \
     ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8))

#define __bswap_constant_32(x) \
     ((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) |                      \
      (((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24))


void string2hexString(char *input, char *output);
void swapbytes(void *object, size_t size);
unsigned short swap_bytes(unsigned short x);
void modem_connect(bool debug);
void simpan_monita_gagal(void);
char modem_send(char *send, char *tampung, int timeout, bool debug);
char send_post_HTTP(char *send, int timeout, bool debug);
int  kirim_monita(bool debug); // 1 kirim data gagal 0 untuk data realtime
void kirim_monita_gagal(bool debug);
char modem_check_signal(bool debug);
bool modem_update_time(void);
void modem_init(void);
void modem_ping(bool debug);
void modem_reboot(bool debug);
char command(char *send);






#endif