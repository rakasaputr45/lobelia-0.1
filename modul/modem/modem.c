/*======================================================
 #HEADER
 header          :       ->  18
 jumlah ID data  : 26 * 2 -> 52
 enter           : 2 * 2 ->   4
-----------------------------------  +
 ukuran header           ->  74 byte

 #DATA 
 array data    : 26 * 4 -> 156
 enter         : 2      ->  2
 ---------------------------- +
 data                   -> 158 byte

 file 30 paket dalam 1 file jika 26 array data perngiriman
 total size file = header + (data * 30)  
                 = 74 + (158 * 1) // 1 = jumlah array data
                 = 232 byte / file data gagal
======================================================
 #HEADER
 header :        ->  18
 id     : 26 * 2 ->  52
 enter  : 2 * 2  ->   4
---------------------------  +
 header           74 byte

 #DATA 
 26 data: 26 * 4 -> 104
 enter  : 2      ->   2
 ----------------------------- +
 data            -> 106 byte

 file 30 paket dalam 1 file jika 26 array data
 total size file = header + (data * 30)  
                 = 74 + ( 106 * 30)
                 = 74 + 3120
                 = 3194 byte / file data gagal
======================================================*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ch.h"
#include "hal.h"
#include "shell/shell.h"
#include "modem.h"
#include "chprintf.h"
#include "app_shell.h"
#include "VG.h"
#include "portconf.h"
#include "time.h"
#include <locale.h>


char tempBufferPostRealtime[360];

#define HTTPREAD  "AT+QHTTPREAD=60\r\n"

char *strptime(const char *restrict s, const char *restrict format,
                      struct tm *restrict tm);

static const char enter[2] = "\r\n";
        
short lastHour;
const char *_useragent =            "PP";
const char *_AT =                   "AT";
const char *_OK =                   "OK";
const char *_ERROR  =            "ERROR";
const char *_CONNECT =         "CONNECT";
const char *_CME_ERROR =   "CME ERROR: ";
const char *_INET_PREFIX =           "I";
const char *_SSL_PREFIX =          "SSL";

static RTCDateTime timespec;
extern bool FILEBARU;

extern struct t_jwt *st_jwt;
extern struct t_env st_env;
extern struct flage st_flage;
extern struct t_flag st_flag;
extern struct t_data data[];
extern struct t_debug st_debug;
extern struct t_modem *st_modem;
extern struct t_monita_protokol st_monita_protokol;

short LENGheader = 0;
short LENGdata = 0;


bool           kirimGagal;          // Flag data Gagal
short          GAGAL = 0;
int            timezone;


unsigned char  DATAhex[230], REGIShex[110], SNhex[40];

void           initModem(void);
void           swapbytes(void *object, size_t size);
void           string2hexString(char *input, char *output);
void           modem_connect(bool debug);
char  modem_send(char *send, char *tampung, int timeout, bool debug);
char  send_post_HTTP(char *send, int timeout, bool debug);
char  checkSignal(bool debug);
unsigned short swapBytes(unsigned short x);

void modem_init(void)
{
    sdStart(&SD1, &MODEM_config); // modem config
    palClearLine(MODEM_RESET);
    chThdSleepMilliseconds(300);
    palSetLine(MODEM_RESET);
}

unsigned short swapBytes(unsigned short x)
{
  unsigned short bitmask = 0x00FF;
  unsigned short temp = x & bitmask;
  x = x >> 8;
  temp = temp << 8;
  x = x | temp;
  return x;
}

void swapbytes(void *object, size_t size)
{
  unsigned char *start, *end;
  for (start = object, end = start + size - 1; start < end; ++start, --end)
  {
    unsigned char swap = *start;
    *start = *end;
    *end   = swap;
  }
}

void string2hexString(char *input, char *output)
{
  int loop;
  int i;

  i = 0;
  loop = 0;

  while (input[loop] != '\0')
  {
    sprintf((char *)(output + i), "%02x", input[loop]);
    loop += 1;
    i += 2;
  }
  // insert NULL at the end of the output string
  output[i++] = '\0';
}

char modem_send(char *send, char *tampung, int timeout, bool debug)
{
  unsigned char x = 0;   // x false
  int y = 0;
  char cmd[100];
  memset(cmd, '\0', sizeof(cmd));
  printfModem("%s", send);
  while (true)
  {
    char c, st[2];
    if (sdReadTimeout(&SD1, (uint8_t *)&c, 1, timeout) == 0)
    {
      return x;
    }
    else
    {
      cmd[y++] = c;
      if (strstr(cmd, tampung) != NULL) x = 1;  
    }
  }
}

char command(char *send)
{
  unsigned char x = 0;   // x false
  printfModem("%s", send);
  while (true)
  {
    char c, st[2];
    if (sdReadTimeout(&SD1, (uint8_t *)&c, 1, 10000) == 0)   return x;
    else                                                     printf("%c", c);
  }
}

// AT+QPING=1,"google.com"
// AT+QIACT?
// AT+QIACT=1

void modem_ping(bool debug)
{
if (modem_send("AT+QPING=1,\"google.com\"\r\n", "\r\n", 5000, debug) == 0)  printf("Cek Signal\r\n"); 
if (modem_send("AT+CGACT?\r\n", "\r\n", 5000, debug) == 0)                  printf("Cek Signal\r\n");
}

void modem_reboot(bool debug)
{
   REBOOT = TRUE;
   printf("\r\nRebooting modem..........");
   palClearLine(MODEM_RESET);
   chThdSleepMilliseconds(300);
   palSetLine(MODEM_RESET);    
   delay(8000);
   modem_connect(1);
   printf("BEHASIL\r\n\r\n");
   REBOOT = FALSE;
}



void modem_connect(bool debug)
{
  if (modem_send("AT+QNWINFO\r\n", "+QNWINFO", 10000, debug) == 0)                         printf("Gagal chek signal\r\n");
  if (modem_send("ATI\r\n", "\r\n", 5000, debug) == 0)                                     printf("Cek Signal\r\n");
  if (modem_send("AT+QHTTPCFG=\"contextid\",1\r\n", "OK", 10000, debug) == 0)              printf("Failed to activate PDP context\r\n");
  if (modem_send("AT+QIACT=1\r\n", "OK", 30000, debug) == 0)                               printf("Failed to activate PDP context\r\n");
  if (modem_send("AT+QIACT?\r\n", "OK", 11000 , debug) == 0)                               printf(">> time out\r\n");
  if (modem_send("AT+COPS?\r\n", "\r\n", 5000, debug) == 0)                                printf("Cek Signal Gagal\r\n");
  if (modem_send("AT+CEREG?\r\n", "\r\n", 5000, debug) == 0)                               printf("Cek Signal Gagal\r\n");
  char URL[72], rekuesURL[50];
  sprintf(URL, "http://%s:%d/\r\n", st_monita_protokol.domain, st_monita_protokol.port);  
  sprintf(rekuesURL, "AT+QHTTPURL=%d,30\r\n", strlen(URL) - 2); 
  modem_send(rekuesURL, _CONNECT, 3000, debug);  
  modem_send(URL, _OK, 3000, debug);
  memset(URL, '\0', sizeof(URL));
  memset(rekuesURL, '\0', sizeof(rekuesURL));
  }


char send_post_HTTP(char *send, int timeout, bool debug)
{
  char x = 0;
  int y = 0;
  char cmd[200];
  memset(cmd, '\0', sizeof(cmd));
  if (debug)    printf(">> %s <<", send);
  printfModem("%s", send);
  while (true)
  {
    char c;
    if (sdReadTimeout(&SD1, (uint8_t *)&c, 1, timeout) == 0)
    {
      return x;
    }
    else
    {
      cmd[y++] = c;
        // printf("%c", c);

      // response from server
      if      (strstr(cmd, "200") != NULL) x = 1;
      else if (strstr(cmd, "404") != NULL) x = 2;
      else if (strstr(cmd, "500") != NULL) x = 3;
    }
  }
  return x;
}

char modem_check_signal(bool debug)
{
  unsigned char x = 0, c;
  int y = 0;
  printfModem("AT+CSQ\r\n");

    while (sdReadTimeout(&SD1, (uint8_t *)&c, 1, 10000) == 1)
    {
    }
        return x;
    }

//
    int kirim_monita(bool debug) // 1 kirim data gagal 0 untuk data realtime
    {
    unsigned int EPOCH,  EPOCHhex;
    EPOCH  = GetTimeUnixSec()-(GetTimeUnixSec()%intervalKirimRealtime);    // Epochtime 
    int SNLen = strlen(st_env.SN);
    char SN[(SNLen * 2) + 1];
    string2hexString(st_env.SN, SN);          // Konversi String to Hex untuk Serial Number
    sprintf(SNhex, "%s", SN);
    swapbytes(&EPOCH, sizeof(EPOCHhex));      // Swap DCBA epochtime

    short jumlahDATA = 0, dataLen = 0, regisLen = 0;

    for (int i = 0; i < JUMLAH_DATA_PERSUMBER * JUMLAH_SUMBER; i++)
    {
      if (data[i].status)    // Status data pengiriman monita setiap register data
      {
        int16_t register_data;
        register_data = __bswap_constant_16(data[i].id);
        regisLen += sprintf(REGIShex + regisLen, "%04x", register_data);
        dataLen  += sprintf(DATAhex + dataLen, "%04x%04x", data_reg[data[i].id - 1], data_reg[data[i].id]);
        jumlahDATA++;
      }
    }
      memset(tempBufferPostRealtime, '\0', sizeof(tempBufferPostRealtime));
      sprintf(tempBufferPostRealtime, "token=%s0d0a%s0d0a%08x%s", SNhex, REGIShex, EPOCH, DATAhex); // kirim data reltime
      char rekuesPOST[30]; 
      memset(rekuesPOST, '\0', sizeof(rekuesPOST));
      sprintf(rekuesPOST, "AT+QHTTPPOST=%d,60,60\r\n", strlen(tempBufferPostRealtime));
      modem_send(rekuesPOST, _CONNECT, 5000, debug);
      send_post_HTTP(tempBufferPostRealtime, 10000, debug);
      if (modem_send(HTTPREAD, "true", 10000, debug) == 1)
        {
            if(debug)   printf( "<%d> " CYAN("Post Monita : ") "BERHASIL\r\n" , (int)system_uptime);
            return 1;
        }
        else
        {
           if(debug)  printf( "<%d> " CYAN("Post Monita : ") RED("GAGAL\r\n"), (int)system_uptime);
          return 0;
        }
    }

    void kirim_monita_gagal(bool debug) // 1 kirim data gagal 0 untuk data realtime
    {
      char rekuesPOST[50]; 
      memset(rekuesPOST, '\0', sizeof(rekuesPOST));
      if(debug)  printf("kirim_monita_gagal : %s\r\n", folderMonitaGagal);
      sdcard_scan(folderMonitaGagal);
      if (fileGagal)
        {
          char bacaGGL[50];
          sprintf(bacaGGL, "%s%s", folderMonitaGagal, bacaSD);
          read_file(bacaGGL);

          if(debug) printf("<%d> Nama file \"%s\"\r\n", (int)system_uptime, bacaGGL);
          sprintf(rekuesPOST, "AT+QHTTPPOST=%d,60,60\r\n", strlen(sendBuffer));
          modem_send(rekuesPOST, _CONNECT, 6000, debug);
          send_post_HTTP(sendBuffer, 10000, debug);
          if (modem_send(HTTPREAD, "true", 10000, debug) == 1)  
          {
            remove_file(bacaGGL);
            if(debug)   printf( "<%d> " CYAN("Post Monita : ") "BERHASIL\r\n" , (int)system_uptime);
          }
          else            
           if(debug)  printf( "<%d> " CYAN("Post Monita : ") RED("GAGAL\r\n"), (int)system_uptime);
        }
      }

void simpan_monita_gagal(void)
{
unsigned char panjangData = 0;
unsigned char panjangHeader = 0;
int debugSD = 0;
char NAMAFOLDER[20], NAMAFILE[50];
unsigned char lenIsi = 0;
struct tm *waktu = NULL;
time_t saiki = GetTimeUnixSec();
uint32_t epoch = GetTimeUnixSec()-(GetTimeUnixSec()%intervalKirimRealtime);

printf("epoch %d\r\n", epoch);

waktu = localtime ((const time_t *)&saiki);
short menit =  ( waktu->tm_min / st_monita_protokol.interval) * st_monita_protokol.interval;	

memset(&NAMAFILE, '\0', sizeof(NAMAFILE));
memset(&NAMAFOLDER, '\0', sizeof(NAMAFOLDER));

	sprintf(NAMAFOLDER, "/MONITA");
	Create_Dir(NAMAFOLDER);
	sprintf(NAMAFILE, "%s/%4d%02d%02d-%02d%02d.str", NAMAFOLDER, waktu->tm_year + 1900, waktu->tm_mon + 1, waktu->tm_mday, waktu->tm_hour, menit);
	debugSD += Create_File(NAMAFILE);

	for (int i = 0; i < JUMLAH_DATA_PERSUMBER * JUMLAH_SUMBER; i++)
	{
		if (data[i].status)   panjangData += sizeof(int);
	}

	panjangHeader = strlen(st_env.SN) + 2 + (panjangData / 2) + 2;
	panjangData = sizeof(epoch) + panjangData + 2;
	char SDCisiGagalMonita[panjangData];

	if (FILEBARU == TRUE)
	{
		char SDCHeaderGagalMonita[panjangHeader];
		short lenHeader = 0;
		memset(&SDCHeaderGagalMonita, '\0', sizeof(SDCHeaderGagalMonita));
		memcpy(&SDCHeaderGagalMonita[lenHeader], &st_env.SN, strlen(st_env.SN));
		lenHeader += strlen(st_env.SN);
		memcpy(&SDCHeaderGagalMonita[lenHeader], &enter, 2);
		lenHeader += sizeof(short);

		for (int i = 0; i < JUMLAH_DATA_PERSUMBER * JUMLAH_SUMBER; i++)
		{
			if (data[i].status)
			{
				memcpy(&SDCHeaderGagalMonita[lenHeader], &data[i].id, sizeof(short));   lenHeader += sizeof(short);
			}
		}
		debugSD += Update_File(NAMAFILE, SDCHeaderGagalMonita, lenHeader);
	}
	memcpy(&SDCisiGagalMonita[lenIsi], &enter, 2);
	lenIsi += 2;
	memcpy(&SDCisiGagalMonita[lenIsi], &epoch, sizeof(float));
	lenIsi += sizeof(float);
	for (int i = 0; i < JUMLAH_DATA_PERSUMBER * JUMLAH_SUMBER; i++)
	{
		if (data[i].status)
		{
      uint16_t AB = __bswap_constant_16(data_reg[data[i].id + 1]);
      uint16_t CD = __bswap_constant_16(data_reg[data[i].id]);
			memcpy(&SDCisiGagalMonita[lenIsi], &AB, sizeof(short));
			lenIsi += sizeof(short);
			memcpy(&SDCisiGagalMonita[lenIsi], &CD, sizeof(short));
			lenIsi += sizeof(short);
		}
	}
	debugSD += Update_File(NAMAFILE, SDCisiGagalMonita, lenIsi);
  printf( "<%d> " CYAN("Simpan Monita Gagal : ") , (int)system_uptime);
	printf(" %s\r\n", debugSD ? "Gagal" : "Berhasil");
}

bool modem_update_time(void)
{
    bool x = 0;
    char c = 0;
    unsigned char y = 0;
    static char cmd[100];
    memset(cmd, '\0', sizeof(cmd));
    sprintf(cmd, "$$");
    printfModem("AT+QNTP=1,\"1.id.pool.ntp.org\",123,1\r\n");

    while (sdReadTimeout(&SD1, (uint8_t *)&c, 1, 20000) == 1)
    {
        cmd[y++] = c;
    }
    c = 0;
    char *token = NULL;
    const char delimiter[2] = "+";
    token = strtok(cmd, delimiter);

    unsigned char cnt_token;
    while (token != NULL)
    {
        if (cnt_token == 2)
        {
            sscanf(token, "%d\"", &timezone);   // printf("MASUKKKKKKKKKKKKKKKKKKKKK %d\r\n", timezone);
        }
        token = strtok(NULL, delimiter);
        cnt_token++;
    }
    timezone = timezone / 4;

    delay(1000);
    printfModem("AT+CCLK?\r\n");
    y = 0;
    while (sdReadTimeout(&SD1, (uint8_t *)&c, 1, 10000) == 1)
    {
        cmd[y++] = c;
    }
    c = 0;
    if (y == 41 || y == 39)
    {
        char *token = NULL;
        const char delimiter[2] = " ";
        token = strtok(cmd, delimiter);
        unsigned char cnt_token;

        while (token != NULL)
        {
            if (cnt_token == 1)
            {
                strptime(token, "\"%y/%m/%d,%H:%M:%S", &st_tm); // format date "22/10/21,08:55:13+28"
                rtcConvertStructTmToDateTime(&st_tm, 0, &timespec);
                rtcSetTime(&RTCD1, &timespec);
                printf("--- Kalibrasi waktu :");
                char buf[40] ;
                strftime(buf,50, "%Y-%m-%d %H:%M:%S %Z", &st_tm);
                printf(" %s \r\n", buf);
                lastHour = st_tm.tm_mday;
                return TRUE;
            }
            token = strtok(NULL, delimiter);
            cnt_token++;
        }
        y = cnt_token = 0;
    }
    return FALSE;
}
