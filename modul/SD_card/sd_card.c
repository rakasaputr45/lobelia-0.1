#include "sd_card.h"

char monitaRealtimeFolder[50];

#include "shell/shell.h"
#include "app_shell.h"
#include "modem.h"
#include <app_gsm.h>


extern bool FILEBARU;
static const char enter[2] = "\r\n";
short intervalKirimGagal = 60,  intervalKirimRealtime = 10;
/*
 * SDIO configuration.
 */
static const SDCConfig sdccfg = {
    SDC_MODE_4BIT};

static const char *mode[] = {"SDV11", "SDV20", "MMC", NULL};

static RTCDateTime timespec;
struct tm st_tm;         // struct time

short YEAR, MONTH, DAY, HOUR, MINUTE, SEC;  // simpan variabel waktu
char  dataLen, regisLen, jumlahSimpan;

void sdcard_init() {
	// high = true, low = false;
	static const char *mode[] = {"SDV11", "SDV20", "MMC", NULL};
	printf("--- Init SDCARD ---\r\n");
		sdcStart(&SDCD1, &sdccfg); // modem config
		chThdSleepMilliseconds(300);
		if (!blkIsInserted(&PORTAB_SDCD1))   {   printf("\r\n     GAGAL INIT SD DRIVER\r\n");   return;}
		if (sdcConnect(&PORTAB_SDCD1))       {   printf("\r\n     SDCARD TIDAK TERDETEKSI SOFTWARE\r\n");  return; }
		Mount_SD("/");
  printf("    OK\r\n\r\nCard Info\r\n");
  printf("    Mode     : %s\r\n", mode[SDCD1.cardmode & 3U]);
  printf("    Capacity : %D GB\r\n", SDCD1.capacity / 1024 / 2048);

}

void sdcard_simpan_realtime(void)
{
  struct tm *waktu;
  char NAMAFOLDER[20], NAMAFILE[50];
	unsigned char panjangData = 0, panjangHeader = 0, lenIsi = 0;
	int debugSD = 0;
	time_t saiki = GetTimeUnixSec();
	waktu = localtime((const time_t *)&saiki);
	short menit = (waktu->tm_min / 30) * 30;

  uint32_t epoch = GetTimeUnixSec()-(GetTimeUnixSec()%intervalKirimRealtime);
	sprintf(NAMAFOLDER, "%d%02d%02d", waktu->tm_year + 1900, waktu->tm_mon + 1, waktu->tm_mday);
	Create_Dir(NAMAFOLDER);
	sprintf(NAMAFILE, "%s/%4d%02d%02d-%02d%02d.str", NAMAFOLDER, waktu->tm_year + 1900, waktu->tm_mon + 1, waktu->tm_mday, waktu->tm_hour, menit);
	debugSD = Create_File(NAMAFILE);

	for (int i = 0; i < JUMLAH_DATA_PERSUMBER * JUMLAH_SUMBER; i++)
	{
		if (data[i].status)     panjangData += sizeof(int);
	}
	panjangHeader = strlen(st_env.SN) + 2 + (panjangData / 2) + 2;
	panjangData = sizeof(epoch) + panjangData + 2;
	char SDCIsiSimpan[panjangHeader];

	if (FILEBARU == TRUE)
	{
		char SDCHeaderSimpan[panjangHeader];
		short lenHeader = 0;
		memset(&SDCHeaderSimpan, '\0', sizeof(SDCHeaderSimpan));
		memcpy(&SDCHeaderSimpan[lenHeader], &st_env.SN, strlen(st_env.SN));
		lenHeader += strlen(st_env.SN);
		memcpy(&SDCHeaderSimpan[lenHeader], &enter, 2);
		lenHeader += sizeof(short);

		for (int i = 0; i < JUMLAH_DATA_PERSUMBER * JUMLAH_SUMBER; i++)
		{
			if (data[i].status)
			{
				memcpy(&SDCHeaderSimpan[lenHeader], &data[i].id, sizeof(short));
				lenHeader += sizeof(short);
			}
		}
		debugSD += Update_File(NAMAFILE, SDCHeaderSimpan, lenHeader);
	}

  memcpy(&SDCIsiSimpan[lenIsi], &enter, 2);                                            
  lenIsi += 2;

	memcpy(&SDCIsiSimpan[lenIsi], &epoch, sizeof(float));
	lenIsi += sizeof(float);

  for (int i = 0; i < JUMLAH_DATA_PERSUMBER * JUMLAH_SUMBER; i++)
  {
		if (data[i].status)
		{
      uint16_t AB = __bswap_constant_16(data_reg[data[i].id + 1]);
      uint16_t CD = __bswap_constant_16(data_reg[data[i].id]);

			memcpy(&SDCIsiSimpan[lenIsi], &AB, sizeof(short));
			lenIsi += sizeof(short);
			memcpy(&SDCIsiSimpan[lenIsi], &CD, sizeof(short));
			lenIsi += sizeof(short);
		}
  }

 debugSD += Update_File(NAMAFILE, SDCIsiSimpan, lenIsi);
 memset(&NAMAFILE, '\0', sizeof(NAMAFILE));
 memset(&SDCIsiSimpan, '\0', sizeof(panjangHeader));
 printf("<%d>" GREEN(" Simpan Rutin :") , (int)system_uptime);
 printf(" %s\r\n", debugSD ? RED("GAGAL") : "BERHASIL");
}

void sdcard_scan(char *file)
{
   
   Scan_Dir(file);
   if(fileGagal) printf("<%d> File gagal Monita : %d file\r\n", (int)system_uptime, fileGagal);
}


int sdcard_logging_modem(char *prokotol)
{
  int debugSD = 0;
  char NAMAFILE[77];
  time_t saiki = GetTimeUnixSec();
  struct tm *waktu = localtime((const time_t *)&saiki);
  short menit = waktu->tm_min%60;
  char pengiriman[200];
  memset(&NAMAFILE, '\0', sizeof(NAMAFILE));
  Create_Dir("/LOGGING_MODEM");  
  sprintf(NAMAFILE, "/LOGGING_MODEM/%4d%02d%02d-%02d%02d.str", waktu->tm_year+1900, waktu->tm_mon+1, waktu->tm_mday, waktu->tm_hour, menit); 
  sprintf(pengiriman, "<%4d/%02d/%02d-%02d:%02d %d> | %s\r\n",waktu->tm_year+1900, waktu->tm_mon+1, waktu->tm_mday, waktu->tm_hour, waktu->tm_min,system_uptime, prokotol);
  printf(pengiriman);
  debugSD += Create_File(NAMAFILE);
  debugSD += Update_File(NAMAFILE, pengiriman, strlen(pengiriman));
  return debugSD;
}

char sdcard_write_config()
{

   char *line; /* Line buffer */
   line = malloc(350);
   sprintf(line, "\n[sumber]\n");
   Create_File(NAMASETFILE);
   Update_File(NAMASETFILE, line, strlen(line));
   printf(line);
   for (unsigned char i = 0; i < JML_SUMBER_MB; i++)
   {
    sprintf(line,"flag = %d\nnama = %s\nmode = %d\nIP = %d.%d.%d.%d\nswap = %d\nstatus = %d\nport = %d\nregister_modul = %d\nregister_sumber = %d\nfc = %d\n",i,st_sumber[i].nama,st_sumber[i].mode,st_sumber[i].IP0,st_sumber[i].IP1,st_sumber[i].IP2,st_sumber[i].IP3,st_sumber[i].swap,st_sumber[i].status,st_sumber[i].port,st_sumber[i].RegDest,st_sumber[i].RegSrc,st_sumber[i].fc);
    Update_File(NAMASETFILE, line, strlen(line));
    printf(line);
    sprintf(line, "ID = %d\ntipe_data = %d\njumlah = %d\nberapa_bit = %d\nsandi1 = %d\nsandi2 = %d\n", st_sumber[i].ID,st_sumber[i].tipe_data,st_sumber[i].jmlReg,st_sumber[i].width,st_sumber[i].ky1,st_sumber[i].ky2);
    Update_File(NAMASETFILE, line, strlen(line));
    printf(line);
   }

   sprintf(line, "\n[data]\n");
   Update_File(NAMASETFILE,line, strlen(line));
   printf(line);
   for(unsigned char i=0;i<JUMLAH_SUMBER*JUMLAH_DATA_PERSUMBER;i++)
   {
   	sprintf(line,"flag = %d\nID = %d\nsatuan = %s\nnama = %s\nstatus = %d\nstatus_simpan = %d\ntipe = %d\npanjang = %d\n",i,data[i].id, data[i].satuan,data[i].nama,data[i].status, data[i].status_simpan, data[i].tipe, data[i].panjang);
    Update_File(NAMASETFILE, line, strlen(line));
    printf(line);

   }
   sprintf(line, "\n[kanal]\n");
   Update_File(NAMASETFILE, line, strlen(line));
   printf(line);
   for (unsigned char i = 0; i < JML_KANAL; i++)
   {
    int mm, cc;
    mm = st_kadc[i].m * 1000;
    cc = st_kadc[i].c * 1000;
    sprintf(line, "flag = %d\nm = %d\nc = %d\ntipe = %d\nstatus = %d\nky1 = %d\nky2 = %d\n", i, mm, cc, st_kadc[i].tipe, st_kadc[i].status, st_kadc[i].ky1, st_kadc[i].ky2);
    Update_File(NAMASETFILE, line, strlen(line));
    printf(line);
    } 

   sprintf(line,"\n[env]\n");
   Update_File(NAMASETFILE, line, strlen(line));
   printf(line);

   sprintf(line,"nama_board = %s\nSN = %s\nbaud_master = %d\nsimpan_file = %d\ninterval_simpan = %d\ndomain = %s\nport = %d\ntopik = %s\ninterval = %d\nstatus = %d\nmonita = %s\nport_m = %d\ninterval_m = %d\nstatus_m = %d\n", st_env.nama_board,st_env.SN, st_modbus_master.baud, st_memory.file,st_memory.interval,st_MQTT_protokol.domain, st_MQTT_protokol.port,st_MQTT_protokol.topik, st_MQTT_protokol.interval, st_MQTT_protokol.status, st_monita_protokol.domain, st_monita_protokol.port,st_monita_protokol.interval, st_monita_protokol.status);
   Update_File(NAMASETFILE, line, strlen(line));
    printf(line);

   free(line);
   return 0;
}