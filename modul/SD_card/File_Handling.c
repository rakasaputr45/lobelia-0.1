#include <File_Handling.h>
#include <ff.h>
#include "hal.h"
#include "VG.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "chprintf.h"
#include "portconf.h"
#include "VG.h"

bool FILEBARU;
char bacaSD[21], sendBuffer[BufferSend];
extern struct t_modbus_master st_modbus_master;
short sizeFile;
char flag_sg;
int nx;

short fileGagal;
extern struct t_env st_env;
extern struct t_monita_protokol st_monita_protokol;
extern struct t_MQTT_protokol   st_MQTT_protokol ;

extern struct t_memory st_memory;
extern struct t_sumber st_sumber[];
extern struct t_data data[];
extern struct t_adc st_kadc[];

/* =============================>>>>>>>> NO CHANGES AFTER THIS LINE =====================================>>>>>>> */

FATFS fs;  // file system
FIL fil; // File
FILINFO fno;
FRESULT fresult;  // result
UINT br, bw;  // File read/write count
BYTE www;

/**** capacity related *****/
FATFS *pfs;
DWORD fre_clust;
uint32_t total, free_space;


void Mount_SD (const TCHAR* path)
{
	fresult = f_mount(&fs, path, 0);    
}

void Unmount_SD (const TCHAR* path)
{
	fresult = f_mount(NULL, path, 0);
}

FRESULT Scan_Dir(char* pat){
    DIR dir;
	fileGagal = 0;
	fresult = f_opendir(&dir,pat); 
	while(1){
		f_readdir(&dir, &fno);
		if (fresult != FR_OK || fno.fname[0] == 0)             
		break;
		sprintf(bacaSD, "%s", fno.fname);
		fileGagal++;
	}
	return fresult;
}

FRESULT cek_file(char* pat){
    DIR dir;
	fileGagal = 0;
	printf("\r\n\r\n|---------------------------------------------------------|\r\n");	
	printf("| no |        name        | size | date       | time  |\r\n");	
	printf("------------------------------------------------------------\r\n");	

	fresult = f_opendir(&dir,pat); 
	while(1)
	{
	f_readdir(&dir, &fno);
	if (fresult != FR_OK || fno.fname[0] == 0)             break;
	printf("| %03d |%20s|", fileGagal + 1, fno.fname);
	printf(" %04d |", fno.fsize);
	printf(" %u/%02u/%02u | %02u:%02u |\r\n", (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31, fno.ftime >> 11, fno.ftime >> 5 & 63);
	sprintf(bacaSD, "%s", fno.fname);
	fileGagal++;
	sizeFile = fno.fsize;
	}
    printf(bacaSD);
	return fresult;
}


/* Only supports removing files from home directory */
FRESULT Format_SD (void)
{
    DIR dir;
    char *path = malloc(20 * sizeof (char));
    sprintf (path, "%s","/");

    fresult = f_opendir(&dir, path);                       /* Open the directory */
    if (fresult == FR_OK)
    {
        for (;;)
        {
            fresult = f_readdir(&dir, &fno);                   /* Read a directory item */
            if (fresult != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
            if (fno.fattrib & AM_DIR)     /* It is a directory */
            {
            	if (!(strcmp ("SYSTEM~1", fno.fname))) continue;
            	fresult = f_unlink(fno.fname);
            	if (fresult == FR_DENIED) continue;
            }
            else
            {   /* It is a file. */
               fresult = f_unlink(fno.fname);
            }
        }
        f_closedir(&dir);
    }
    free(path);
    return fresult;
}

FRESULT Write_File (char *name, char *data, int size)
{

	/**** check whether the file exists or not ****/
	fresult = f_stat (name, &fno);
	if (fresult != FR_OK)
	{
	    return fresult;
	}

	else
	{
	    /* Create a file with read write access and open it */
	    fresult = f_open(&fil, name, FA_OPEN_EXISTING | FA_WRITE);
	    if (fresult != FR_OK)       return fresult;
	    else
	    {
	    	fresult = f_write(&fil, data, size, &bw);
	    	fresult = f_close(&fil);
	    }
	    return fresult;
	}
}

FRESULT read_file (char *name)
{
	char bufx[2];
	short lenBuf = 0;
	/**** check whether the file exists or not ****/
	fresult = f_stat (name, &fno);
    if (fresult != FR_OK)        return fresult;
	else
	{
		/* Open file to read */
		fresult = f_open(&fil, name, FA_READ);
		if (fresult != FR_OK)    return fresult;
		char *buffer = chHeapAlloc(NULL, maxBufferRead);
		fresult = f_read (&fil, buffer, f_size(&fil), &br);
		if (fresult != FR_OK)           chHeapFree(buffer);
		else
		{
			memset(&sendBuffer, '\0', sizeof(sendBuffer));
			short lenBuf = sprintf(sendBuffer, "token=");
			for (int i = 0; i < br; i++)
			{
				lenBuf += sprintf(sendBuffer + lenBuf, "%02x", buffer[i]);
			}
			chHeapFree(buffer);
			fresult = f_close(&fil);
		}
		return fresult;
	}
}

FRESULT SDCReadKonfig()
{
	printf("Masuk Read_Konfigurasi\r\n");
	char line[50]; /* Line buffer */
	char bufx[2];
	/**** check whether the file exists or not ****/
	fresult = f_stat("/file_setting.txt", &fno);
	// printf("f_stat %d\r\n",fresult);
	if (fresult != FR_OK)       return fresult;
	else
	{
		/* Open file to read */
		fresult = f_open(&fil, "/file_setting.txt", FA_READ);
		// printf("f_open %d\r\n",fresult);
		if (fresult != FR_OK)   return fresult;
			bufx[1] = '\0';
			unsigned int ln = 0;
			while (1)
			{
				fresult = f_read(&fil, bufx, 1, &br);
				// printf("f_open %d\r\n",fresult);

				chThdSleepMilliseconds(1);
				if (bufx[0] == '\n')
				{
				cek_menu(line);
				memset(line, '\0', sizeof(line));
				// delay(1);
				}
				else                    { strcat(line, bufx);}
				if (br < 1) 	break; // sudah mencapai akhir file
			}
		        printf("buffer selesai\r\n");
		}
		return fresult;
	}

FRESULT Create_File (char *name)
{
	FILEBARU = 0;
	fresult = f_stat (name, &fno);
	if (fresult == FR_OK)       return fresult;
	else
	{
		fresult = f_open(&fil, name, FA_CREATE_ALWAYS|FA_READ|FA_WRITE);
				// printf("f_open %d\r\n", fresult);

		if (fresult != FR_OK)     return fresult;
		fresult = f_close(&fil);
	}
	FILEBARU = TRUE;
    return fresult;
}

FRESULT Update_File (char *name, char *data, int size)
{
	/**** check whether the file exists or not ****/
	fresult = f_stat (name, &fno);    // buka file    update

	 if (fresult == FR_NO_FILE) { 
	    fresult = f_open(&fil, name, FA_CREATE_ALWAYS|FA_READ|FA_WRITE);
		// printf("f_open %d\r\n", fresult);
		if (fresult != FR_OK)       return fresult;
	    fresult = f_close(&fil);	
	}
	else if (fresult == FR_OK) { 
	{
		 /* Create a file with read write access and open it */
	    fresult = f_open(&fil, name, FA_OPEN_APPEND | FA_WRITE);   // buka file dengan tulis
		// printf("f_open %d\r\n", fresult);
    	if (fresult != FR_OK)  return fresult;
	    // /* Writing text */
	    fresult = f_write(&fil, data, size, &bw);	
		// printf("f_write %d\r\n", fresult);	
	    /* Close file */
	    fresult = f_close(&fil);
	    // printf("f_close %d\r\n", fresult);	
	}
    return fresult;
  }
}

FRESULT remove_file (char *name)
{
	/**** check whether the file exists or not ****/
	fresult = f_stat (name, &fno);
	printf("f_stat %d\r\n", fresult);
	if (fresult != FR_OK)    return fresult;
	else   fresult = f_unlink (name);
    printf("<%d> Hapus File %s \"Berhasil\"\r\n",(int)system_uptime, name);
	printf(bacaSD);
	return fresult;
}

FRESULT Create_Dir (char *name)
{
    fresult = f_mkdir(name);
	return fresult;
}


int cek_menu(char *str){
	char *pch;
	pch=strchr(str,'[');
	if(pch!=NULL){  flag_parsing(str);  
	}
	else  
	 parsing_cek(str); 
	return 0;
}

int flag_parsing(char *str)
{
	char *pch;
	pch = (char *)strtok(str, "[]");
	if (pch != NULL)
	{
		if      (strcmp(pch, "sumber") == 0)    {  flag_sg = 1;  printf("\r\nada sumber");	}
		else if (strcmp(pch, "data") == 0)  	{  flag_sg = 2;  printf("\r\nada data"); }
		else if (strcmp(pch, "env") == 0)	    {  flag_sg = 3;  printf("\r\nada env"); }
		else if (strcmp(pch, "kanal") == 0)  	{  flag_sg = 4;  printf("\r\nada kanal");	}
		}
	else                    
	     printf("masalah pada flag_parsing();");
	return 0;
}

int parsing_cek(char *str)
{
	char *pch, parsingString[32];
	int parsingNumber;
    switch (flag_sg)
    {
    case SUMBER:
		printf(".");
		pch = strchr(str, '=') + 1;
		pch = strtok(pch, "\r\n");
		if (strstr(str, "flag =")) sscanf(str, " flag = %d", &nx);
		else if (strstr(str, "nama ="))
		{
			do      pch++;
		    while (*pch == ' ');
           strcpy(st_sumber[nx].nama, pch);
		}
		else if (strstr(str, "mode ="))            sscanf(str, "mode = %d", &st_sumber[nx].mode);
		else if (strstr(str, "swap ="))            sscanf(str, "swap = %d", &st_sumber[nx].swap); 
		else if (strstr(str, "status ="))          sscanf(str, "status = %d", &st_sumber[nx].status);   
		else if (strstr(str, "port ="))            sscanf(str, "port = %d", &st_sumber[nx].port);
		else if (strstr(str, "jumlah ="))          sscanf(str, "jumlah = %d", &st_sumber[nx].jmlReg);
		else if (strstr(str, "register_modul ="))  sscanf(str, "register_modul = %d", &st_sumber[nx].RegDest);
		else if (strstr(str, "register_sumber =")) sscanf(str, "register_sumber = %d", &st_sumber[nx].RegSrc);
		else if (strstr(str, "berapa_bit ="))      sscanf(str, "berapa_bit = %d", &st_sumber[nx].width); 
		else if (strstr(str, "fc ="))              sscanf(str, "fc = %d", &st_sumber[nx].fc); 
		else if (strstr(str, "ID ="))              sscanf(str, "ID = %d", &st_sumber[nx].ID);   
		else if (strstr(str, "tipe_data ="))       sscanf(str, "tipe_data = %d", &st_sumber[nx].tipe_data);
		else if (strstr(str, "sandi1 ="))          sscanf(str, "sandi1 = %d", &st_sumber[nx].ky1 );
		else if (strstr(str, "sandi2 ="))          sscanf(str, "sandi2 = %d", &st_sumber[nx].ky2);
		// else      printf("gagal parsing\r\n");
        break;
	case DATA:
		printf(".");
		pch = strchr(str, '=') + 1;
		pch = strtok(pch, "\r\n");
		if (strstr(str, "flag ="))                 sscanf(str, "flag = %d", &nx);
		else if (strstr(str, "ID ="))              sscanf(str, "ID = %d", &data[nx].id);
		else if (strstr(str, "satuan ="))
		{
			do 	pch++;
	        while (*pch == ' ');
			strcpy(data[nx].satuan, pch);
		}
		else if (strstr(str, "nama =") != 0)
		{
			do	pch++;
				 while (*pch == ' ');
			strcpy(data[nx].nama, pch);
		}
		else if (strstr(str, "status ="))         sscanf(str, "status = %d", &data[nx].status);
		else if (strstr(str, "status_simpan ="))  sscanf(str, "status_simpan = %d", &data[nx].status_simpan);
		else if (strstr(str, "tipe ="))           sscanf(str, "tipe = %d", &data[nx].tipe);
		else if (strstr(str, "panjang ="))        sscanf(str, "panjang = %d\r\n", &data[nx].panjang);
	break;
	
	    case KANAL:
		printf(".");
		pch = strchr(str, '=') + 1;
		pch = strtok(pch, "\r\n");
		if (strstr(str, "flag ="))
		{
			sscanf(str, "flag = %d", &nx);
		}
		else if (strstr(str, "m ="))    {        sscanf(str, "m = %d", &st_kadc[nx].m);  st_kadc[nx].m = st_kadc[nx].m / 1000.0f;}
		else if (strstr(str, "c ="))    {        sscanf(str, "c = %d", &st_kadc[nx].c);  st_kadc[nx].c = st_kadc[nx].c / 1000.0f;	}
		else if (strstr(str, "tipe ="))          sscanf(str, "tipe = %d", &st_kadc[nx].tipe);
		else if (strstr(str, "status ="))        sscanf(str, "status = %d", &st_kadc[nx].status);
		break;

	    case ENV:
		printf(".");
		pch = strchr(str, '=') + 1;
		pch = strtok(pch, "\r\n");
		if (strstr(str, "nama_board =") != 0)
		{
			do pch++;
		    while (*pch == ' ');
			strcpy(st_env.nama_board, pch);
		}
		else if (strstr(str, "SN =") != 0)
		{
			do   pch++;
		    while (*pch == ' ');
			strcpy(st_env.SN, pch);
		}
		else if (strstr(str, "baud_master ="))     sscanf(str, "baud_master = %d", &st_modbus_master.baud);
		else if (strstr(str, "simpan_file ="))     sscanf(str, "simpan_file = %d", &st_memory.file);
		else if (strstr(str, "interval_simpan =")) sscanf(str, "interval_simpan = %d", &st_memory.interval);
		else if (strstr(str, "domain ="))
		{
			do   pch++;
		    while (*pch == ' ');   strcpy(st_MQTT_protokol.domain, pch);
		}
		else if (strstr(str, "port ="))            sscanf(str, "port = %d", &st_MQTT_protokol.port);
		else if (strstr(str, "topik ="))
		{
			do   pch++;
		    while (*pch == ' ');                   strcpy(st_MQTT_protokol.domain, pch);
		}
		else if (strstr(str, "interval ="))
		{
			sscanf(str, "interval = %d", &st_MQTT_protokol.interval);
		}
	    else if (strstr(str, "status ="))          sscanf(str, "status = %d", &st_MQTT_protokol.status);
		else if (strstr(str, "monita ="))
		{
			do    pch++;
			while (*pch == ' ');         		   strcpy(st_monita_protokol.domain, pch);
		}
		else if (strstr(str, "port_m ="))          sscanf(str, "port_m = %d", &st_monita_protokol.port);
		else if (strstr(str, "interval_m ="))      sscanf(str, "interval_m = %d", &st_monita_protokol.interval); 
		else if (strstr(str, "status_m ="))        sscanf(str, "status_m = %d", &st_monita_protokol.status);
	}
	return 0;
}

unsigned int baca_ip(char *sIP)	{
	unsigned char byte = atoi(sIP);
	unsigned ip, i=2;
	char *pch;
	
	if (byte==0)	{
		printf("  ERROR parsing IP !!\r\n");
		return 0;
	}
	ip = byte<<24;	
	pch = strchr(sIP,'.');
	while (pch!=NULL)	{
		byte = atoi(pch+1);
		printf("pch: %s, %d, %d\r\n", pch+1, atoi(pch+1), byte);
		ip |= byte<<(i--*8);
		pch = strchr(pch+1,'.');
	}
	return ip;
}