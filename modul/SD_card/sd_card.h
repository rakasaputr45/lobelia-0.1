#ifndef _SD_CARD_H_
#define _SD_CARD_H_
// #include "sram.h"
#include "hal.h"
#include "ch.h"
#include "sd_card.h"
#include "portconf.h"
#include "ffconf.h"
#include "rtc.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ff.h"
#include "File_Handling.h"
#include "VG.h"
#include "ff.h"


#define intervalFileSimpan     10  // satuan menit / menyimpan file dalam 10 menit
#define intervalFileGagal      10  // satuan menit / menyimpan file dalam 30 menit

/* Buffer for block read/write operations, note that extra bytes are
   allocated in order to support unaligned operations.*/


extern struct t_memory st_memory;
extern struct t_sumber st_sumber[];
extern struct t_env st_env;
extern struct t_flag st_flag;
extern struct t_memory st_memory ;
extern struct t_monita_protokol st_monita_protokol;
extern struct t_mbrt st_mbrt;
extern struct t_modbus_master st_modbus_master;
extern struct t_MQTT_protokol   st_MQTT_protokol ;
extern struct t_data data[];
extern struct t_adc st_kadc[];


#define NAMASETFILE "file_setting.txt"
#define PORTAB_SDCD1 SDCD1
#if defined(STM32_SDC_SDIO_UNALIGNED_SUPPORT)
#define PORTAB_UNALIGNED_SUPPORT STM32_SDC_SDIO_UNALIGNED_SUPPORT

#elif defined(STM32_SDC_SDMMC_UNALIGNED_SUPPORT)
#define PORTAB_UNALIGNED_SUPPORT STM32_SDC_SDMMC_UNALIGNED_SUPPORT

#else
#error "unexpected definitions"
#endif
#define SDC_BURST_SIZE 16

void sdcard_init(void);
void sdcard_simpan_realtime(void);
void sdcard_scan(char *file);
char sdcard_write_config(void);
int  sdcard_logging_modem(char *prokotol);

extern short intervalKirimGagal,  intervalKirimRealtime;
#endif
