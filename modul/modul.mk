
#
#	23 Nov 2017, Afrendy Bayu
#
include $(CHIBIOS)/os/various/fatfs_bindings/fatfs.mk
include $(MODUL)/FreeModbus/modbus.mk
include $(MODUL)/shell/shell.mk

ifeq ($(USE_SMART_BUILD),yes)
	#APPCONF := $(strip $(shell cat $(APP)/app.h | egrep -e "\#define"))
	MODULCONF := $(strip $(shell cat $(MODUL)/modul.h | egrep -e "\#define"))
	APPCONF := $(strip $(shell cat $(CONFDIR)/appconf.h | egrep -e "\#define"))

	# List of all the board related files.
    MODULSRC += $(MODUL)/rtc/rtc.c
    MODULSRC += $(MODUL)/modem/modem.c
    MODULSRC += $(MODUL)/SD_card/sd_card.c 	
	MODULSRC += $(MODUL)/SD_card/File_Handling.c 	
    MODULSRC += $(MODUL)/formula/tinyexpr.c	
	MODULSRC += $(MODUL)/flash/flash.c
	MODULSRC += $(MODUL)/flash/stm32l4xx_hal_flash.c
	MODULSRC += $(MODUL)/flash/stm32l4xx_hal_flash_ex.c
	MODULSRC += $(MODUL)/flash/stm32l4xx_hal_flash_ramfunc.c	  			

	MODULSRC += $(MODBUSSRC) 

    # Required include directories
	MODULINC = $(MODUL)
	MODULINC += $(MODBUSINC)
	MODULINC += $(MODUL)/rtc
	MODULINC += $(MODUL)/modem	
	MODULINC += $(MODUL)/SD_card 	
	MODULINC += $(MODUL)/formula
	MODULINC += $(MODUL)/flash
endif