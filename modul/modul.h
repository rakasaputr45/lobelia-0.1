
#ifndef __MODUL_H__
#define __MODUL_H__

// 23 Nov 2017
// MODUL.H dipakai untuk checking requirement SYSTEM saja
#include "portconf.h"
#include "wdg.h"

 #define MODUL_USE_RELAY      TRUE
 #define MODUL_USE_BASE64     TRUE

#if !defined(MODUL_USE_AD7708) || defined(__DOXYGEN__)
//#include "ad7708/ad7708.h"
	
    #define MODUL_USE_AD7708    FALSE                  // MASIH RUSAK
#endif

#if (APP_USE_LWIP == TRUE) || defined(__DOXYGEN__)
    #define MODUL_USE_LWIP      FALSE
#endif
#if (APP_USE_FLASH == TRUE) || defined(__DOXYGEN__)
    #define MODUL_USE_SPI      TRUE
#endif
#if defined(APP_USE_HTTPD) && (APP_USE_HTTPD == TRUE) && (APP_USE_LWIP == FALSE)
    #error "Aplikasi HTTPD perlu mengkatifkan LWIP"
#endif

#if !defined(APP_USE_SHELL) || (APP_USE_SHELL == TRUE)
    #error "Aplikasi SHELL harus aktif"
#endif

#if defined(APP_USE_GPS) && (APP_USE_GPS == TRUE)
//    #error "Aplikasi HTTPD perlu mengkatifkan LWIP"
#endif

#if defined(APP_USE_RELAY) && (MODUL_USE_RELAY == FALSE)
           #error "Aplikasi HTTPD perlu mengkatifkan LWIP"
#endif

#endif