#include "ch.h"
#include "hal.h"
#include "VG.h"
#include "app.h"
#include "modul.h"
#include "gpio_int.h"

static virtual_timer_t time8;
extern struct t_adc    st_kadc[];

extern struct t_data data[];
extern uint16_t data_reg[];
extern int *timer_con;
extern int *conter;
struct gpio st_gpio;
int data_i[4];
int8_t RH_cek[4];

void counting_timer_S()
{
	for (int kintil = 0; kintil < JML_KANAL; kintil++)
	{
		if (st_kadc[kintil].tipe == 1 && !RH_cek[kintil])   data_i[kintil] = 0;
		if (st_kadc[kintil].status_R == 1 && RH_cek[kintil] == 1)
		{
			timer_con[kintil]++;    RH_cek[kintil] = 0;
		}
		else  RH_cek[kintil] = 0;
	}
	chVTSet(&time8, TIME_MS2I(1000), (void *)counting_timer_S, NULL);
}

void button_cb1(void *arg)
{
	(void)arg;

	chSysLockFromISR();
	if (st_kadc[4].status_H == 1 && st_kadc[4].tipe == 1)    st_gpio.flag[0] = 1;
	else if (st_kadc[0 + (JML_KANAL - 4)].tipe == 3)           conter[0]++;
	RH_cek[0] = 1;
	chSysUnlockFromISR();
}
void button_cb2(void *arg)
{
	(void)arg;

	chSysLockFromISR();
	if (st_kadc[5].status_H == 1 && st_kadc[5].tipe == 1)   st_gpio.flag[1] = 2;
	else if (st_kadc[1 + (JML_KANAL - 4)].tipe == 3)         conter[1]++;
	RH_cek[1] = 1;
	chSysUnlockFromISR();
}
void button_cb3(void *arg)
{
	(void)arg;

	chSysLockFromISR();
	if (st_kadc[6].status_H == 1 && st_kadc[6].tipe == 1)    st_gpio.flag[2] = 3;
	else if (st_kadc[2 + (JML_KANAL - 4)].tipe == 3)          conter[2]++;
	RH_cek[2] = 1;
	chSysUnlockFromISR();
}
void button_cb4(void *arg)
{
	(void)arg;

	chSysLockFromISR();
	if (st_kadc[7].status_H == 1 && st_kadc[7].tipe == 1)     st_gpio.flag[3] = 4;
	else if (st_kadc[3 + (JML_KANAL - 4)].tipe == 3)           conter[3]++;
	RH_cek[3] = 1;
	chSysUnlockFromISR();
}
void prin()
{
	printf("\r\ndata intrup 1 %d\r\n", data_i[0]);
	printf("\r\ndata intrup 2 %d\r\n", data_i[1]);
	printf("\r\ndata intrup 3 %d\r\n", data_i[2]);
	printf("\r\ndata intrup 4 %d\r\n", data_i[3]);
}

unsigned char i[4];
void init_gpio()
{ 
	// init sistem meaktifkan port intrup
	if (i[0] != 1 && st_kadc[4].status_H == 1 && (st_kadc[4].tipe == 1 || st_kadc[4].tipe == 3))
	{
		palEnableLineEvent(DI_1, PAL_EVENT_MODE_FALLING_EDGE);
		palSetLineCallback(DI_1, button_cb1, NULL);
		i[0] = 1;
	}
	else if (i[0] == 1 && st_kadc[4].status_H == 0 && (st_kadc[4].tipe == 1 || st_kadc[4].tipe == 3))
	{
		palDisableLineEvent(DI_1);
		i[0] = 0;
	}
	if (i[1] != 1 && st_kadc[5].status_H == 1 && (st_kadc[5].tipe == 1 || st_kadc[5].tipe == 3))
	{
		palEnableLineEvent(DI_2, PAL_EVENT_MODE_RISING_EDGE);
		palSetLineCallback(DI_2, button_cb2, NULL);
		i[1] = 1;
	}
	else if (i[1] == 1 && st_kadc[5].status_H == 0 && (st_kadc[5].tipe == 1 || st_kadc[5].tipe == 3))
	{
		palDisableLineEvent(DI_2);   i[1] = 0;
	}
	if (i[2] != 1 && st_kadc[6].status_H == 1 && (st_kadc[6].tipe == 1 || st_kadc[6].tipe == 3))
	{
		palEnableLineEvent(DI_3, PAL_EVENT_MODE_RISING_EDGE);
		palSetLineCallback(DI_3, button_cb3, NULL);
		i[2] = 1;
	}
	else if (i[2] == 1 && st_kadc[6].status_H == 0 && (st_kadc[6].tipe == 1 || st_kadc[6].tipe == 3))
	{
		palDisableLineEvent(DI_3);
		i[2] = 0;
	}
	if (i[3] != 1 && st_kadc[7].status_H == 1 && (st_kadc[7].tipe == 1 || st_kadc[7].tipe == 3))
	{
		palEnableLineEvent(DI_4, PAL_EVENT_MODE_RISING_EDGE);
		palSetLineCallback(DI_4, button_cb4, NULL);
		i[3] = 1;
	}
	else if (i[3] == 1 && st_kadc[7].status_H == 0 && (st_kadc[7].tipe == 1 || st_kadc[7].tipe == 3))
	{
		palDisableLineEvent(DI_4);
		i[3] = 0;
	}
}
void sistem_onoff()
{
	if (st_kadc[4].status_H == 1 && st_kadc[4].tipe == 2)
	{
		data_i[0] = !palReadLine(DI_1);
		if (data_i[0] == 1)    RH_cek[0] = 1;
	}
	if (st_kadc[5].status_H == 1 && st_kadc[5].tipe == 2)
	{
		data_i[1] = !palReadLine(DI_2);
		if (data_i[1] == 1)    RH_cek[1] = 1;
	}
	if (st_kadc[6].status_H == 1 && st_kadc[6].tipe == 2)
	{
		data_i[2] = !palReadLine(DI_3);
		if (data_i[2] == 1)   RH_cek[2] = 1;
	}
	if (st_kadc[7].status_H == 1 && st_kadc[7].tipe == 2)
	{
		data_i[3] = !palReadLine(DI_4);
		if (data_i[3] == 1)   RH_cek[3] = 1;
	}
}
void sistem_rpm()
{
	int data1[4];
	float B, C;
	if (st_gpio.flag[0] == 1)
	{
		if (st_gpio.flag_RPM[0] != 0)
		{
			data1[0] = st_gpio.flag_RPM[0];
			st_gpio.flag_RPM[0] = 1;
		}
		st_gpio.flag_RPM[0]++;
		if (st_gpio.flag_RPM[0] == 1)
		{
			data1[0] = port_timer_get_time();
			st_gpio.flag_RPM[0] = data1[0];
			st_gpio.flag[0] = 0;
		}
		else if (st_gpio.flag_RPM[0] == 2)
		{
			st_gpio.flag_RPM[0] = port_timer_get_time() - data1[0];
			B = st_gpio.flag_RPM[0];
			C = (float)1 / (B / 10000);
			st_gpio.flag_RPM[0] = (C)*60;
			data_i[0] = st_gpio.flag_RPM[0];
			st_gpio.flag_RPM[0] = 0;
			st_gpio.flag[0] = 0;
		}
	}

	if (st_gpio.flag[1] == 2)
	{
		if (st_gpio.flag_RPM[1] != 0)
		{
			data1[1] = st_gpio.flag_RPM[1];
			st_gpio.flag_RPM[1] = 1;
		}
		st_gpio.flag_RPM[1]++;
		if (st_gpio.flag_RPM[1] == 1)
		{
			data1[1] = port_timer_get_time();
			st_gpio.flag_RPM[1] = data1[1];
			st_gpio.flag[1] = 0;
		}
		else if (st_gpio.flag_RPM[1] == 2)
		{
			st_gpio.flag_RPM[1] = port_timer_get_time() - data1[1];
			B = st_gpio.flag_RPM[1];
			C = (float)1 / (B / 10000);
			st_gpio.flag_RPM[1] = (C)*60;
			data_i[1] = st_gpio.flag_RPM[1];
			st_gpio.flag_RPM[1] = 0;
			st_gpio.flag[1] = 0;
		}
	}
#if 1
	if (st_gpio.flag[2] == 3)
	{
		if (st_gpio.flag_RPM[2] != 0)
		{
			data1[2] = st_gpio.flag_RPM[2];     st_gpio.flag_RPM[2] = 1;
		}
		st_gpio.flag_RPM[2]++;

		if (st_gpio.flag_RPM[2] == 1)
		{
			data1[2] = port_timer_get_time();
			st_gpio.flag_RPM[2] = data1[2];
			st_gpio.flag[2] = 0;
		}
		else if (st_gpio.flag_RPM[2] == 2)
		{
			st_gpio.flag_RPM[2] = port_timer_get_time() - data1[2];
			B = st_gpio.flag_RPM[2];
			C = (float)1 / (B / 10000);
			st_gpio.flag_RPM[2] = (C)*60;
			data_i[2] = st_gpio.flag_RPM[2];
			st_gpio.flag_RPM[2] = 0;
			st_gpio.flag[2] = 0;
		}
	}
	if (st_gpio.flag[3] == 4)
	{
		if (st_gpio.flag_RPM[3] != 0)
		{
			data1[3] = st_gpio.flag_RPM[3];
			st_gpio.flag_RPM[3] = 1;
		}
		st_gpio.flag_RPM[3]++;

		if (st_gpio.flag_RPM[3] == 1)
		{
			data1[3] = port_timer_get_time();
			st_gpio.flag_RPM[3] = data1[3];
			st_gpio.flag[3] = 0;
		}
		else if (st_gpio.flag_RPM[3] == 2)
		{
			st_gpio.flag_RPM[3] = port_timer_get_time() - data1[3];
			B = st_gpio.flag_RPM[3];
			C = (float)1 / (B / 10000);
			st_gpio.flag_RPM[3] = (C)*60;
			data_i[3] = st_gpio.flag_RPM[3];
			st_gpio.flag_RPM[3] = 0;
			st_gpio.flag[3] = 0;
		}
	}
#endif

}

void sistem_conting()
{
	for (size_t i = 0; i < 4; i++)
	{
		if (st_kadc[i + (JML_KANAL - 4)].status_H == 1)
		{
			if (st_kadc[i + (JML_KANAL - 4)].tipe == 3)   memcpy(&data_reg[18 + (i * 2)], &conter[i], sizeof(int));
			else                                           data_reg[18 + (i * 2)] = data_i[i];
		}
	}
}

void timer()
{
	for (size_t i = 0; i < 4; i++)
	{
		if (st_kadc[4 + i].status_R == 1)
		{
			float convt = 0;
			convt = (float )timer_con[i] / 3600;
			memcpy(&data_reg[26 + (i * 2)], &convt, sizeof(float));
		}
	}
}

#if 0
void extHdlFunc()	{
	
		//chSysLockFromISR();
	if(GPIOA->IDR & GPIO_IDR_IDR_0 ) {
		
		TD[0]++;
		x=0;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR0;
	}
	//mendeteksi pada intrupasi GPIOC port C, IDR_6 port no 6.(JIKA DATA TRUE/ADA ISINYA(tidak null) maka memenuhi untuk masuk ke if)
	else if ((GPIOC->IDR & GPIO_IDR_IDR_6)==0) {
	 	TD[1]++;
		x=1;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		/* Clear bit */
		EXTI->PR = EXTI_PR_PR6;
	}
	
	else if ((GPIOF->IDR & GPIO_IDR_IDR_7)==0) {
		TD[2]++;
		x=2;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR7;
	}
	else if ((GPIOB->IDR & GPIO_IDR_IDR_8 )==0 ) {
			/* Clear bit */
		TD[3]++;
		x=3;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR8;
	}
	else if ((GPIOB->IDR & GPIO_IDR_IDR_9)==0 ) {
		TD[4]++;
		x=4;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
	 	B1[x] = port_timer_get_time()-A[x];
		palClearLine(LINE_LED1_RED);
		TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR9;
	}
	else if ((GPIOA->IDR & GPIO_IDR_IDR_11)==0 ) {
		TD[5]++;
		x=5;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR11;
	}
	else if ((GPIOA->IDR & GPIO_IDR_IDR_12)==0 ) {
		TD[6]++;
		x=6;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR12;
	}
	else if ((GPIOB->IDR & GPIO_IDR_IDR_14)==0 ) {
		TD[7]++;
		x=7;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR14;
	}
	else if ((GPIOB->IDR & GPIO_IDR_IDR_15)==0 ) {
		TD[8]++;
		x=8;
		if (TD[x]==1){
			A[x] = port_timer_get_time();
			palSetLine(LINE_LED1_RED);
			x=9;
		}
		else {
			B1[x] = port_timer_get_time()-A[x];
			palClearLine(LINE_LED1_RED);
			TD[x]=0;
		}
		EXTI->PR = EXTI_PR_PR15;
	}
	//chSysUnlockFromISR();
}
#endif

// perhitungan dalam intrupsi#
#if 0
void hitung(void){
    float B,C;
    if (st_gpio.cur_ch==1){
	    B=B1[st_gpio.cur_ch];
	    C=1/(B/10000);
		B1[st_gpio.cur_ch] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",st_gpio.cur_ch,B1[st_gpio.cur_ch]);
		st_gpio.cur_ch=0;
		
		
    }

    else if (x==1){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
    }
    else if (x==2){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
    }
    else if (x==3){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
    }
	else if (x==4){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
	}
	else if (x==5){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
	}
	else if (x==6){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
	}
	else if (x==7){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
	}
	else if (x==8){
		B=B1[x];
		C=1/(B/10000);
		A[x] = (C)*60;
		printf("--- cek saja %d = %d ...\r\n",x,A[x]);
		x=9;
		
	}

}
#endif
#if 0

void sistem_RPM (void){
//konfigurasi interupsi external
	int data1[10];
	float B,C;
		
	if(st_gpio.cur_ch==1){
		if(st_gpio.flag_int[st_gpio.cur_ch]!=0){
			data1[st_gpio.cur_ch]=st_gpio.flag_int[st_gpio.cur_ch];
			st_gpio.flag_int[st_gpio.cur_ch]=1;
			//printf("cek bro %d",data1[st_gpio.cur_ch]);
		}
		st_gpio.flag_int[st_gpio.cur_ch]++;
		
		if (st_gpio.flag_int[st_gpio.cur_ch]==1){
			data1[st_gpio.cur_ch] = port_timer_get_time();
			st_gpio.flag_int[st_gpio.cur_ch] = data1[st_gpio.cur_ch];
			//printf("cek banding bro %d",data1[st_gpio.cur_ch]);
			palSetLine(LINE_LED1_RED);
			st_gpio.cur_ch=0;
		}
		else if (st_gpio.flag_int[st_gpio.cur_ch]==2){
			st_gpio.flag_int[st_gpio.cur_ch] = port_timer_get_time()-data1[st_gpio.cur_ch];
			palClearLine(LINE_LED1_RED);
			B=st_gpio.flag_int[st_gpio.cur_ch];
			C=1/(B/10000);
			st_gpio.flag_int[st_gpio.cur_ch] = (C)*60;
			printf("--- cek saja %d = %d ...\r\n",st_gpio.cur_ch,st_gpio.flag_int[st_gpio.cur_ch]);
			st_gpio.flag_int[st_gpio.cur_ch]=0;
			st_gpio.cur_ch=0;
		}
		EXTI->PR = EXTI_PR_PR0;
	}
	
	if(st_gpio.cur_ch==2){
		if(st_gpio.flag_int[st_gpio.cur_ch]!=0){
			data1[st_gpio.cur_ch]=st_gpio.flag_int[st_gpio.cur_ch];
			st_gpio.flag_int[st_gpio.cur_ch]=1;
			//printf("cek bro %d",data1[st_gpio.cur_ch]);
		}
		st_gpio.flag_int[st_gpio.cur_ch]++;
		
		if (st_gpio.flag_int[st_gpio.cur_ch]==1){
			data1[st_gpio.cur_ch] = port_timer_get_time();
			st_gpio.flag_int[st_gpio.cur_ch] = data1[st_gpio.cur_ch];
			//printf("cek banding bro %d",data1[st_gpio.cur_ch]);
			palSetLine(LINE_LED1_RED);
			st_gpio.cur_ch=0;
		}
		else if (st_gpio.flag_int[st_gpio.cur_ch]==2){
			st_gpio.flag_int[st_gpio.cur_ch] = port_timer_get_time()-data1[st_gpio.cur_ch];
			palClearLine(LINE_LED1_RED);
			B=st_gpio.flag_int[st_gpio.cur_ch];
			C=1/(B/10000);
			st_gpio.flag_int[st_gpio.cur_ch] = (C)*60;
			printf("--- cek saja %d = %d ...\r\n",st_gpio.cur_ch,st_gpio.flag_int[st_gpio.cur_ch]);
			st_gpio.flag_int[st_gpio.cur_ch]=0;
			st_gpio.cur_ch=0;
		}
		EXTI->PR = EXTI_PR_PR0;
	}
	
	if(st_gpio.cur_ch==3){
		if(st_gpio.flag_int[st_gpio.cur_ch]!=0){
			data1[st_gpio.cur_ch]=st_gpio.flag_int[st_gpio.cur_ch];
			st_gpio.flag_int[st_gpio.cur_ch]=1;
			//printf("cek bro %d",data1[st_gpio.cur_ch]);
		}
		st_gpio.flag_int[st_gpio.cur_ch]++;
		
		if (st_gpio.flag_int[st_gpio.cur_ch]==1){
			data1[st_gpio.cur_ch] = port_timer_get_time();
			st_gpio.flag_int[st_gpio.cur_ch] = data1[st_gpio.cur_ch];
			//printf("cek banding bro %d",data1[st_gpio.cur_ch]);
			palSetLine(LINE_LED1_RED);
			st_gpio.cur_ch=0;
		}
		else if (st_gpio.flag_int[st_gpio.cur_ch]==2){
			st_gpio.flag_int[st_gpio.cur_ch] = port_timer_get_time()-data1[st_gpio.cur_ch];
			palClearLine(LINE_LED1_RED);
			B=st_gpio.flag_int[st_gpio.cur_ch];
			C=1/(B/10000);
			st_gpio.flag_int[st_gpio.cur_ch] = (C)*60;
			printf("--- cek saja %d = %d ...\r\n",st_gpio.cur_ch,st_gpio.flag_int[st_gpio.cur_ch]);
			st_gpio.flag_int[st_gpio.cur_ch]=0;
			st_gpio.cur_ch=0;
		}
		EXTI->PR = EXTI_PR_PR0;
	}
		
}
void init_gpio_value() {
	for(char i;i<10;i++)
		st_gpio.flag_int[i]=0;
	st_gpio.cur_ch=0;
}
#endif