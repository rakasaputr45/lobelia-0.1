
/**
  ***************************************************************************************************************
  ***************************************************************************************************************
  ***************************************************************************************************************
  File:	      FLASH_PAGE_F1.h
  Modifier:   ControllersTech.com
  Updated:    27th MAY 2021
  ***************************************************************************************************************
  Copyright (C) 2017 ControllersTech.com
  This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  of the GNU General Public License version 3 as published by the Free Software Foundation.
  This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  or indirectly by this software, read more about this on the GNU General Public License.
  ***************************************************************************************************************
*/
#ifndef __FLASH_H
#define __FLASH_H

#include "hal.h"
#include "ch.h"
#include "VG.h"
#include "string.h"
#define STM32F431xx
#include "stm32l4xx.h"
#include "portconf.h"

    #define __weak   __attribute__((weak))
    #define __packed __attribute__((__packed__))
#define HAL_FLASH_MODULE_ENABLED

#define     __IO    volatile

#define assert_param(expr) ((void)0U)

/** 
  * @brief  HAL Status structures definition  
*/  

typedef enum 
{
  HAL_OK       = 0x00U,
  HAL_ERROR    = 0x01U,
  HAL_BUSY     = 0x02U,
  HAL_TIMEOUT  = 0x03U
} HAL_StatusTypeDef;

/** 
  * @brief  HAL Lock structures definition  
  */
typedef enum 
{
  HAL_UNLOCKED = 0x00U,
  HAL_LOCKED   = 0x01U  
} HAL_LockTypeDef;

#define HAL_MAX_DELAY      0xFFFFFFFFU

#define __HAL_LOCK(__HANDLE__)                                           \
                                do{                                        \
                                    if((__HANDLE__)->Lock == HAL_LOCKED)   \
                                    {                                      \
                                       return HAL_BUSY;                    \
                                    }                                      \
                                    else                                   \
                                    {                                      \
                                       (__HANDLE__)->Lock = HAL_LOCKED;    \
                                    }                                      \
                                  }while (0U)

  #define __HAL_UNLOCK(__HANDLE__)                                          \
                                  do{                                       \
                                      (__HANDLE__)->Lock = HAL_UNLOCKED;    \
                                    }while (0U)

#include "stm32l4xx_hal_flash.h"
#include "stm32l4xx_hal_flash_ex.h"

uint32_t Flash_Write_Data (uint32_t StartPageAddress, uint64_t *Data, uint32_t len);
void Flash_Read_Data (uint32_t StartPageAddress, uint8_t *RxBuf, uint16_t len);
int erase_emb_flash(uint32_t Address, uint32_t Address_end);
void flash_write_config(void);
int flash_read_config(void);

#define ADDR_FLASH_PAGE_100   ((uint32_t)0x08032000) /* Base @ of Page 100, 2 Kbytes */
#define ADDR_FLASH_PAGE_101   ((uint32_t)0x08032800) /* Base @ of Page 101, 2 Kbytes */
#define ADDR_FLASH_PAGE_102   ((uint32_t)0x08033000) /* Base @ of Page 102, 2 Kbytes */

extern struct t_env             st_env; 
extern struct t_environment     st_environment;
extern struct t_monita_protokol st_monita_protokol;
extern struct t_MQTT_protokol   st_MQTT_protokol ;
extern struct t_modbus_master   st_modbus_master;
extern struct t_memory          st_memory ;
extern struct t_flag            st_flag ;

extern struct t_adc              st_kadc[]; 
extern struct t_data             data[];
extern struct t_sumber           st_sumber[];

#define FLASH_USER_START_ADDR   ADDR_FLASH_PAGE_100   /* Start @ of user Flash area */

#define FLASH_USER_END_ADDR     ADDR_FLASH_PAGE_102 + FLASH_PAGE_SIZE - 1   /* End @ of user Flash area */

#define ENV_FLASH_ADDR             ADDR_FLASH_PAGE_100
#define MQTT_FLASH_ADDR            ENV_FLASH_ADDR            + (sizeof(st_env))               + 2
#define MODBUS_FLASH_ADDR          MQTT_FLASH_ADDR           + (sizeof(st_MQTT_protokol))     + 8
#define MONITA_FLASH_ADDR          MODBUS_FLASH_ADDR         + (sizeof(st_modbus_master))     + 12

#define KANAL_FLASH_ADDR           ADDR_FLASH_PAGE_101
#define DATA_FLASH_ADDR            KANAL_FLASH_ADDR           + (sizeof(*st_kadc)*JML_KANAL) 
#define SUMBER_FLASH_ADDR          ((uint32_t)0x08032cd0) /* Base @ of Page 100, 2 Kbytes */
#define SUMBER_MEMORY_SD_ADDR      ADDR_FLASH_PAGE_102

#define UNUSED(X) (void)X      /* To avoid gcc/g++ warnings */
#endif