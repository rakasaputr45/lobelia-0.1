
/**
  ***************************************************************************************************************
  ***************************************************************************************************************
  ***************************************************************************************************************
  File:	      FLASH_PAGE_F1.c
  Modifier:   ControllersTech.com
  Updated:    27th MAY 2021
  ***************************************************************************************************************
  Copyright (C) 2017 ControllersTech.com
  This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  of the GNU General Public License version 3 as published by the Free Software Foundation.
  This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  or indirectly by this software, read more about this on the GNU General Public License.
  ***************************************************************************************************************
*/
#include "flash.h"
#include "stdio.h"
#include "VG.h"
#include "cmd_env.h"




extern struct t_memory st_memory;
/*Variable used to handle the Options Bytes*/
static FLASH_OBProgramInitTypeDef    OBInit; 

static uint32_t GetPage(uint32_t Addr)
{
  uint32_t page = 0;
  
  if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
  {
    /* Bank 1 */
    page = (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
  }
  else
  {
    /* Bank 2 */
    page = (Addr - (FLASH_BASE + FLASH_BANK_SIZE)) / FLASH_PAGE_SIZE;
  }
  
  return page;
}

uint8_t bytes_temp[4];


void float2Bytes(uint8_t * ftoa_bytes_temp,float float_variable)
{
    union {
      float a;
      uint8_t bytes[4];
    } thing;

    thing.a = float_variable;

    for (uint8_t i = 0; i < 4; i++) {
      ftoa_bytes_temp[i] = thing.bytes[i];
    }

}

float Bytes2float(uint8_t * ftoa_bytes_temp)
{
    union {
      float a;
      uint8_t bytes[4];
    } thing;

    for (uint8_t i = 0; i < 4; i++) {
    	thing.bytes[i] = ftoa_bytes_temp[i];
    }

   float float_variable =  thing.a;
   return float_variable;
}


int erase_emb_flash(uint32_t Address, uint32_t Address_end)
{
  static FLASH_EraseInitTypeDef EraseInitStruct;
  uint32_t FirstPage = 0, NbOfPages = 0, PAGEError = 0;
  __IO uint32_t MemoryProgramStatus = 0;

  FirstPage = GetPage(Address);
  NbOfPages = GetPage(Address_end);

  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.Banks = 1U;
  EraseInitStruct.Page = FirstPage;
  EraseInitStruct.NbPages = NbOfPages - FirstPage + 1;
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR);
  HAL_FLASH_Unlock();
  HAL_FLASH_OB_Unlock();
  HAL_FLASHEx_OBGetConfig(&OBInit);


  if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
  {
      printf("Gagal Menghapus Konfigurasi\r\n");
      return 1;
  }
    /* Lock the Options Bytes *************************************************/
    HAL_FLASH_Lock();
    printf("Menghapus Konfigurasi Berhasil\r\n");
}

uint32_t Flash_Write_Data(uint32_t FirstPageAddress, uint64_t *Data, uint32_t len)
{
    HAL_FLASH_Unlock();
    uint32_t Address = 0, StartPage = 0, EndPage = 0;
    uint32_t Address_end = 0;
    uint32_t MMS = *Data;
    Address = FirstPageAddress;
    Address_end = Address + len;
    EndPage = GetPage(FLASH_USER_START_ADDR);
    StartPage = GetPage(Address_end);
    	int index_ = 0;
    if (len <= 4)
    {
      while (Address < Address_end)
      {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, Address, MMS) == HAL_OK)
        {
          Address += 4;
          *Data++;
        }
        else
        {
          while (1)
          {
            return 1;
          }
        }
      }
    }
    else
    {
      while (Address < Address_end)
      {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, Address, Data[index_]) == HAL_OK)
        {
          Address += 8;
          index_ += 1;
        }
        else
        {
            return 1;
        }
      }
    }
    HAL_FLASH_Lock();
    return 0;
}

void Flash_Read_Data (uint32_t StartPageAddress, uint8_t *RxBuf, uint16_t len)
{
  uint32_t Address_end = StartPageAddress + len;
  uint32_t Address = StartPageAddress;
  int index_ = 0;
	while (Address < Address_end)
	{	
		RxBuf[index_] = *(volatile uint8_t *)Address;
		Address += 1;
		index_  = index_  + 1;   
	}
}

void flash_write_config(void)
{
  erase_emb_flash(FLASH_USER_START_ADDR, FLASH_USER_END_ADDR);
  HAL_FLASH_Unlock();
  HAL_FLASH_OB_Unlock();
  char cnnt;
  cnnt += Flash_Write_Data(ENV_FLASH_ADDR,(uint64_t *)&st_env, sizeof(st_env));
  cnnt += Flash_Write_Data(MQTT_FLASH_ADDR,(uint64_t *)&st_MQTT_protokol, sizeof(st_MQTT_protokol));
  cnnt += Flash_Write_Data(MODBUS_FLASH_ADDR,(uint64_t *)&st_modbus_master, sizeof(st_modbus_master));
  cnnt += Flash_Write_Data(MONITA_FLASH_ADDR,(uint64_t *)&st_monita_protokol, sizeof(st_monita_protokol));
  cnnt += Flash_Write_Data(KANAL_FLASH_ADDR, (uint64_t *)&st_kadc, sizeof(*st_kadc)*JML_KANAL);
  cnnt += Flash_Write_Data(DATA_FLASH_ADDR , (uint64_t *)&data, sizeof(*data)*JML_TITIK_DATA);
  cnnt += Flash_Write_Data(SUMBER_FLASH_ADDR , (uint64_t *)&st_sumber, sizeof(*st_sumber)*JML_SUMBER_MB);
  cnnt += Flash_Write_Data(SUMBER_MEMORY_SD_ADDR , (uint64_t *)&st_memory, sizeof(st_memory));

  HAL_FLASH_OB_Lock();
  HAL_FLASH_Lock();	
   if (cnnt == 0)   { printf("Menyimpan Konfigurasi Berhasil\r\n"); 	NVIC_SystemReset(); }
   else              printf("Gagal Menyimpan Konfigurasi\r\n");
}


int flash_read_config(void)
{
	//=======================================================================================
	//cek flashnya masih kosong atau udah isi
	//bisa juga dipakai kunci atau yang lain, tapi gini juga cukup
	//kalau flashnya kosong, isinya FF
	//kalau datanya kosong, isinya 00
	uint8_t cek_flash[4];
	Flash_Read_Data(FLASH_USER_START_ADDR, (uint8_t *)&cek_flash, sizeof(cek_flash));
	if(cek_flash[0] == 0xFF)
	{
		{
			printf("_____________________________________________________________\r\n");
			printf("flash masih kosong\r\n");
			printf("_____________________________________________________________\r\n");
		}
		return 1;
	}
    
  Flash_Read_Data(ENV_FLASH_ADDR,   (uint64_t *)&st_env, sizeof(st_env));
  Flash_Read_Data(MQTT_FLASH_ADDR,  (uint64_t *)&st_MQTT_protokol, sizeof(st_MQTT_protokol));
  Flash_Read_Data(MODBUS_FLASH_ADDR,(uint64_t *)&st_modbus_master, sizeof(st_modbus_master));
  Flash_Read_Data(MONITA_FLASH_ADDR,(uint64_t *)&st_monita_protokol, sizeof(st_monita_protokol));
  Flash_Read_Data(KANAL_FLASH_ADDR, (uint64_t *)&st_kadc, sizeof(*st_kadc)*JML_KANAL);
  Flash_Read_Data(SUMBER_FLASH_ADDR , (uint64_t *)&st_sumber, sizeof(*st_sumber)*JML_SUMBER_MB);
  Flash_Read_Data(DATA_FLASH_ADDR , (uint64_t *)&data, sizeof(*data)*JML_TITIK_DATA);
  Flash_Read_Data(SUMBER_MEMORY_SD_ADDR , (uint64_t *)&st_memory, sizeof(st_memory));
  return 0;
}