/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (C) 2013 Armink <armink.ztl@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * File: $Id: mbrtu_m.c,v 1.60 2013/08/20 11:18:10 Armink Add Master Functions $
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"
#include "VG.h"
#include "app_adc.h"


/* ----------------------- Modbus includes ----------------------------------*/

#include "mb.h"
#include "mb_m.h"
#include "mbconfig.h"
#include "mbframe.h"
#include "mbproto.h"
#include "mbfunc.h"
#include "portconf.h"
#include "mbport.h"
#if MB_MASTER_RTU_ENABLED == 1
#include "rtu/mbrtu.h"
#endif
#if MB_MASTER_ASCII_ENABLED == 1
#include "ascii/mbascii.h"
#endif
#if MB_MASTER_TCP_ENABLED == 1
#include "tcp/mbtcp.h"
#endif
// static MUTEX_DECL(Mutex1);
// extern int system_uptime;
#if MB_MASTER_RTU_ENABLED > 0 || MB_MASTER_ASCII_ENABLED > 0

#ifndef MB_PORT_HAS_CLOSE
#define MB_PORT_HAS_CLOSE 0
#endif
extern struct t_data data[JML_TITIK_DATA];

/* ----------------------- Static variables ------------------------------*/

static UCHAR    ucMBMasterDestAddress;
static BOOL     xMBRunInMasterMode = FALSE;
static eMBMasterErrorEventType eMBMasterCurErrorType;

static enum
{
    STATE_ENABLED,
    STATE_DISABLED,
    STATE_NOT_INITIALIZED
} eMBState = STATE_NOT_INITIALIZED;

extern struct t_sumber st_sumber[];
extern struct t_mbrt st_mbrt;

extern int data_i[JML_TITIK_DATA];

char sm;
static MUTEX_DECL(Mutex5);
/* Functions pointer which are initialized in eMBInit( ). Depending on the
 * mode (RTU or ASCII) the are set to the correct implementations.
 * Using for Modbus Master,Add by Armink 20130813
 */
static peMBFrameSend peMBMasterFrameSendCur;
static pvMBFrameStart pvMBMasterFrameStartCur;
static pvMBFrameStop pvMBMasterFrameStopCur;
static peMBFrameReceive peMBMasterFrameReceiveCur;
static pvMBFrameClose pvMBMasterFrameCloseCur;

/* Callback functions required by the porting layer. They are called when
 * an external event has happend which includes a timeout or the reception
 * or transmission of a character.
 * Using for Modbus Master,Add by Armink 20130813
 */
BOOL( *pxMBMasterFrameCBByteReceived ) ( void );
BOOL( *pxMBMasterFrameCBTransmitterEmpty ) ( void );
BOOL( *pxMBMasterPortCBTimerExpired ) ( void );

BOOL( *pxMBMasterFrameCBReceiveFSMCur ) ( void );
BOOL( *pxMBMasterFrameCBTransmitFSMCur ) ( void );

/* An array of Modbus functions handlers which associates Modbus function
 * codes with implementing functions.
 */
static xMBFunctionHandler xMasterFuncHandlers[MB_FUNC_HANDLERS_MAX] = {
#if MB_FUNC_OTHER_REP_SLAVEID_ENABLED > 0
	//TODO Add Master function define
    {MB_FUNC_OTHER_REPORT_SLAVEID, eMBFuncReportSlaveID},
#endif
#if MB_FUNC_READ_INPUT_ENABLED > 0
    {MB_FUNC_READ_INPUT_REGISTER, eMBMasterFuncReadInputRegister},
#endif
#if MB_FUNC_READ_HOLDING_ENABLED > 0
    {MB_FUNC_READ_HOLDING_REGISTER, eMBMasterFuncReadHoldingRegister},
#endif
#if MB_FUNC_WRITE_MULTIPLE_HOLDING_ENABLED > 0
    {MB_FUNC_WRITE_MULTIPLE_REGISTERS, eMBMasterFuncWriteMultipleHoldingRegister},
#endif
#if MB_FUNC_WRITE_HOLDING_ENABLED > 0
    {MB_FUNC_WRITE_REGISTER, eMBMasterFuncWriteHoldingRegister},
#endif
#if MB_FUNC_READWRITE_HOLDING_ENABLED > 0
    {MB_FUNC_READWRITE_MULTIPLE_REGISTERS, eMBMasterFuncReadWriteMultipleHoldingRegister},
#endif
#if MB_FUNC_READ_COILS_ENABLED > 0
    {MB_FUNC_READ_COILS, eMBMasterFuncReadCoils},
#endif
#if MB_FUNC_WRITE_COIL_ENABLED > 0
    {MB_FUNC_WRITE_SINGLE_COIL, eMBMasterFuncWriteCoil},
#endif
#if MB_FUNC_WRITE_MULTIPLE_COILS_ENABLED > 0
    {MB_FUNC_WRITE_MULTIPLE_COILS, eMBMasterFuncWriteMultipleCoils},
#endif
#if MB_FUNC_READ_DISCRETE_INPUTS_ENABLED > 0
    {MB_FUNC_READ_DISCRETE_INPUTS, eMBMasterFuncReadDiscreteInputs},
#endif
};

/* ----------------------- Start implementation -----------------------------*/
eMBErrorCode
eMBMasterInit( eMBMode eMode, UCHAR ucPort, ULONG ulBaudRate, eMBParity eParity )
{
    eMBErrorCode    eStatus = MB_ENOERR;

	switch (eMode)
	{
#if MB_MASTER_RTU_ENABLED > 0
	case MB_RTU:
		pvMBMasterFrameStartCur = eMBMasterRTUStart;
		pvMBMasterFrameStopCur = eMBMasterRTUStop;
		peMBMasterFrameSendCur = eMBMasterRTUSend;
		peMBMasterFrameReceiveCur = eMBMasterRTUReceive;
		pvMBMasterFrameCloseCur = MB_PORT_HAS_CLOSE ? vMBMasterPortClose : NULL;
		pxMBMasterFrameCBByteReceived = xMBMasterRTUReceiveFSM;
		pxMBMasterFrameCBTransmitterEmpty = xMBMasterRTUTransmitFSM;
		pxMBMasterPortCBTimerExpired = xMBMasterRTUTimerExpired;

		eStatus = eMBMasterRTUInit(ucPort, ulBaudRate, eParity);
		break;
#endif
#if MB_MASTER_ASCII_ENABLED > 0
		case MB_ASCII:
		pvMBMasterFrameStartCur = eMBMasterASCIIStart;
		pvMBMasterFrameStopCur = eMBMasterASCIIStop;
		peMBMasterFrameSendCur = eMBMasterASCIISend;
		peMBMasterFrameReceiveCur = eMBMasterASCIIReceive;
		pvMBMasterFrameCloseCur = MB_PORT_HAS_CLOSE ? vMBMasterPortClose : NULL;
		pxMBMasterFrameCBByteReceived = xMBMasterASCIIReceiveFSM;
		pxMBMasterFrameCBTransmitterEmpty = xMBMasterASCIITransmitFSM;
		pxMBMasterPortCBTimerExpired = xMBMasterASCIITimerT1SExpired;

		eStatus = eMBMasterASCIIInit(ucPort, ulBaudRate, eParity );
		break;
#endif
	default:
		eStatus = MB_EINVAL;
		break;
	}

	if (eStatus == MB_ENOERR)
	{
		if (!xMBMasterPortEventInit())
		{
			/* port dependent event module initalization failed. */
			eStatus = MB_EPORTERR;
		}
		else
		{
			eMBState = STATE_DISABLED;
		}
		/* initialize the OS resource for modbus master. */
		vMBMasterOsResInit();
	}
	return eStatus;
}

eMBErrorCode
eMBMasterClose( void )
{
    eMBErrorCode    eStatus = MB_ENOERR;

    if( eMBState == STATE_DISABLED )
    {
        if( pvMBMasterFrameCloseCur != NULL )
        {
            pvMBMasterFrameCloseCur(  );
        }
    }
    else
    {
        eStatus = MB_EILLSTATE;
    }
    return eStatus;
}

eMBErrorCode
eMBMasterEnable( void )
{
    eMBErrorCode    eStatus = MB_ENOERR;

    if( eMBState == STATE_DISABLED )
    {
        /* Activate the protocol stack. */
        pvMBMasterFrameStartCur(  );
        eMBState = STATE_ENABLED;
    }
    else
    {
        eStatus = MB_EILLSTATE;
    }
    return eStatus;
}

eMBErrorCode
eMBMasterDisable( void )
{
    eMBErrorCode    eStatus;

    if( eMBState == STATE_ENABLED )
    {
        pvMBMasterFrameStopCur(  );
        eMBState = STATE_DISABLED;
        eStatus = MB_ENOERR;
    }
    else if( eMBState == STATE_DISABLED )
    {
        eStatus = MB_ENOERR;
    }
    else
    {
        eStatus = MB_EILLSTATE;
    }
    return eStatus;
}

eMBErrorCode
eMBMasterPoll( void )
{
    static UCHAR   *ucMBFrame;
    static UCHAR    ucRcvAddress;
    static UCHAR    ucFunctionCode;
    static USHORT   usLength;
    static eMBException eException;

    int             i , j;
    eMBErrorCode    eStatus = MB_ENOERR;
    eMBMasterEventType    eEvent;
    eMBMasterErrorEventType errorType;

    /* Check if the protocol stack is ready. */
    if( eMBState != STATE_ENABLED )
    {
        return MB_EILLSTATE;
    }

    /* Check if there is a event available. If not return control to caller.
     * Otherwise we will handle the event. */
    if( xMBMasterPortEventGet( &eEvent ) == TRUE )
    {
		// printf("cek_son %d",eEvent);
        switch ( eEvent )
        {
        case EV_MASTER_READY:
            break;

        case EV_MASTER_FRAME_RECEIVED:
			eStatus = peMBMasterFrameReceiveCur( &ucRcvAddress, &ucMBFrame, &usLength );
			/* Check if the frame is for us. If not ,send an error process event. */
			// printf("cek_son %d",system_uptime);
			// system_uptime=0;
			if ( ( eStatus == MB_ENOERR ) && ( ucRcvAddress == ucMBMasterGetDestAddress() ) )
			{
					// printf("cek_son");
				( void ) xMBMasterPortEventPost( EV_MASTER_EXECUTE );
			}
			else
			{
				vMBMasterSetErrorType(EV_ERROR_RECEIVE_DATA);
				( void ) xMBMasterPortEventPost( EV_MASTER_ERROR_PROCESS );
			}
			break;

        case EV_MASTER_EXECUTE:
            ucFunctionCode = ucMBFrame[MB_PDU_FUNC_OFF];
            eException = MB_EX_ILLEGAL_FUNCTION;
			// for(int i=-1;i<=usLength+1;i++){
			// 	printf("%02X ",ucMBFrame[i]);

			// }
			// printf("\r\n");
            /* If receive frame has exception .The receive function code highest bit is 1.*/
            if(ucFunctionCode >> 7) {
            	eException = (eMBException)ucMBFrame[MB_PDU_DATA_OFF];
            }
			else
			{
				
				for (i = 0; i < MB_FUNC_HANDLERS_MAX; i++)
				{
					/* No more function handlers registered. Abort. */
					if (xMasterFuncHandlers[i].ucFunctionCode == 0)	{
						break;
					}
					else if (xMasterFuncHandlers[i].ucFunctionCode == ucFunctionCode) {
						vMBMasterSetCBRunInMasterMode(TRUE);
						/* If master request is broadcast,
						 * the master need execute function for all slave.
						 */
						if ( xMBMasterRequestIsBroadcast() ) {
							// printf("cek_son");
							usLength = usMBMasterGetPDUSndLength();
							for(j = 1; j <= MB_MASTER_TOTAL_SLAVE_NUM; j++)
							{
								vMBMasterSetDestAddress(j);
								eException = xMasterFuncHandlers[i].pxHandler(ucMBFrame, &usLength);
							}
						}
						else {
							// printf("cek_son1");
							eException = xMasterFuncHandlers[i].pxHandler(ucMBFrame, &usLength);
							simpan_data(ucMBFrame);
						}
						vMBMasterSetCBRunInMasterMode(FALSE);
						break;
					}
				}
			}
            /* If master has exception ,Master will send error process.Otherwise the Master is idle.*/
            if (eException != MB_EX_NONE) {
            	vMBMasterSetErrorType(EV_ERROR_EXECUTE_FUNCTION);
            	( void ) xMBMasterPortEventPost( EV_MASTER_ERROR_PROCESS );
            }
            else {
            	vMBMasterCBRequestSuccess( );
            	vMBMasterRunResRelease( );
            }
            break;

        case EV_MASTER_FRAME_SENT:
        	/* Master is busy now. */
			// printf("cek_son");
			// system_uptime=0;
        	vMBMasterGetPDUSndBuf( &ucMBFrame );
			eStatus = peMBMasterFrameSendCur( ucMBMasterGetDestAddress(), ucMBFrame, usMBMasterGetPDUSndLength() );
            break;

        case EV_MASTER_ERROR_PROCESS:
        	/* Execute specified error process callback function. */
			errorType = eMBMasterGetErrorType();
			vMBMasterGetPDUSndBuf( &ucMBFrame );
			switch (errorType) {
			case EV_ERROR_RESPOND_TIMEOUT:
				vMBMasterErrorCBRespondTimeout(ucMBMasterGetDestAddress(),
						ucMBFrame, usMBMasterGetPDUSndLength());
				break;
			case EV_ERROR_RECEIVE_DATA:
				vMBMasterErrorCBReceiveData(ucMBMasterGetDestAddress(),
						ucMBFrame, usMBMasterGetPDUSndLength());
				break;
			case EV_ERROR_EXECUTE_FUNCTION:
				vMBMasterErrorCBExecuteFunction(ucMBMasterGetDestAddress(),
						ucMBFrame, usMBMasterGetPDUSndLength());
				break;
			}
			vMBMasterRunResRelease();
        	break;
        }
    }
    return MB_ENOERR;
}

/* Get whether the Modbus Master is run in master mode.*/
BOOL xMBMasterGetCBRunInMasterMode( void )
{
	return xMBRunInMasterMode;
}
/* Set whether the Modbus Master is run in master mode.*/
void vMBMasterSetCBRunInMasterMode( BOOL IsMasterMode )
{
	xMBRunInMasterMode = IsMasterMode;
}
/* Get Modbus Master send destination address. */
UCHAR ucMBMasterGetDestAddress( void )
{
	return ucMBMasterDestAddress;
}
/* Set Modbus Master send destination address. */
void vMBMasterSetDestAddress( UCHAR Address )
{
	ucMBMasterDestAddress = Address;
}
/* Get Modbus Master current error event type. */
eMBMasterErrorEventType eMBMasterGetErrorType( void )
{
	return eMBMasterCurErrorType;
}
/* Set Modbus Master current error event type. */
void vMBMasterSetErrorType( eMBMasterErrorEventType errorType )
{
	eMBMasterCurErrorType = errorType;
}
/* simpan modbus kedata*/
void simpan_data(UCHAR *s){
	char jml,n;
	// int *ft;
	int tmpFl=0;
	unsigned char b;
	b=sm;
	uint8_t A, B, C, D;
	chMtxLock(&Mutex5);
	// for(char b=0;b<JML_SUMBER_MB;b++){
	if(st_mbrt.flage==0){
		if(st_sumber[b].fc==3||st_sumber[b].fc==4)   
		{
			if((st_sumber[b].status==1)&&(st_sumber[b].mode==0))
			{
				if(st_sumber[b].width==1){
					jml=s[1]/4;
					// printf("jumlah word %d           \r\n",jml);
				}else if(st_sumber[b].width==2){
					jml=s[1]/8;
					// printf("jumlah word %d           ",jml);
				}
				else{
					jml=s[1]/2;
				}
				n=st_sumber[b].RegDest-1000;
				char bbb  =st_sumber[b].RegDest;											
				for(char i=1;i<=jml;i++){
					if(st_sumber[b].width==1){//32 bit
						if(st_sumber[b].tipe_data==0)
						   {   //tipe data float
							float *fl;
							if(st_sumber[b].swap==3){ //swap word and byte
								tmpFl = ((s[(i*4+3)-2] & 0xFF)<<24) | ((s[(i*4+2)-2] & 0xFF)<<16) | ((s[(i*4+1)-2] & 0xFF)<<8) | (s[(i*4+0)-2] & 0xFF);
							}else if(st_sumber[b].swap==2){//swap word
								tmpFl = ((s[(i*4+2)-2] & 0xFF)<<24) | ((s[(i*4+3)-2] & 0xFF)<<16) | ((s[(i*4+0)-2] & 0xFF)<<8) | (s[(i*4+1)-2] & 0xFF);
							}else if(st_sumber[b].swap==1){//swap byte
								tmpFl = ((s[(i*4+1)-2] & 0xFF)<<24) | ((s[(i*4+0)-2] & 0xFF)<<16) | ((s[(i*4+3)-2] & 0xFF)<<8) | (s[(i*4+2)-2] & 0xFF);
							}else {//no swap
								tmpFl = ((s[(i*4+0)-2]& 0xFF) <<24) | ((s[(i*4+1)-2]& 0xFF) <<16) | ((s[(i*4+2)-2]& 0xFF) << 8 ) | (s[(i*4+3)-2] & 0xFF);
							}
							// printf("data  %d %d %d %d \r\n", (i*4+0)-2,(i*4+1)-2,(i*4+2)-2,(i*4+3)-2);
							// printf("data  %x %x %x %x \r\n",s[(i*4+0)-2],s[(i*4+1)-2],s[(i*4+2)-2],s[(i*4+3)-2]);
							
							int index = 0;
							index  = bbb - 1;
							data[index].tipe = 0;
                            data[index].panjang = 1;
							data[index].id = data[index - 1].id + 2;
							sumberConvertTo16bit(tmpFl, index);
							printf("float %08x index %d\r\n", &fl, bbb);

						}
						else{  //    int
							int *fl;
							if(st_sumber[b].swap==3){ //swap word and byte
								tmpFl = ((s[(i*4+3)-2] & 0xFF)<<24) | ((s[(i*4+2)-2] & 0xFF)<<16) | ((s[(i*4+1)-2] & 0xFF)<<8) | (s[(i*4+0)-2] & 0xFF);
							}else if(st_sumber[b].swap==2){//swap word
								tmpFl = ((s[(i*4+2)-2] & 0xFF)<<24) | ((s[(i*4+3)-2] & 0xFF)<<16) | ((s[(i*4+0)-2] & 0xFF)<<8) | (s[(i*4+1)-2] & 0xFF);
							}else if(st_sumber[b].swap==1){//swap byte
								tmpFl = ((s[(i*4+1)-2] & 0xFF)<<24) | ((s[(i*4+0)-2] & 0xFF)<<16) | ((s[(i*4+3)-2] & 0xFF)<<8) | (s[(i*4+2)-2] & 0xFF);
							}else {//no swap
								tmpFl = ((s[(i*4+0)-2] & 0xFF)<<24) | ((s[(i*4+1)-2] & 0xFF)<<16) | ((s[(i*4+2)-2] & 0xFF)<<8) | (s[(i*4+3)-2] & 0xFF);
							}
							int index = 0;
							index  = bbb - 1;
						    data[index].tipe = 1;
                            data[index].panjang = 1;
							sumberConvertTo16bit(tmpFl, index);
						}
					}else if(st_sumber[b].width==2){//64 bit
						if(st_sumber[b].tipe_data==0){//tipe data float
							float *fl;

							if(st_sumber[b].swap==3){ //swap word and byte
								tmpFl = ((s[(i*4+3)-2] & 0xFF)<<24) | ((s[(i*4+2)-2] & 0xFF)<<16) | ((s[(i*4+1)-2] & 0xFF)<<8) | (s[(i*4+0)-2] & 0xFF);
							}else if(st_sumber[b].swap==2){//swap word
								tmpFl = ((s[(i*4+2)-2] & 0xFF)<<24) | ((s[(i*4+3)-2] & 0xFF)<<16) | ((s[(i*4+0)-2] & 0xFF)<<8) | (s[(i*4+1)-2] & 0xFF);
							}else if(st_sumber[b].swap==1){//swap byte
								tmpFl = ((s[(i*4+1)-2] & 0xFF)<<24) | ((s[(i*4+0)-2] & 0xFF)<<16) | ((s[(i*4+3)-2] & 0xFF)<<8) | (s[(i*4+2)-2] & 0xFF);
							}else {//no swap
								tmpFl = ((s[(i*4+0)-2] & 0xFF)<<24) | ((s[(i*4+1)-2] & 0xFF)<<16) | ((s[(i*4+2)-2] & 0xFF)<<8) | (s[(i*4+3)-2] & 0xFF);
							}

							fl = (float *)&tmpFl;
							memcpy(&data_f[n-1],fl,sizeof(float));
							// printf("%x %x %x %x ",s[(i*4+0)-2],s[(i*4+1)-2],s[(i*4+2)-2],s[(i*4+3)-2]);
						}
						else{ //int
							int64_t *fl,tmpFll;
							if(st_sumber[b].swap==3){ //swap word and byte
								tmpFll = ((s[(i*8+3)-6] & 0xFF)<<56) | ((s[(i*8+2)-6] & 0xFF)<<48) | ((s[(i*8+1)-6] & 0xFF)<<40) | (s[(i*8+0)-6] & 0xFF)<<32|((s[(i*8+7)-6] & 0xFF)<<24) | ((s[(i*8+6)-6] & 0xFF)<<16) | ((s[(i*8+5)-6] & 0xFF)<<8) | (s[(i*8+4)-6] & 0xFF);
							}else if(st_sumber[b].swap==2){//swap word
								tmpFll = ((s[(i*8+4)-6] & 0xFF)<<56) | ((s[(i*8+5)-6] & 0xFF)<<48) | ((s[(i*8+6)-6] & 0xFF)<<40) | (s[(i*8+7)-6] & 0xFF)<<32|((s[(i*8+0)-6] & 0xFF)<<24) | ((s[(i*8+1)-6] & 0xFF)<<16) | ((s[(i*8+2)-6] & 0xFF)<<8) | (s[(i*8+3)-6] & 0xFF);
							}else if(st_sumber[b].swap==1){//swap byte
								tmpFll = ((s[(i*8+1)-6] & 0xFF)<<56) | ((s[(i*8+0)-6] & 0xFF)<<48) | ((s[(i*8+3)-6] & 0xFF)<<40) | (s[(i*8+2)-6] & 0xFF)<<32|((s[(i*8+5)-6] & 0xFF)<<24) | ((s[(i*8+4)-6] & 0xFF)<<16) | ((s[(i*8+7)-6] & 0xFF)<<8) | (s[(i*8+6)-6] & 0xFF);
							}else {//no swap
								tmpFll = ((s[(i*8+0)-6] & 0xFF)<<56) | ((s[(i*8+1)-6] & 0xFF)<<48) | ((s[(i*8+2)-6] & 0xFF)<<40) | (s[(i*8+3)-6] & 0xFF)<<32|((s[(i*8+4)-6] & 0xFF)<<24) | ((s[(i*8+5)-6] & 0xFF)<<16) | ((s[(i*8+6)-6] & 0xFF)<<8) | (s[(i*8+7)-6] & 0xFF);
							}
                            int index = 0;
							index  = bbb - 1;
						    data[index].tipe = 1;
                            data[index].panjang = 2;
							fl=(int64_t*)&tmpFll;
							// sumberConvertTo16bit(tmpFl, index);
							// data_int[n-1-JML_TITIK_DATA]=*fl;
							printf("64 bit %d   %16x", fl, fl);

						}
					}
					else
					{ //16 bit
						if(st_sumber[b].tipe_data==0)
						{//float
							float *fl;
							if(st_sumber[b].swap==1){//swap byte
								tmpFl = ((s[i*2+1] & 0xFF)<<8) | (s[i*2+0] & 0xFF) ;
							}
							else{//no swap
								tmpFl = ((s[i*2+0] & 0xFF)<<8) | (s[i*2+1] & 0xFF) ;
							}
							fl = (float *)&tmpFl;						
							memcpy(&data_f[n-1],fl,sizeof(float));
						}
						else
						{    // short data
							int *fl;
							if(st_sumber[b].swap==1){//swap byte
								tmpFl = ( (s[i*2+1] & 0xFF) <<8 ) | ( s[i*2+0] & 0xFF ) ;
							}
							else{//no swap
								tmpFl = ((s[i*2+0] & 0xFF) << 8) | ( s[i*2+1] & 0xFF ) ;
							}
							int index = 0;
							index  = bbb - 1;
						    data[index].tipe = 1;   //tipe data
                            data[index].panjang = 0;   // panjang data
							data[index + 1].id = data[index].id + 1;
							sumberConvertTo16bit(tmpFl, index);
						    // printf("NONTON SHort %08x %d index %d\r\n", tmpFl, tmpFl, bbb);
						}
					}
					n++;
				}
			}
		}else if(st_sumber[b].fc==1)
		{
			if((st_sumber[b].status==1)&&(st_sumber[b].mode==0))
			{
				int *fl;
				char buff[4];
				memset(buff,'\0',4);

				n=st_sumber[b].RegDest-1000;
				for(unsigned char i=0;i<s[1];i++){
					buff[i]=s[2+i];
					
				}
				for(unsigned char i=0;i<=s[1]/4;i++){
					tmpFl=((buff[i+3] & 0xFF)<<24) | ((buff[i+2] & 0xFF)<<16) | ((buff[i+1] & 0xFF)<<8) | (buff[i] & 0xFF);
					
				}
				fl=(int*)&tmpFl;
				data_f[n-1]=*fl;

			}
		}
	}else{
		printf("simpan file broo-----------");
		st_mbrt.flage=0;
	}
		chMtxUnlock(&Mutex5);
	// printf("simpan MBRTU\r\n");

}


#endif




// set_sumber 1 status 1
// set_sumber 2 status 1
// set_sumber 3 status 1
// set_sumber 4 status 1
// set_sumber 5 status 1
// set_sumber 6 status 1
// set_sumber 7 status 1
// set_sumber 8 status 1
// set_sumber 9 status 1
// set_sumber 10 status 1

// set_sumber 1 formula 10;1;3;1;10;0;1;0
// set_sumber 1 formula 10;1014;3;1;10;0;1;0
// set_sumber 2 formula 10;1014;3;2;11;0;1;1

// set_sumber 2 formula 10;1001;3;2;11;0;0;1

// set_sumber 2 formula 10;1002;3;2;11;0;0;1
// set_sumber 3 formula 10;1003;3;2;12;0;0;1
// set_sumber 4 formula 10;1004;3;2;13;0;0;1
// set_sumber 5 formula 10;1005;3;2;14;0;0;1
// set_sumber 6 formula 10;1006;3;2;15;0;0;1
// set_sumber 7 formula 10;1007;3;2;16;0;0;1
// set_sumber 8 formula 10;1008;3;2;17;0;0;1
// set_sumber 9 formula 10;1009;3;2;18;0;0;1
// set_sumber 10 formula 10;1010;3;2;19;;0;0;1
// set_sumber 10 formula 10;1010;3;2;19;0;0;1

// MB_voltage_PV
// set_data 11 nama MB_voltage_Batt