#include "app_blink.h"

#define max_uptime 100000000
float system_uptime;

static void led_on(virtual_timer_t *vtp, void *p);
static void led_off(virtual_timer_t *vtp, void *p);

static virtual_timer_t uptime;
static virtual_timer_t blink_led;

extern struct t_data data[JML_REGISTER];
extern struct t_flag st_flag;

static void uptime_cb(virtual_timer_t *vtp, void *p) {

  (void)vtp;
  (void)p; 
  system_uptime++;
  chVTSet(&uptime, TIME_MS2I(1000), uptime_cb, NULL);                   
}

static void led_off(virtual_timer_t *vtp, void *p) {

  (void)p;
    palClearLine(LED_SYSTEM);
    chVTSet(&blink_led, TIME_MS2I(100), led_on, NULL);
}


static void led_on(virtual_timer_t *vtp, void *p) {

  (void)p;
    palSetLine(LED_SYSTEM);
    chVTSet(&blink_led, TIME_MS2I(500), led_off, NULL);
}

static THD_WORKING_AREA(blinkThread, 2000);
static THD_FUNCTION(Threadblink, arg)
{
    (void)arg;
    chRegSetThreadName("blink");

    if (flash_read_config())
    {
      default_formula();
      default_env();
      default_kanal();
      default_data();
      default_alarm();
      default_sumber();
    }

    chVTSet(&uptime, TIME_MS2I(1000), uptime_cb, NULL);
    chVTSet(&blink_led, TIME_MS2I(100), led_off, NULL);
    while (true)
    {
    if (system_uptime > max_uptime) system_uptime = 0;  
       chThdSleepMilliseconds(1000);
    }
}

void thdblink(void)
{
    chThdCreateStatic(blinkThread, sizeof(blinkThread), NORMALPRIO, Threadblink, NULL);
}