#ifndef __APP_ALARM_H__
#define __APP_ALARM_H__

#include "hal.h"
#include "portconf.h"
#include <stdlib.h>
#include <stdio.h>
#include "VG.h"

void thdAlarm(void);
void alarm(int no);
#endif