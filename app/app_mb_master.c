#include "app_mb_master.h"

#define PORTAB_UART1 UARTD3

extern char sm;
extern struct t_modbus_master st_modbus_master;
extern struct t_sumber st_sumber[];
extern struct t_mbrt st_mbrt;

#define MB_PORT			1			                    	// NU
#define MB_MODE			MB_RTU			                // RTU mode only
#define MB_BITRATE	st_modbus_master.baud  			// bitrate
#define MB_PARITY		MB_PAR_NONE	// parity

static THD_WORKING_AREA(waThdModbus, 512);
#define MB_PRIO		(NORMALPRIO+1)	// Modbus process priority

USHORT  usModbusUserData[MB_PDU_SIZE_MAX];
UCHAR   ucModbusUserData[MB_PDU_SIZE_MAX];

unsigned char crc2[2], c[6], bufout[10];
int  w;
char rx;
char flg = 0, a = 0;
int  iI = 0, timer_mb, length = 8, leng = 8, coil_addr_relay = 1024, w3, w4;

#define USART_IDX_LAST (1) // only one usart

/*===========================================================================*/
/* Main and generic code.                                                    */
/*===========================================================================*/

/*
* The MODBUS master main thread
*/
static THD_WORKING_AREA(waThdTest, 1024);
static THD_FUNCTION(thdTest, arg) {
  (void)arg;

  eMBMasterReqErrCode    errorCode = MB_MRE_NO_ERR;
  uint16_t errorCount = 0;

  chRegSetThreadName("MBTest");

  while (true)
  {
    for(char i=0;i<JML_SUMBER_MB;i++){
      if(st_mbrt.flage==1){
        switch(st_mbrt.fc){
          case 5:
            errorCode = eMBMasterReqWriteCoil(st_mbrt.id_slave,st_mbrt.addr_sumber,st_mbrt.data,WAIT_FOREVER);
	          (void) ucMCoilBuf[0];
          break;
          default:
            chThdSleepMilliseconds(10);
        }
        chThdSleepMilliseconds(1000);
      }
      #if 1
      if((st_sumber[i].status==1)&&(st_sumber[i].mode==0)){
        sm=i;
        switch(st_sumber[i].fc){
          case 1:
            errorCode = eMBMasterReqReadCoils(st_sumber[i].ID,st_sumber[i].RegSrc,st_sumber[i].jmlReg,WAIT_FOREVER);
            (void) ucMCoilBuf[0];
          break;
          case 3:
            errorCode = eMBMasterReqReadHoldingRegister(st_sumber[i].ID,st_sumber[i].RegSrc,st_sumber[i].jmlReg,WAIT_FOREVER);
	          usMRegHoldBuf[0];
            // printf("masuk1");
          break;
          case 4:
            errorCode = eMBMasterReqReadInputRegister(st_sumber[i].ID,st_sumber[i].RegSrc,st_sumber[i].jmlReg,WAIT_FOREVER);
	          usMRegHoldBuf[0];
            // printf("masuk1");
          break;
          case 5:
           errorCode = eMBMasterReqWriteCoil(1,0,0xFF00,WAIT_FOREVER);
	          (void) ucMCoilBuf[0];
            // printf("masuk1");
          break;
          default:
            chThdSleepMilliseconds(500);
            // printf("formula %d tidak terdaftar",st_sumber[i].fc);
        }
        
	      if (errorCode != MB_MRE_NO_ERR) {
		      errorCount++;
	      }
        chThdSleepMilliseconds(1000);
      }
      #endif
      chThdSleepMilliseconds(1);
    }
    
  }
}

bool initModbus(void) {
  eMBErrorCode eStatus;
 
  eStatus = eMBMasterInit(MB_MODE, MB_PORT, MB_BITRATE, MB_PARITY);
  if (eStatus != MB_ENOERR) {
	  return false;
  }

  eStatus = eMBMasterEnable();
  if (eStatus != MB_ENOERR) {
	  return false;
  }

  return true;
}

static THD_FUNCTION(thdModbus, arg) {
  (void)arg;

  chRegSetThreadName("MODBUS");

  while (initModbus() != TRUE) {
	chThdSleepMilliseconds(1000);

	if (chThdShouldTerminateX())
	  goto cleanAndExit;
  }

  do {
    eMBMasterPoll();  
  } while (!chThdShouldTerminateX());
    
cleanAndExit:
  eMBMasterDisable();
  eMBMasterClose();
}


void modbus_init(void) {
  chThdCreateStatic(waThdModbus, sizeof(waThdModbus), LOWPRIO, thdModbus, NULL);
}

void halt(void){
	port_disable();
	while(TRUE)
	{
	}
}

void thdMBMaster(void){
      printf("\r\n--- Modbus Master Aplikasi\r\n");
    chThdCreateStatic(waThdTest, sizeof(waThdTest), LOWPRIO, thdTest, NULL);
}

