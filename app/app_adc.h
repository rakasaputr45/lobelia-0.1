#ifndef __APP_ADC_H__
#define __APP_ADC_H__

#include "ch.h"
#include "hal.h"
#include <stdio.h>
#include "chprintf.h"
#include "portconf.h"
#include "app_gsm.h"
#include "VG.h"


void thdADC();
void sumberConvertTo16bit(int *TempData, short n);
// void sumberConvert64To16bit(int64_t *TempData, short n);
void inputConvertTo16bit(short n);



#endif
