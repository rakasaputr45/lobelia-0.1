#include "app.h"

#define voltageChargingMax 12.65
#define PWMMax 970

float voltage_peak;
bool overvoltagebatt, peak;
int pwm_flag;
float PWMPrev;
bool cb, BR;

static void pwmpcb(PWMDriver *pwmp)
{
  (void)pwmp;
}

static void pwmc1cb(PWMDriver *pwmp)
{
  (void)pwmp;
}

static PWMConfig pwmcfg = {
    80000000, /* 80kHz PWM clock frequency.   */
    1000,     /* Initial PWM period .       */
    pwmpcb,
    {{PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb}, // Channel 1
     {PWM_OUTPUT_ACTIVE_LOW, pwmc1cb},  // // Channel 2
     {PWM_OUTPUT_DISABLED, NULL},
     {PWM_OUTPUT_DISABLED, NULL}},
    0,
    0,
    0};

void init_pwm(void)
{
  palSetLineMode(LINE_TIM2_CH1, PAL_MODE_ALTERNATE(1)); /* PWM2_channel 1    */
  palSetLineMode(LINE_TIM2_CH2, PAL_MODE_ALTERNATE(1)); /* PWM2_channel 2    */
  pwmStart(&PWMD2, &pwmcfg);
  chThdSleepMilliseconds(500); 
}

void thdPWM(void)
{
  init_pwm();
}


void chargingAlgorithm()
{
 if (BND == 1)   // battery nod connect
    {
    // printf(" ---->>> TRIGGER BMS : ON\r\n");
    PPWM = ((12.7) / voltageInput) * 1000;
    pwmEnableChannel(&PWMD2, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, PPWM * 10));
    pwmEnableChannel(&PWMD2, 1, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, PPWM * 10));
    buck_Enable(1);
    delay(6000);
    // printf(" ---->>> TRIGGER BMS : Proses %f - %f\r\n", voltageOutput, currentOutput);
    delay(56000);

    PPWM = 0;
    buck_Enable(0);
    pwmEnableChannel(&PWMD2, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, PPWM * 10));
    pwmEnableChannel(&PWMD2, 1, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, PPWM * 10));

    if     (currentOutput >= 0.5)    BND = 0;
    else                             BR  = 1;

    // printf(" ---->>> TRIGGER BMS : Selesai %f - %f\r\n", voltageOutput, currentOutput);
    }

  else if (ERR || REC)     
  { 
      // printf(" ---->>> CHARGING MODE : OFF\r\n");
      cb = true;
      PWM = 0;
      buck_Enable(0); // Disable buck before PPWM initialization
  }
  else  // Charging mode                            
  { 
    predictivePWM();
    PWM = Constrain(PWM, PPWM, PWMMax);
    //  printf(" ---->>> CHARGING MODE : ON\r\n");
    if (voltageOutput >= voltageChargingMax) 
        {
          overvoltagebatt = true;
          if (voltageOutput > voltage_peak) voltage_peak = voltageOutput;
          PWM = PWM - ((voltageOutput - voltageChargingMax) / (voltageInput / 1000));
        }
    else {
         overvoltagebatt = false;
        if (powerInput > powerInputPrev && voltageInput > voltageInputPrev)
        {
          PWM-- ; predict1 = 1; predict = 1;
        } //  ↑P ↑V ; →MPP  //D--
        else if (powerInput > powerInputPrev && voltageInput < voltageInputPrev)
        {
          PWM++; predict2 = 1; predict = 2;
        } //  ↑P ↓V ; MPP←  //D++
        else if (powerInput < powerInputPrev && voltageInput > voltageInputPrev)
        {
          PWM++; predict3 = 1; predict = 3;
        } //  ↓P ↑V ; MPP→  //D++
        else if (powerInput < powerInputPrev && voltageInput < voltageInputPrev)
        {
          PWM--; predict4 = 1; predict = 4;
        } //  ↓P ↓V ; ←MPP  //D--
    }
    if (PWM > PWMMax) PWM = PWMMax;    
    PWMPrev = PWM;
    powerInputPrev = powerInput; // Store Previous Recorded Power
    voltageInputPrev = voltageInput;
    voltageBattery = voltageOutput;
    pwmEnableChannel(&PWMD2, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, PWM * 10));
    pwmEnableChannel(&PWMD2, 1, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, PWM * 10));
    buck_Enable(1);
  }
}

void predictivePWM() // vin / 1000 = 20 / 1000 = 0.02 step  
{ // PREDICTIVE PWM ALGORITHM
      PPWM = ((voltageChargingMax + 0.1) / voltageInput) * 1000;
      cb = false;
  }


int Constrain(int au32_IN, int au32_MIN, int au32_MAX)
{
  if (au32_IN < au32_MIN)
  {
    return au32_MIN;
  }
  else if (au32_IN > au32_MAX)
  {
    return au32_MAX;
  }
  else
  {
    return au32_IN;
  }
}

void init_variable_MPPT()
{
  voltageInput = data_f[4];
  voltageOutput = data_f[5];
  currentInput = data_f[6];
  currentOutput = data_f[7];
  powerInput = voltageInput * currentInput ;
  powerOutput = currentOutput * voltageOutput;
}
