#ifndef __APP_SOPHIA_H__
#define __APP_SOPHIA_H__

#include "portconf.h"
#include "VG.h"
#include "string.h"
#include "ch.h"
#include "cmd.h"
#include "hal.h"
#include "stdlib.h"
#if APP_USE_SDC
#include "sd_card.h"
#endif
#define BESAR_PAKET         200

extern char simpan_config(void);

extern struct t_env                 st_env;
extern struct t_environment         st_environment;
extern struct t_ethernet 			st_ethernet;
extern struct t_monita_protokol 	st_monita_protokol;
extern struct t_jwt_protokol 		st_jwt_protokol;
extern struct t_onlimo_protokol 	st_onlimo_protokol;
extern struct t_modbusS             st_modbusS ;
extern struct t_modbus_master 		st_modbus_master;
extern struct t_memory 			    st_memory;
extern struct t_debug               st_debug;
extern struct t_modem               *st_modem;
extern struct t_data                data[];
extern struct t_sumber              st_sumber[];
extern struct t_adc                 st_kadc[];
extern struct t_formula             st_formula[];
extern uint8_t env_united[];
extern int env_united_ln;

//struk paket komunikasi panjangnya 226
struct t_kom_dat {
    char    kpl[10]; //header
    char    iden[6]; //data diterima
    char 	data[BESAR_PAKET]; //data
    char	cek[10];
};

typedef enum {
    ZZ,
    A0,
    A1,
    A2,
    A3,
    A4,
    A5,
    A6,
    B1,
    B2,
    B3,
    B4,
    B5,
    B6,
    C1,
    C2,
    C3,
    C4,
    C5,
    C6,
    C7,
    C8,
    C9,
    C10,
    C11,
    C12,
    C13,
    D1,
    D2,
    D3,
    D4,
    D5,
    D6,
    D7,
    E1,
    E2,
    E3,
    E4,
    E5,
    E6,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    G1,
    G2,
    G3,
    G4,
    G5,
    G6,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    I1,
    I2} token_t;

token_t lexer(const char *s);

void initUSB(void);
void thd_Sophia(void);

#endif