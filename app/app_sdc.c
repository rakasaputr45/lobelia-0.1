#include "app_sdc.h"

extern struct t_memory st_memory;

void thdsdc_int() {
    printf("--- Logger Monita\r\n");
    printf("    Status   : %s\r\n", st_memory.file? "Aktif" : "Mati");
    printf("    Interval : %d detik\r\n", st_memory.interval);
	// chThdCreateStatic(waThreadsdc, sizeof(waThreadsdc), LOWPRIO, Threadsdc, NULL);
}
