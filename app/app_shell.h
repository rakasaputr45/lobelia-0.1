
#ifndef __APP_SHELL_H__
#define __APP_SHELL_H__

#include "portconf.h"
#include "shell.h"
#include "ch.h"
#include "portconf.h"
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include "cmd.h"
#include "chprintf.h"


#define OFFSET              0   // argumen dari shell chibios mulai dari 0

void thdShell(void);
int printfShell (const char *fmt, ...);
// THD_FUNCTION(ThreadShell, arg);
void initShell(void) ;
void printInitShell(void);

// int growArray(ShellCommand **shell, int cur, int add);
void addAllCommandShell(void);
int addCmdShell(char* nama, void* cmd);
// int addCmdShell(ShellCommand cmd);

#endif
