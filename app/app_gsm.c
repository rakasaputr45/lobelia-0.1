#include <app_gsm.h>
#include <cmd_modem.h>


static void cb_interval(virtual_timer_t *vtp, void *p) {

  (void)vtp;
  (void)p; 

  if (monita_status)     FLAG_MONITA_KIRIM = TRUE;
  chVTSet(&interval_kirim, TIME_S2I(monita_interval), cb_interval, NULL);                   
}

static void cb_interval_gagal(virtual_timer_t *vtp, void *p) {

  (void)vtp;
  (void)p; 

  if (monita_gagal == 30) { FLAG_monita_gagal = TRUE; monita_gagal = 0; }
  monita_gagal++;
  chVTSet(&interval_simpan_gagal, TIME_S2I(2), cb_interval_gagal, NULL);                   
}




static THD_WORKING_AREA(gsmThread,2000);
static THD_FUNCTION(Thread_modem, arg)
{
  (void)arg;

  chRegSetThreadName("modem");
  
  char debug = st_flag.debug_gsm;
  static int modem_gagal_kirim = 0;
                  
  while (true)
  {

     if (monita_status)
      {
           if (modem_gagal_kirim == cnt_gagal)    {  modem_reboot(1);     modem_gagal_kirim = 0;   }
           if ((FLAG_MONITA_KIRIM) && (!REBOOT))
           {
             sdcard_simpan_realtime();
             if (kirim_monita(1))       {  modem_gagal_kirim = 0; kirim_monita_gagal(0);   }
             else                       {  modem_gagal_kirim++;  
             if (FLAG_monita_gagal)     simpan_monita_gagal(); 
             }
           }
           else  if (FLAG_monita_gagal)         simpan_monita_gagal();
          FLAG_monita_gagal = FLAG_MONITA_KIRIM = FALSE;
      }
    if (DAY != lastHour)  { modem_update_time();   DAY = lastHour; }   // update waktu
       delay(1000);
  }
}

void thdgsm(void)
{
  printf("--- Modem Aplikasi");
  modem_init();
  delay(9000);
    printf(": Selesai\r\n");
  modem_connect(1);
  modem_update_time();
  // sdcard_scan(folderMonitaGagal);
  chVTObjectInit(&interval_kirim);
  chVTObjectInit(&interval_simpan_gagal);

  chVTSet(&interval_kirim, TIME_S2I(monita_interval), cb_interval, NULL); 
  chVTSet(&interval_simpan_gagal, TIME_S2I(2), cb_interval_gagal, NULL); 
  chThdCreateStatic(gsmThread, sizeof(gsmThread), LOWPRIO, Thread_modem, NULL);
}
