/*
//-------------------------------------------------------------
    |  CURRENT OUTPUT           0.9983054963 X - 0.211490750172029  |
    |  CURRENT INPUT                    1,1  X + 0,147              |
    |  Hasil faktor pengali di kali -1                              |
    |  Sampling Rate = Frekuensi Timer / interval timer             |
    |                = 40.000.000 / 4.000                           |
    |                = 10.000 SPS                                   |
    -------------------------------------------------------------
*/
#include "app_adc.h"


bool flagARUSERROR;
#define FREQ_GPT 40000000       /* Frekuensi Timer External         */
#define DIV_FREQ_GPT 80000      /* Interval timer                   */
#define AVG_ADC 100              /* Jumlah data charging untuk rata  */
#define ADC_GRP1_NUM_CHANNELS 8 /* jumlah kanal                  */
#define ADC_GRP1_BUF_DEPTH 1
#define C 0.038975
#define M 1.01
#define temp_C 1365.0
#define temp_M 11.0

short n_ADC;
float data_f[9];
float temp_data_f[JML_TITIK_DATA];

extern struct t_data data[JML_TITIK_DATA];
extern struct t_adc st_kadc[];

static adcsample_t adc[ADC_GRP1_NUM_CHANNELS * ADC_GRP1_BUF_DEPTH];

uint16_t data_reg[JML_REGISTER];

void adccallback(ADCDriver *adcp)
{
    (void)adcp;
    /*  Kanal                                                     */
    temp_data_f[0] = adc[4]; /*  ADC Kanal 1               */
    temp_data_f[1] = adc[5]; /*  ADC Kanal 2               */
    temp_data_f[2] = adc[6]; /*  ADC Kanal 3               */
    temp_data_f[3] = adc[7]; /*  ADC Kanal 4               */
                             /*  Sensing data                                              */
    temp_data_f[4] = adc[2]; /*  ADC Voltage PV            */
    temp_data_f[5] = adc[0]; /*  ADC Voltage Batterry 12V  */
    temp_data_f[6] = adc[3]; /*  ADC Current Pv            */
    temp_data_f[7] = adc[1]; /*  ADC Current Battery 12v   */
    n_ADC++;
}
unsigned short swap_bytes1(unsigned short x)
{
    unsigned short bitmask = 0x00FF;
    unsigned short temp = x & bitmask;
    x = x >> 8;
    temp = temp << 8;
    x = x | temp;
    return x;
}

/*
 * GPT configuration.
 */
const GPTConfig portab_gptcfg1 = {
    .frequency = FREQ_GPT, /* 40MHz ADC clock frequency.   */
    .callback = NULL,
    .cr2 = TIM_CR2_MMS_1, /* MMS = 010 = TRGO on Update Event.    */
    .dier = 0U};

const ADCConfig portab_adccfg1 = {
    .difsel = 0U};

static const ADCConversionGroup adcgrpcfg1 = {
    .circular = true,
    .num_channels = ADC_GRP1_NUM_CHANNELS,
    .end_cb = adccallback,
    .error_cb = NULL,
    .cfgr = ADC_CFGR_EXTEN_RISING |
            ADC_CFGR_EXTSEL_SRC(13), /* TIM4_TRGO */
    .tr1 = ADC_TR_DISABLED,
    .tr2 = ADC_TR_DISABLED,
    .tr3 = ADC_TR_DISABLED,
    .awd2cr = 0U,
    .awd3cr = 0U,
    .smpr = {ADC_SMPR1_SMP_AN1(ADC_SMPR_SMP_24P5) | ADC_SMPR1_SMP_AN2(ADC_SMPR_SMP_24P5) | ADC_SMPR1_SMP_AN3(ADC_SMPR_SMP_24P5) | ADC_SMPR1_SMP_AN4(ADC_SMPR_SMP_24P5) |
                 ADC_SMPR1_SMP_AN5(ADC_SMPR_SMP_24P5) | ADC_SMPR1_SMP_AN6(ADC_SMPR_SMP_24P5) | ADC_SMPR1_SMP_AN9(ADC_SMPR_SMP_24P5),
             ADC_SMPR2_SMP_AN10(ADC_SMPR_SMP_24P5)},

    .sqr = {
        ADC_SQR1_SQ1_N(ADC_CHANNEL_IN1) | /*  ADC Kanal 1               */
        ADC_SQR1_SQ2_N(ADC_CHANNEL_IN2) | /*  ADC Kanal 2               */
        ADC_SQR1_SQ3_N(ADC_CHANNEL_IN3) | /*  ADC Kanal 3               */
        ADC_SQR1_SQ4_N(ADC_CHANNEL_IN4),  /*  ADC Kanal 4               */

        ADC_SQR2_SQ5_N(ADC_CHANNEL_IN5) | /*  ADC Voltage PV            */
        ADC_SQR2_SQ6_N(ADC_CHANNEL_IN6) | /*  ADC Voltage Batterry 12V  */
        ADC_SQR2_SQ7_N(ADC_CHANNEL_IN9) | /*  ADC Current Pv            */
        ADC_SQR2_SQ8_N(ADC_CHANNEL_IN10)  /*  ADC Voltage Battery 12v   */
    }};

static THD_WORKING_AREA(waADC, 700);
static THD_FUNCTION(Thread2, arg)
{
    (void)arg;
    chRegSetThreadName("ADC");
    while (true)
    {
        if (n_ADC >= AVG_ADC)
        {
            bool ERR = 0;
            for (int i = 0; i < JML_KANAL; i++) // kanal 1 - 4
            data_f[i] = M * (temp_M * temp_data_f[i] / temp_C) + C;                                      // voltage output            
            data_f[4] = 1.01 * (temp_data_f[4] * 14.3 / 1365) + 0.456;                                   // voltage input
            data_f[5] = 1.02 * (temp_data_f[5] * 15.62 / 3003) + 0.0692;                                 // voltage output
            data_f[6] = -1 * ((0.9983054963 * ((temp_data_f[6] - 2047.5) / 163.8)) - 0.211490750172029); // current input
            data_f[7] = -1 * ((1.1 * ((2047.5 - temp_data_f[7]) / 163.8)) + 0.145);                      // current output
            data_f[8] = system_uptime;
            formulaADC();
            n_ADC = 0;
        }
        chThdSleepMilliseconds(50);
    }
}

void thdADC(void)
{
    adcStart(&ADCD1, &portab_adccfg1);
    gptStart(&GPTD6, &portab_gptcfg1);
    adcStartConversion(&ADCD1, &adcgrpcfg1, adc, ADC_GRP1_BUF_DEPTH);
    gptStartContinuous(&GPTD6, DIV_FREQ_GPT);
    chThdCreateStatic(waADC, sizeof(waADC), LOWPRIO, Thread2, NULL);
}
void formulaADC()
{
    for (int i = 0; i < JML_DATA_INPUT; i++)
    {
        if ((st_kadc[i].status) && (i < JML_KANAL))  data_f[i] = data_f[i] * st_kadc[i].m + st_kadc[i].c; // y = Mx + C
        inputConvertTo16bit(i);                                                                           // konversi data adc (float) to hex
    }
    for (int i = 9; i < JML_TITIK_DATA; i++)
    {
        if (data[i].panjang == 0)
            data[i + 1].id = data[i].id + 1;
        else if (data[i].panjang == 1)
            data[i + 1].id = data[i].id + 2;
        else if (data[i].panjang == 2)
            data[i + 1].id = data[i].id + 4;
    }
}

/* DEPRECATED - Menyimpan data float ke 2 unsigned short */
void sumberConvertTo16bit(int *TempData, short n)
{
    short regbank = data[n].id;
    char type = data[n].tipe;
    char width = data[n].panjang;

    if ((width == 1) && (type == 1))
    {
        uint32_t i;
        memcpy(&i, &TempData, sizeof(uint32_t));
        data_reg[regbank - 1] = (uint16_t)i;
        data_reg[regbank] = (uint16_t)(i >> 16);
        data_reg[regbank - 1] = swap_bytes1(data_reg[regbank - 1]);
        data_reg[regbank] = swap_bytes1(data_reg[regbank]);
    }
    else if ((width == 0) && (type == 1))
    {
        uint32_t i;
        memcpy(&i, &TempData, sizeof(uint32_t));
        data_reg[regbank - 1] = (uint16_t)i;
    }
}

/* DEPRECATED - Menyimpan data float ke 2 unsigned short */
void inputConvertTo16bit(short n)
{
    uint32_t i;
    short regbank = data[n].id;
    memcpy(&i, &data_f[n], sizeof(uint32_t));
    data_reg[regbank - 1] = (uint16_t)i;
    data_reg[regbank] = (uint16_t)(i >> 16);
    data_reg[regbank - 1] = swap_bytes1(data_reg[regbank - 1]);
    data_reg[regbank] = swap_bytes1(data_reg[regbank]);
}


