
ifeq ($(USE_SMART_BUILD),yes)
	APPCONF := $(strip $(shell cat cfg/appconf.h | egrep -e "\#define"))

	# List of all the board related files.

	# Required include directories

	APPINC = $(APP)

	ifneq ($(findstring APP_USE_BLINK TRUE,$(APPCONF)),)
		APPSRC += $(APP)/app_blink.c
	endif	
	
	ifneq ($(findstring APP_USE_SHELL TRUE,$(APPCONF)),)
		APPSRC += $(APP)/app_shell.c
	endif
	
	ifneq ($(findstring APP_USE_ADC TRUE,$(APPCONF)),)
		APPSRC += $(APP)/app_adc.c
	endif
	
	ifneq ($(findstring APP_USE_PWM TRUE,$(APPCONF)),)
	    APPSRC += $(APP)/app_pwm.c	
	endif
	
	ifneq ($(findstring APP_USE_MPPT TRUE,$(APPCONF)),)
	APPSRC += $(APP)/app_MPPT.c
	endif
	
	ifneq ($(findstring APP_USE_GSM TRUE,$(APPCONF)),)
		APPSRC += $(APP)/app_gsm.c
	endif
	# ifneq ($(findstring APP_USE_WEB TRUE,$(APPCONF)),)
	# 	APPSRC += $(APP)/app_web.c
	# endif
	ifneq ($(findstring APP_USE_MB_MASTER TRUE,$(APPCONF)),)
		APPSRC += $(APP)/app_mb_master.c
	endif

	ifneq ($(findstring APP_USE_ALARM TRUE,$(APPCONF)),)
	APPSRC += $(APP)/app_alarm.c
	endif

	ifneq ($(findstring APP_USE_FORMULA TRUE,$(APPCONF)),)
	    APPSRC += $(APP)/app_formula.c
	endif
	
	ifneq ($(findstring APP_USE_SDC TRUE,$(APPCONF)),)
		APPSRC += $(APP)/app_sdc.c
	endif
	
	ifneq ($(findstring APP_USE_USB TRUE,$(APPCONF)),)
		APPSRC += $(APP)/app_sophia.c
	endif
else
	APPSRC += $(APP)/app_sequence.c	 \
			  $(APP)/app_blink.c	 \
			  $(APP)/app_adc.c		 \

	APPINC += $(LWIPAPP)
endif

