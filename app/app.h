
#ifndef __APP_H__
#define __APP_H__

/*********************************
    17 Nov 2017, Afrendy Bayu

*********************************/

#define TH_SHELL_SIZE       5500
#define TH_BLINK_SIZE       128
#define TH_GPS_SIZE         128
#define TH_ADC_SIZE         128
#define TH_BLINK2_SIZE      128
#define TH_MAC_SIZE         128
#define TH_MB_MASTER_SIZE   2048

#include "appconf.h"
#if (APP_USE_ADC == TRUE) || defined(__DOXYGEN__)
#include "app_adc.h"
#endif

#if (APP_USE_PWM == TRUE) || defined(__DOXYGEN__)
#include "app_pwm.h"
#endif

#if (APP_USE_BLINK == TRUE) || defined(__DOXYGEN__)
#include "app_blink.h"
#endif

#if (APP_USE_SDC == TRUE) || defined(__DOXYGEN__)
#include "app_sdc.h"
#endif

#if (APP_USE_SHELL == TRUE) || defined(__DOXYGEN__)
#include "app_shell.h"
#endif
#if (APP_USE_MPPT == TRUE) || defined(__DOXYGEN__)
#include "app_MPPT.h"
#endif

#if (APP_USE_MB_MASTER == TRUE) || defined(__DOXYGEN__)
#include "app_mb_master.h"
#endif

#if (APP_USE_GSM == TRUE) || defined(__DOXYGEN__)
#include "app_gsm.h"
#endif
#if (APP_USE_ALARM == TRUE) || defined(__DOXYGEN__)
#include "app_alarm.h"
#endif
#if (APP_USE_FORMULA == TRUE) || defined(__DOXYGEN__)
#include "app_formula.h"
#endif
#if (APP_USE_USB == TRUE) || defined(__DOXYGEN__)
   #include "app_sophia.h"
#endif

#define namaThread          chRegSetThreadName

// #define JML_KANAL_DISKRET       6
// #define JML_KANAL_ANALOG        6
// #define JML_KANAL_RELAY         4
// #define JML_KANAL_FORMULA       20

#endif
