#include "app_alarm.h"

// #include "relay.h"


#define STARTING_REGISTER 1

uint8_t alarm_temp, key_alarm = 1;

extern struct t_alarm st_alarm[];
extern struct t_data data[];
extern float data_f[9];
extern uint8_t data_relay;
struct t_ack ack;

char delay_alarm = 5, trig_status, ack_delay;
short data_alarm[15], reg_trigger;
float trig_acknowledge;

static virtual_timer_t ack_vt;

void masuk_1(void);
void masuk_2(void);
void masuk_3(void);


void masuk_1()
{
    key_alarm = 1;
    data_relay = 0;
}
void masuk_2()
{
    key_alarm = 3;
    data_relay = alarm_temp;
}
void masuk_3()
{
    key_alarm = 2;
    data_relay = alarm_temp;
}

static THD_WORKING_AREA(waThAlarm, 1024);
static THD_FUNCTION(ThAlarm, arg)
{

    (void)arg;
    chRegSetThreadName("Alarm");
    chVTObjectInit(&ack_vt);
    while (true)
    {
        ack.data = data_f[ack.reg - STARTING_REGISTER];
        for (unsigned char i = 0; i < JML_ALARM; i++)
        {
            if (st_alarm[i].status == 1)
            {
                alarm(i);
                alarm_temp = data_alarm[0] | data_alarm[1] | data_alarm[2] | data_alarm[3] | data_alarm[4] | data_alarm[5] | data_alarm[6] | data_alarm[7] | data_alarm[8] | data_alarm[9] | data_alarm[10] | data_alarm[11] | data_alarm[12] | data_alarm[13] | data_alarm[14] | data_alarm[15];
                /**************************************************************************************/
                /*  Acknowledge alarm dengan Lagging Alarm                                            */
                /**************************************************************************************/
                // printf("state : %d\r\n", key_alarm);
                if ((alarm_temp == 0) && (key_alarm == 2))
                { // alarm 1
                    // printf("alarm and emergency OFF\r\n");
                    key_alarm = 1;
                    chVTSet(&ack_vt, OSAL_S2I(ack.alarm_off_delay), (void *)masuk_1, NULL);
                }
                if ((alarm_temp == 1) || (alarm_temp == 2))
                {
                    if (key_alarm == 1)
                    {
                        //  printf("alarm ON\r\n");
                        key_alarm = 2;
                        data_relay = alarm_temp;
                        // output_relay(data_relay);
                        chVTSet(&ack_vt, OSAL_S2I(ack.alarm_off_delay), (void *)masuk_1, NULL);
                        if (ack.alarm_off_status == 0)
                        {
                            key_alarm = 0;
                            chVTResetI(&ack_vt);
                            // printf(" -> Hold\r\n");
                        }
                    }
                }
                if (alarm_temp == 3)
                {
                    chVTResetI(&ack_vt);
                    data_relay = alarm_temp;

                    chVTSet(&ack_vt, OSAL_S2I(ack.alarm_off_delay), (void *)masuk_3, NULL);
                    if (ack.alarm_off_status == 0)
                    {
                        key_alarm = 0;
                        chVTResetI(&ack_vt);
                        // printf(" -> Hold\r\n");
                    }
                }
                if ((ack.data >= 5) && (ack.status == 1))
                { 
                    // acknowledge button
                    ack.data = 0;
                    // printf("acknowledge trigger ON\r\n");
                    chVTResetI(&ack_vt);
                    data_relay = 0;
                    // output_relay(data_relay);
                    chThdSleepSeconds(ack.delay - 1); // ack
                    if (alarm_temp == 0)                          key_alarm = 2;
                    if ((alarm_temp == 1) || (alarm_temp == 2))   key_alarm = 1;
                    if (alarm_temp == 3)                          key_alarm = 2;
                    // printf("acknowledge trigger Selesai\r\n");
                    chThdSleepSeconds(1); // ack
                }
                /**************************************************************************************/
            }
        }
        chThdSleepMilliseconds(10);
    }
}
// port_timer_get_time();

void alarm(int no)
{
    short Type_alarm, regAlarm, regInt, regOut;
    Type_alarm = st_alarm[no].alarm_type;
    // printf("Tpe Alarm %d ", Type_alarm);
    regOut = st_alarm[no].reg_output - STARTING_REGISTER;
    regInt = st_alarm[no].regsumber - STARTING_REGISTER;
    regAlarm = st_alarm[no].reg_setpoint - STARTING_REGISTER;
    // printf("In %d, out %d, alrm %d\r\n", regInt, regOut, regAlarm);
    // printf("In %f, out %f, alrm %f\r\n", data_f[regInt], data_f[regOut], data_f[regAlarm]);
    if (Type_alarm == 2 || Type_alarm == 1)
    {
        data_alarm[no] = 0;
        if (data_f[regAlarm] != 0)
        {
            if (data_f[regInt] <= data_f[regAlarm])       data_alarm[no] = 1;
            else                                          data_alarm[no] = 0;
        }
    }
    if (Type_alarm == 3 || Type_alarm == 4)
    {
        data_alarm[no] = 0;
        if (data_f[regAlarm] != 0)
        {
            if (data_f[regInt] >= data_f[regAlarm])
                data_alarm[no] = 1;
            else
                data_alarm[no] = 0;
        }
    }
    switch (regOut + 1)
    {
    case 24:
        // printf("relay 1\r\n");
        data_f[regOut] = data_alarm[no];
        break;
    case 25:
        // printf("relay 2\r\n");
        data_f[regOut] = data_alarm[no];
        data_alarm[no] = data_alarm[no] << 1;
        break;

    default:
        // printf("relay salah\r\n");
        break;
    }
}

void thdAlarm(void)
{
    printf("-- Init App Alarm\r\n");
    chThdCreateStatic(waThAlarm, sizeof(waThAlarm), LOWPRIO, ThAlarm, NULL);
}
