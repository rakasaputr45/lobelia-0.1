#ifndef __APP_GSM_H__
#define __APP_GSM_H__

#include "ch.h"
#include "hal.h"
#include <stdio.h>
#include <stdlib.h>
#include "chprintf.h"
#include "portconf.h"
#include "VG.h"
#include <time.h>
#include <locale.h>
#include <math.h>
#include <modem.h>
#include <sd_card.h>

extern struct t_env st_env;
extern struct t_flag st_flag;
extern struct t_memory st_memory ;
extern struct t_monita_protokol st_monita_protokol;

static char  monita_gagal  = 0;
#define  cnt_gagal           4
#define  monita_interval     st_monita_protokol.interval
#define  monita_status       st_monita_protokol.status

static void cb_interval(virtual_timer_t *vtp, void *p);
static void cb_interval_gagal(virtual_timer_t *vtp, void *p);

static virtual_timer_t interval_kirim, interval_simpan_gagal;

static bool  FLAG_MONITA_KIRIM, FLAG_monita_gagal;

void thdgsm(void);
#endif


