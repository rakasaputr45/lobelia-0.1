#ifndef __APP_MPPT_H__
#define __APP_MPPT_H__


#include "ch.h"
#include "hal.h"
#include "VG.h"
#include <stdio.h>
#include "chprintf.h"
#include "portconf.h"
#include <math.h>


void thdMPPT(void);
void buck_Enable(int EN_driver);                    //Enable MPPT Buck Converter
void Device_Protection(void);
void backflowControl();                                             //PV BACKFLOW CONTROL (INPUT MOSFET)  
void led_charging();
                                                          //CHARGER MODE: Force backflow MOSFET on
#endif