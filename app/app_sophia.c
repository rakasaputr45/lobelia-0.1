#include "app_sophia.h"


  char DeviceName[18];

token_t lexer(const char *s) //diganti chulqy
{
    // TODO: consider hash table here
    static struct entry_s {
        const char *key;
        token_t token;
    } token_table[] = {
        { "default",    A0 }, // ini untuk cek data aja
        { "minta_env",  A1 }, //Meminta konfigurasi env modul
        { "minta_dat",  A2 }, //Meminta konfigurasi data modul
        { "minta_smb",  A3 }, //Meminta konfigurasi sumber modul
        { "minta_knl",  A4 }, //Meminta konfigurasi kanal modul
        { "minta_frm",  A5 }, //Meminta konfigurasi formula modul
        { "minta_can",  A6 }, //Meminta konfigurasi sumber canbus modul
        { "kirim_env",  B1 }, //Kirim konfigurasi env modul ke pc
        { "kirim_dat",  B2 }, //Kirim konfigurasi data modul ke pc
        { "kirim_smb",  B3 }, //Kirim konfigurasi sumber modul ke pc
        { "kirim_knl",  B4 }, //Kirim konfigurasi kanal modul ke pc
        { "kirim_frm",  B5 }, //Kirim konfigurasi formula modul ke pc
        { "kirim_can",  B6 }, //Kirim konfigurasi sumber canbus modul ke pc
        { "set_env",    C1 }, //Kirim konfigurasi env pc ke modul
        { "set_data",   C2 }, //Kirim konfigurasi data pc ke modul
        { "set_smbr",   C3 }, //Kirim konfigurasi sumber pc ke modul
        { "set_knal",   C4 }, //Kirim konfigurasi kanal pc ke modul
        { "set_form",   C5 }, //Kirim konfigurasi formula pc ke modul
        { "set_canb",   C6 }, //Kirim konfigurasi sumber canbus pc ke modul
        { "set_endef",  C7 }, //Mengatur konfigurasi environment default
        { "set_dtdef",  C8 }, //Mengatur konfigurasi data default (1 data)
        { "set_sbdef",  C9 }, //Mengatur konfigurasi sumber default (1 sumber)
        { "set_kldef",  C10 }, //Mengatur konfigurasi kanal default (1 kanal)
        { "set_frdef",  C11 }, //Mengatur konfigurasi formula default (1 formula)
        { "set_cndef",  C12 }, //Mengatur konfigurasi sumber canbus default (1 formula)
        { "set_flash",  C13 }, //Request ke modul untuk melakukan penyimpanan
        { "okey_env",   D1 }, //Konfigurasi env diterima dengan baik oleh modul
        { "okey_data",  D2 }, //Konfigurasi data diterima dengan baik oleh modul
        { "okey_smbr",  D3 }, //Konfigurasi sumber diterima dengan baik oleh modul
        { "okey_knal",  D4 }, //Konfigurasi kanal diterima dengan baik oleh modul
        { "okey_frma",  D5 }, //Konfigurasi formula diterima dengan baik oleh modul
        { "okey_canb",  D6 }, //Konfigurasi sumber canbus diterima dengan baik oleh modul
        { "okey_flsh",  D7 }, //Request penyimpanan konfigurasi diterima dengan baik oleh modul
        { "isi_env",    E1 }, //Kirim konfigurasi env ke pc
        { "isi_data",   E2 }, //Kirim data ke pc
        { "isi_smbr",   E3 }, //Kirim konfigurasi sumber ke pc
        { "isi_knal",   E4 }, //Kirim konfigurasi kanal ke pc
        { "isi_form",   E5 }, //Kirim konfigurasi formula ke pc
        { "isi_canb",   E6 }, //Kirim konfigurasi sumber canbus ke pc
        { "pnjng_env",  F1 }, //Kirim jumlah bytes env united
        { "pnjng_dat",  F2 }, //Kirim banyaknya data modul ke pc
        { "pnjng_smb",  F3 }, //Kirim banyaknya sumber ke pc
        { "pnjng_knl",  F4 }, //Kirim banyaknya kanal ke pc
        { "pnjng_frm",  F5 }, //Kirim banyaknya formula ke pc
        { "pnjng_can",  F6 }, //Kirim banyaknya sumber canbus ke pc
        { "sdt_dfall",  G2 }, //Mengatur konfigurasi data default (seluruh data)
        { "ssb_dfall",  G3 }, //Mengatur konfigurasi sumber default (serluruh sumber)
        { "skl_dfall",  G4 }, //Mengatur konfigurasi kanal default (serluruh kanal)
        { "sfm_dfall",  G5 }, //Mengatur konfigurasi formula default (serluruh formula)
        { "scn_dfall",  G6 }, //Mengatur konfigurasi sumber canbus default (serluruh formula)
        { "error_env",  H1 },
        { "error_dat",  H2 },
        { "error_smb",  H3 },
        { "error_knl",  H4 },
        { "error_frm",  H5 },
        { "error_can",  H6 },
        { "st_code",    I1 }, //Meminta nama board modul
        { "kirim_cod",  I2 }, //Kirim kode modul
        
    };
    struct entry_s *p = token_table;
    for(; p->key != NULL && strcmp(p->key, s) != 0; ++p);
    return p->token;
}

void initUSB(void)
{
  sdStart(&LPSD1, &BL_config);
  palSetPadMode(GPIOB, 2, PAL_MODE_OUTPUT_PUSHPULL);  // BT Reset
  palSetPad(GPIOB, 2);
}

//thread komunikasi usb
THD_WORKING_AREA(Sophia_Thread_Area, 1500);
THD_FUNCTION(Sophia_Thread_Function, arg)
{
    (void)arg;
    chRegSetThreadName("monita_bluetooth");

    struct t_kom_dat usb_data;
    uint8_t ptr;
    char cr = 13; //carriage return atau new line atau ente
    while(true)
    {   
    char buff[sizeof(usb_data) + 1];
    memset(&buff,    '\0', sizeof(usb_data));
    // /*
    int a = sdReadTimeout(&LPSD1, (uint8_t *)&buff, sizeof(usb_data) + 1, 500);
        int index_ = 0;
     if(a != FALSE)
        {
            memcpy(&usb_data,&buff,sizeof(usb_data));
            switch (lexer(usb_data.kpl))
            {
                //=====================================================
                case A0 ://{ "default",    A0 }, //ini untuk cek data aja
                    sprintf(usb_data.kpl,"error_cmd");
                    sprintf(usb_data.iden,"11111");
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    break;
                //=====================================================
                // case A1 : //{ "minta_env",  A1 }, //Meminta konfigurasi env modul
                //     sprintf(usb_data.kpl,"pnjng_env");

                //     index_ = atoi(usb_data.iden);

                //     sprintf(usb_data.iden,"%d", env_united_ln);

                //     if(index_ == 99) //bagian ini, masing-masing modul beda
                //     {
                //         sprintf(usb_data.data, \
                //         "environment{env_dasar-%d,ethernet-%d,monita-%d,modbus_slave-%d,modbus_master-%d,jwt-%d,onlimo-%d},data{data-%d},sumber{sumber-%d},canbus{canbus-%d},kanal{kanal-%d},formula{formula-%d}",\
                //         sizeof(st_env)+sizeof(st_environment), sizeof(st_ethernet), sizeof(st_monita_protokol), sizeof(st_modbusS), sizeof(st_modbus_master), sizeof(st_jwt_protokol), sizeof(st_onlimo_protokol), \
                //         JML_TITIK_DATA,
                //         JML_SUMBER_MB,
                //         20,
                //         JML_KANAL,
                //         JML_FORMULA);
                //     }
                    
                //     strcpy(usb_data.cek,"\r\n");
                    
                //     memcpy(&buff,&usb_data,sizeof(usb_data));
                //     memcpy(&buff[sizeof(usb_data)],&cr,1);
                
                //     streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

                //     break;
                //=====================================================
                case A2 : //{ "minta_dat",  A2 }, //Meminta konfigurasi data modul
                    sprintf(usb_data.kpl,"pnjng_dat");           
                    sprintf(usb_data.iden,"%d",JML_TITIK_DATA);
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    break;

                //===================================================== tidak tersedia di  lobelia, dahlia dan amelia
                case A3 : //{ "minta_smb",  A3 }, //Meminta konfigurasi sumber modul
                    sprintf(usb_data.kpl,"pnjng_smb");  
                    sprintf(usb_data.iden,"%d",JML_SUMBER_MB);//sprintf(usb_data.iden,"%d",JUMLAH_SUMBER);
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    break;
            //     //===================================================== tidak tersedia di cattleya
                case A4 : //{ "minta_knl",  A4 }, //Meminta konfigurasi kanal modul
                    sprintf(usb_data.kpl,"pnjng_knl");  
                                 
                    sprintf(usb_data.iden,"%d",JML_KANAL);

                    strcpy(usb_data.cek,"\r\n");
                    
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                   
                    break;
            //     //===================================================== hanya tersedia di daffodil
            //     case A5 : //{ "minta_frm",  A5 }, //Meminta konfigurasi formula modul
            //         sprintf(usb_data.kpl,"pnjng_frm");  
                                 
            //         sprintf(usb_data.iden,"%d",JML_FORMULA);

            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                   
            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     // case A6 : //{ "minta_can",  A6 }, //Meminta konfigurasi sumber canbus modul
            //     //     sprintf(usb_data.kpl,"pnjng_can");  
                                 
            //     //     sprintf(usb_data.iden,"%d",JML_SUMBER_CANBUS);

            //     //     strcpy(usb_data.cek,"\r\n");
                    
            //     //     memcpy(&buff,&usb_data,sizeof(usb_data));
            //     //     memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //     //     streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                   
            //     //     break;
            //     //=====================================================
                case C1 : //{ "set_env",    C1 }, //Kirim konfigurasi env pc ke modul
                    sprintf(usb_data.kpl,"okey_env");
                    index_ = atoi(usb_data.iden);
                    sprintf(usb_data.iden, "%d",index_);             
                    memcpy(&env_united[index_*BESAR_PAKET], &usb_data.data, BESAR_PAKET);
                    united_to_env();
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    break;
                //=====================================================
                case C2 : //{ "set_data",   C2 }, //Kirim konfigurasi data pc ke modul
                    sprintf(usb_data.kpl,"okey_data");
                    index_ = atoi(usb_data.iden);
                    memcpy(&data[index_],&usb_data.data,sizeof(data[index_]));
                    strcpy(usb_data.cek,"\r\n");                
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1); 
                    break;
            //     //===================================================== tidak tersedia di dahlia dan amelia
                case C3 : //{ "set_smbr",   C3 }, //Kirim konfigurasi sumber pc ke modul
                    sprintf(usb_data.kpl,"okey_smbr");

                    index_ = atoi(usb_data.iden);
                    memcpy(&st_sumber[index_],&usb_data.data,sizeof(st_sumber[index_]));
                
                    strcpy(usb_data.cek,"\r\n");
                    
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    
                    break;
            //     //===================================================== tidak tersedia di cattleya
            //     case C4 : //{ "set_knal",   C4 }, //Kirim konfigurasi kanal pc ke modul
            //         sprintf(usb_data.kpl,"okey_knal");

            //         index_ = atoi(usb_data.iden);
            //         memcpy(&st_kadc[index_],&usb_data.data,sizeof(st_kadc[index_]));
                    
            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    
            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     case C5 : //{ "set_form",   C5 }, //Kirim konfigurasi formula pc ke modul
            //         sprintf(usb_data.kpl,"okey_form");

            //         index_ = atoi(usb_data.iden);
            //         memcpy(&st_formula[index_],&usb_data.data,sizeof(st_formula[index_]));
                    
            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    
            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     // case C6 : //{ "set_canb",   C6 }, //Kirim konfigurasi sumber canbus pc ke modul
            //     //     sprintf(usb_data.kpl,"okey_canb");

            //     //     index_ = atoi(usb_data.iden);
            //     //     memcpy(&st_canbus[index_],&usb_data.data,sizeof(st_canbus[index_]));
                    
            //     //     strcpy(usb_data.cek,"\r\n");
                    
            //     //     memcpy(&buff,&usb_data,sizeof(usb_data));
            //     //     memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //     //     streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    
            //     //     break;
            //     //=====================================================
            //     case C7 : //{ "set_endef",  C7 }, //Mengatur konfigurasi environment default
            //         sprintf(usb_data.kpl,"okey_env");

            //         sprintf(usb_data.iden,"%d", env_united_ln);

            //         default_env();
                    
            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //         break;
            //     //=====================================================
            //     case C8 : //{ "set_dtdef",  C8 }, //Mengatur konfigurasi data default (1 data)
            //         sprintf(usb_data.kpl,"okey_data");

            //         index_ = atoi(usb_data.iden);
            //         default_dapris(index_/JUMLAH_DATA_PERSUMBER, index_%JUMLAH_DATA_PERSUMBER, index_ + 1);
            //         memcpy(&usb_data.data,&data[index_],sizeof(data[index_]));

            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //         break;
            //     //===================================================== tidak tersedia di dahlia dan amelia
            //     case C9 : //{ "set_sbdef",  C9 }, //Mengatur konfigurasi sumber default (1 sumber)
            //         sprintf(usb_data.kpl,"okey_smbr");

            //         index_ = atoi(usb_data.iden);
            //         // cmd_sumber_i_default(index_);
            //         memcpy(&usb_data.data,&st_sumber[index_],sizeof(st_sumber[index_]));

            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //         break;
            //     //===================================================== tidak tersedia di cattleya
            //     case C10 : //{ "set_kldef",  C10 }, //Mengatur konfigurasi kanal default (1 kanal)
            //         sprintf(usb_data.kpl,"okey_knl");

            //         index_ = atoi(usb_data.iden);
            //         default_cekkan(index_);
            //         memcpy(&usb_data.data,&st_kadc[index_],sizeof(st_kadc[index_]));
                    
            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     case C11 : //{ "set_frdef",  C11 }, //Mengatur konfigurasi formula default (1 formula)
            //         sprintf(usb_data.kpl,"okey_form");

            //         index_ = atoi(usb_data.iden);
            //         default_satu_formula(index_);
            //         memcpy(&usb_data.data,&st_formula[index_],sizeof(st_formula[index_]));
                    
            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     // case C12 : //{ "set_cndef",  C12 }, //Mengatur konfigurasi sumber canbus default (1 sumber canbus)
            //     //     sprintf(usb_data.kpl,"okey_canb");

            //     //     index_ = atoi(usb_data.iden);
            //     //     canbus_i_default(index_);
            //     //     memcpy(&usb_data.data,&st_canbus[index_],sizeof(st_canbus[index_]));
                    
            //     //     strcpy(usb_data.cek,"\r\n");
                    
            //     //     memcpy(&buff,&usb_data,sizeof(usb_data));
            //     //     memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //     //     streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //     //     break;
            //     //=====================================================
            //     case C13 : //{ "set_flash",  C13 }, //Request ke modul untuk melakukan penyimpanan
            //         sprintf(usb_data.kpl,"okey_flsh");

            //         sprintf(usb_data.data,"semua data diflash");

            //         strcpy(usb_data.cek,"\r\n");

            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //         // flash_write_config();

            //         break;
            //     //=====================================================
                case E1 : //{ "isi_env",    E1 }, //Kirim konfigurasi env ke pc
                    sprintf(usb_data.kpl,"kirim_env");
                    index_ = atoi(usb_data.iden);
                    set_united();
                    memcpy(&usb_data.data,&env_united[index_*BESAR_PAKET], BESAR_PAKET);
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

                    break;
            //     //=====================================================
                case E2 : //{ "isi_data",   E2 }, //Kirim data ke pc
                    sprintf(usb_data.kpl,"kirim_dat");
                    float data_temp = 0;
                    index_ = atoi(usb_data.iden);
                    memcpy(&usb_data.data,&data[index_],sizeof(data[index_]));
                    getValueRegister(index_);
                    data_temp = atof(dataReg);                                                                                                                                                 
                    memcpy(&usb_data.data[sizeof(data[index_])],&data_temp,sizeof(data_temp));
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    break;
                //===================================================== tidak tersedia di dahlia dan amelia
                case E3 : //{ "isi_smbr",   E3 }, //Kirim konfigurasi sumber ke pc
                    sprintf(usb_data.kpl,"kirim_smb");    
                    index_ = atoi(usb_data.iden);
                    memcpy(&usb_data.data,&st_sumber[index_],sizeof(st_sumber[index_]));
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);                
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    break;
            //     //===================================================== tidak tersedia di cattleya
            //     case E4 : //{ "isi_knal",   E4 }, //Kirim konfigurasi kanal ke pc
            //         sprintf(usb_data.kpl,"kirim_knl");
                    
            //         index_ = atoi(usb_data.iden);
            //         memcpy(&usb_data.data,&st_kadc[index_],sizeof(st_kadc[index_]));

            //         strcpy(usb_data.cek,"\r\n");

            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                   
            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     case E5 : //{ "isi_form",   E5 }, //Kirim konfigurasi formula ke pc
            //         sprintf(usb_data.kpl,"kirim_frm");
                    
            //         index_ = atoi(usb_data.iden);
            //         memcpy(&usb_data.data,&st_formula[index_],sizeof(st_formula[index_]));

            //         strcpy(usb_data.cek,"\r\n");

            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                   
            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     // case E6 : //{ "isi_form",   E6 }, //Kirim konfigurasi sumber canbus ke pc
            //     //     sprintf(usb_data.kpl,"kirim_can");
                    
            //     //     index_ = atoi(usb_data.iden);
            //     //     memcpy(&usb_data.data,&st_canbus[index_],sizeof(st_canbus[index_]));

            //     //     strcpy(usb_data.cek,"\r\n");

            //     //     memcpy(&buff,&usb_data,sizeof(usb_data));
            //     //     memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //     //     streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                   
            //     //     break;
            //     //=====================================================
                case G2 : //{ "sdt_dfall",  G2 }, //Mengatur konfigurasi data default (seluruh data)
                    sprintf(usb_data.kpl,"okey_data");
                    sprintf(usb_data.iden,"%d",JML_TITIK_DATA);
                    default_data();
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

                    break;
            //     //===================================================== tidak tersedia di dahlia dan amelia
                case G3 : //{ "ssb_dfall",  G3 }, //Mengatur konfigurasi sumber default (serluruh sumber)
                    sprintf(usb_data.kpl,"okey_smbr");
                    sprintf(usb_data.iden,"%d",JUMLAH_SUMBER);
                    default_sumber();
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

                    break;
                //===================================================== tidak tersedia di cattleya
                case G4 : //{ "skl_dfall",  G4 }, //Mengatur konfigurasi kanal default (serluruh kanal)
                    sprintf(usb_data.kpl,"okey_knal");

                    sprintf(usb_data.iden,"%d",JML_KANAL);

                    default_kanal();

                    strcpy(usb_data.cek,"\r\n");
                    
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

                    break;
            //     //===================================================== hanya tersedia di daffodil
            //     case G5 : //{ "sfm_dfall",  G5 }, //Mengatur konfigurasi formula default (serluruh formula)
            //         sprintf(usb_data.kpl,"okey_form");

            //         sprintf(usb_data.iden,"%d",JML_FORMULA);

            //         default_formula();

            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //         break;
            //     //===================================================== hanya tersedia di daffodil
            //     // case G6 : //{ "scn_dfall",  G6 }, //Mengatur konfigurasi sumber canbus default (serluruh canbus)
            //     //     sprintf(usb_data.kpl,"okey_canb");

            //     //     sprintf(usb_data.iden,"%d",JML_SUMBER_CANBUS);

            //     //     canbus_default();

            //     //     strcpy(usb_data.cek,"\r\n");
                    
            //     //     memcpy(&buff,&usb_data,sizeof(usb_data));
            //     //     memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //     //     streamWrite(&LPSD1, buff, sizeof(usb_data)+1);

            //     //     break;
            //     //=====================================================
            //     case I1 : //{ "st_code",    I1 }, //Meminta nama board modul
            //         sprintf(usb_data.kpl,"kirim_cod");  
                                 
            //         sprintf(usb_data.data,"%s",BOARD_NAME);

            //         strcpy(usb_data.cek,"\r\n");
                    
            //         memcpy(&buff,&usb_data,sizeof(usb_data));
            //         memcpy(&buff[sizeof(usb_data)],&cr,1);
                
            //         streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                   
            //         break;
                //=====================================================
                default :
                    //di sini dikasih defaultnya
                    sprintf(usb_data.kpl,"error_cmd");
                    sprintf(usb_data.iden,"11111");
                    strcpy(usb_data.cek,"\r\n");
                    memcpy(&buff,&usb_data,sizeof(usb_data));
                    memcpy(&buff[sizeof(usb_data)],&cr,1);
                    streamWrite(&LPSD1, buff, sizeof(usb_data)+1);
                    break;
            }
                chThdSleepMilliseconds(10);
    }
    // */
    chThdSleepMilliseconds(10);
    }
}

void thd_Sophia(void)
{
    printf("\r\n--- Monita Bluetooth Aplikasi ---\r\n");
    initUSB();
    char cmd[50];
    char c;
    int i = 0;

    memset(&cmd, '\0', 50);
    printfBT("AT+DISC\r\n");
    while (sdReadTimeout(&LPSD1, (uint8_t *)&c, 1, 1000) == 1)
    {  }
    printfBT("AT+ENLOG0\r\n");
    while (sdReadTimeout(&LPSD1, (uint8_t *)&c, 1, 1000) == 1)
    { }

    printfBT("AT+LADDR\r\n");
    while (sdReadTimeout(&LPSD1, (uint8_t *)&c, 1, 1000) == 1)
    {
      cmd[i] = c;
      i++;
    }

      char *token = NULL;
   token = strtok(cmd, "+LADDR=");
   while( token != NULL ) 
   {
      sprintf(DeviceName, "LBL-%s",token);
      token = strtok(NULL, "+LADDR=");
   }
      
    printfBT("AT+NAME%s\r\n", DeviceName);
    while (sdReadTimeout(&LPSD1, (uint8_t *)&c, 1, 1000) == 1)     {  }

      printfBT("AT+RESET\r\n");
    while (sdReadTimeout(&LPSD1, (uint8_t *)&c, 1, 1000) == 1)     {  }

    printf("--- Nama Bluetooth : %s\r\n", DeviceName);
    chThdCreateStatic(Sophia_Thread_Area, sizeof(Sophia_Thread_Area), NORMALPRIO + 1, Sophia_Thread_Function, NULL);
}


