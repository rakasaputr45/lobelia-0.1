#ifndef __APP_PWM_H__
#define __APP_PWM_H__


#include "ch.h"
#include "hal.h"
#include <stdio.h>
#include "chprintf.h"
#include "VG.h"
#include "portconf.h"


void thdPWM();
void init_pwm();
void predictivePWM();
int Constrain(int au32_IN, int au32_MIN, int au32_MAX);
void chargingAlgorithm();
void PWM_Modulation();
void masuk_1(void);
void init_variable_MPPT();
void prePWM();
void PWM_();
void pwm_default();




#endif