#include "app_gpio_int.h"

#include "hal.h"
#include "ch.h"
#include "gpio_int.h"
#include "portconf.h"
#include "VG.h"

extern struct t_formula st_formula[];

static virtual_timer_t tmrm;
int micron = 0;
static void gpt6cb(GPTDriver *gptp)
{
	(void)gptp;
	chSysLockFromISR();
	sistem_rpm();   sistem_onoff();  sistem_conting();   timer();
	micron++;
	chSysUnlockFromISR();
}
static const GPTConfig gpt6cfg = {
	1000000, /* 10kHz timer clock.*/
	gpt6cb,	 /* Timer callback.*/
	0,
	0
};

static THD_WORKING_AREA(waThreadgpio_int, 2524);
static THD_FUNCTION(Threadgpio_int, arg)
{
	(void)arg;
	unsigned char i = 0;
	unsigned char modbus_loop = 0;
	chRegSetThreadName("gpio_int");
	gptStart(&GPTD6, &gpt6cfg);
	gptStartContinuous(&GPTD6, 100);
	chVTSet(&tmrm, TIME_MS2I(1000), (void *)counting_timer_S, NULL);
	while (true)
	{
		init_gpio();
		chThdSleepMilliseconds(1000);
	}
}

void thread_digital_input(void)
{
	chThdCreateStatic(waThreadgpio_int, sizeof(waThreadgpio_int), LOWPRIO, Threadgpio_int, NULL);
}
