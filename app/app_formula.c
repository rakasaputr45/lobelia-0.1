#include "app_formula.h"

extern struct t_formula st_formula[];

static void parsing_formula(char*s,int no);

static THD_WORKING_AREA(waThFormula, 2024);
static THD_FUNCTION(ThFormula, arg) {

    (void)arg;
    chRegSetThreadName("Formula");
    
    while (true) {
                    // printf("muter\r\n");
        for(unsigned char i=0;i<JML_FORMULA;i++){
            if(st_formula[i].status) parsing_formula(st_formula[i].form,i);  
        }
        chThdSleepMilliseconds(500);
    }
}

static void parsing_formula(char*s,int no)	{
    // printf("muter 1\r\n");

	char *tmp1=malloc(50), *tmp2=malloc(20);
    char a[10], b[10], c[2], d[10];
    char sumberRegis[10];
    unsigned char l=0;
	int i;
	float r, *fl;

	memset(tmp1, '\0', 50);
	memset(tmp2, '\0', 20);
    memset(b, '\0', 10);
    memset(a, '\0', 10);

    for(i=0;i<(signed)strlen(st_formula[no].parameter);i++)
    {
        if(st_formula[no].parameter[i]==',')   l++;
        else                                   a[l]=st_formula[no].parameter[i];
    }
    l=0;
    for(i=0;i<(signed)strlen(st_formula[no].regsumber);i++)
    {
        if(st_formula[no].regsumber[i]==',')
        {    
            d[l]=atoi(b);   memset(b, '\0', 10);    l++;
        }
        else 
        {
            sprintf(c,"%c",st_formula[no].regsumber[i]);    strcat(b,c);
        }
    }
    d[l]=atoi(b);
    printf("%c\r\n",b);
    memset(sumberRegis, '\0', 10);
    l=0;

    for(i=0;i<(signed)strlen(s);i++)
    {
        for(l=0;l<(signed)strlen(a);l++)
        {
            if(s[i]==a[l])
            {
                if(d[l]<= JML_TITIK_DATA)
                {
                    // sprintf(tmp2,"%0.2f",data_f[d[l]-1]);
                    // sprintf(tmp2,"%0.2f",getValueRegister(d[l]-1));
                    // chHeapFree(value_temp);
                }
                strcat(tmp1, tmp2);
                c[0]=0;                
                break;
            }
            c[0]=1;
        }
        if(c[0]==1){
            sprintf(tmp2,"%c",s[i]);
            strcat(tmp1, tmp2);
            c[0]=0;
        }
    }   
    r = te_interp(tmp1, 0);	
    printf("putar putar\r\n");
	fl = &r;
    char *nilai;
    sprintf(nilai, "%f", r);
    printf("hasil %s", nilai);
    setValueRegister(nilai, st_formula[no].regtujuan-1);
	// memcpy(&data_f[st_formula[no].regtujuan-1],fl,sizeof(float));
    if(isnan(data_f[st_formula[no].regtujuan-1])==1){
        data_f[st_formula[no].regtujuan-1]=0;
    }
    // }
    memset(b, '\0', 10);
    memset(a, '\0', 10);
    memset(d, '\0', 10);
    memset(c, '\0', 2);
    free(tmp1);
    free(tmp2);
	return 0;
}

void thdFormula() {
    chThdCreateStatic(waThFormula, sizeof(waThFormula), LOWPRIO, ThFormula, NULL);
}