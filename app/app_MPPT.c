#include <app_MPPT.h>

// 500ms
// 1000 s/s = 100
#define voltageInputMax       40.0
#define voltageBatteryMin     5     //   USER PARAMETER - Minimum Battery Charging Voltage (Output V)
#define MaxCurrentIn          3.0
#define MaxCurrentOut         3.0

static virtual_timer_t vt;

float
    powerInput,              // SYSTEM PARAMETER - Input power (solar power) in Watts
    powerInputPrev,          // SYSTEM PARAMETER - Previously stored input power variable for MPPT algorithm (Watts)
    powerOutput,             // SYSTEM PARAMETER - Output power (battery or charing power in Watts)
    voltageInput,            // SYSTEM PARAMETER - Input voltage (solar voltage)
    voltageInputPrev,        // SYSTEM PARAMETER - Previously stored input voltage variable for MPPT algorithm
    voltageOutput,           // SYSTEM PARAMETER - Input voltage (battery voltage)
    currentInput,            // SYSTEM PARAMETER - Output power (battery or charing voltage)
    currentOutput,           // SYSTEM PARAMETER - Output current (battery or charing current in Amperes),
    voltageBattery;

bool
    output_Mode = 1,         //   USER PARAMETER - 0 = PSU MODE, 1 = Charger Mode  
    bypassEnable = 0,        // SYSTEM PARAMETER -  
    IUV = 0,                 // SYSTEM PARAMETER - Input Under Voltage
    IOV = 0,                 // SYSTEM PARAMETER - Input Over Voltage
    IOC = 0,                 // SYSTEM PARAMETER - Input Over Current
    OUV = 0,                 // SYSTEM PARAMETER - Output Under Voltage
    OOV = 0,                 // SYSTEM PARAMETER - Output Over Voltage
    OOC = 0,                 // SYSTEM PARAMETER - Output Over Current
    REC = 0,                 // SYSTEM PARAMETER - 
    BND = 0,
    buckEnable;          // SYSTEM PARAMETER - Buck Enable Status

short
    ERR                   = 0,           // SYSTEM PARAMETER - 
    PWM                   = 0,           // SYSTEM PARAMETER -
    PPWM                  = 0;           // SYSTEM PARAMETER -
float
    pwm_cal               = 0.24;
bool  predict, predict1, predict2, predict3, predict4, predict5; 

static THD_WORKING_AREA(MPPTThread, 1500);
static THD_FUNCTION(ThreadMPPT, arg)
{
    (void)arg;
    chRegSetThreadName("mppt");

    delay(5000);
    while (true)
    {
          init_variable_MPPT();
          Device_Protection();
          chargingAlgorithm();
          chThdSleepMilliseconds(200);
    }
}
void thdMPPT(void)
{
  printf("\r\n--- Charging PV Aplikasi\r\n");
    chThdCreateStatic(MPPTThread, sizeof(MPPTThread), LOWPRIO, ThreadMPPT, NULL);
}
void Device_Protection(void)
{      
        ERR = 0, REC = 0 , BND = 0;
        if (voltageInput  > voltageInputMax)     {IOV = 1; ERR++;}                                  else IOV = 0;
        if (voltageOutput > voltageBatteryMax)   {OOV = 1; ERR++;}                                  else OOV = 0;
        if (currentInput  > MaxCurrentIn)        {IOC = 1; ERR++;}                                  else IOC = 0;
        if (currentOutput > MaxCurrentOut)       {OOC = 1; ERR++;}                                  else OOC = 0;      
        if (voltageInput  <= voltageOutput)      {IUV = 1; REC = 1;voltageBattery = voltageOutput;} else REC = 0;
        if (voltageOutput <= voltageBatteryMin)  {BND = 1; }                                                                                       else   BND = 0;
}  

void buck_Enable(int EN_driver){                    //Enable MPPT Buck Converter
    bypassEnable = EN_driver;
    if (EN_driver)  palSetPad(GPIOH, 0U);       
    else                 palClearPad(GPIOH, 0U);
}

