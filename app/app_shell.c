#include "app.h"

void cmd_printf2(void)
{
    printf(" ----- coba 2 command shell -----\r\n");
    printf(" ----- baris ke 2 command shell -----\r\n");
}

const ShellCommand commands[] = {
    // {"date", (void *)cmd_printf2},
    {"cek_env",    (void *)cek_env},
    {"set_env",    (void *)set_env},
    {"cek_data",   (void *)cek_data},
    {"set_data",   (void *)set_data},
    {"cek_kanal",  (void *)cek_kanal},
    {"set_kanal",  (void *)set_kanal},
    {"cek_sumber", (void *)cmd_cek_sumber},
    {"set_sumber", (void *)cmd_set_sumber},
    {"cek_uptime", (void *)cek_uptime},
    {"cek_time", (void*) cek_time},  //OKE
    {"set_time", (void*) set_time},  //OKE
    {"reset", (void*) restart},  //OKE
    {"cek_modem", (void*) cek_modem},  //OKE
    {"mem", (void*) cmd_mem},  //OKE
    {"threads", (void*) cmd_threads},  //OKE
    {"set_config", (void*) flash_all}, 
    {"set_flash", (void*) set_flash}, 
    // {"help", (void*) help}, 
    {NULL, NULL}};

static THD_WORKING_AREA(waShell, TH_SHELL_SIZE);

void thdShell(void)
{
    printInitShell();
    sdStart(&SD2, &SHELL_cfg);
    static const ShellConfig shell_cfg1 = {(BaseSequentialStream *)&SD2, commands};
    chThdCreateStatic(waShell, sizeof(waShell), NORMALPRIO - 1, shellThread, (void *)&shell_cfg1);
}

void printInitShell(void)
{
    printf("\r\n--- Shell Aplikasi ...\r\n");
}
