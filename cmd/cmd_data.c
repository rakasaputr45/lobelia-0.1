#include <inttypes.h>
#include "cmd.h"
#include "VG.h"



char dataReg[10];
void  setFloat32bit(float float_t, int n);
void  setInteger64bit(int64_t integer64_t, int n);
void  setInteger16bit(short short_t, int n);
void  setInteger32bit(int integer_t, int n);
void  getValueRegister(int n);
void setValueRegister(char *valueRegister, int n);

struct t_data data[JML_TITIK_DATA];

int di = 0;

void set_data(int argc, char *argv[])
{
    if (strcmp(argv[0], "default") == 0)
    {
        default_data();
    }

    else if (strcmp(argv[0], "help") == 0)
    {
        data_halp();
    }

    else if (strcmp(argv[1], "status_monita") == 0 && argc == 3)
    {
        data[atoi(argv[0]) - 1].status = atoi(argv[2]);
        printf("Index ke-%s : %s \r\n", argv[0], atoi(argv[2]) ? "Aktif" : "Mati");
    }

    else if (strcmp(argv[1], "nama") == 0 && argc == 3)
    {
        sprintf(data[atoi(argv[0]) - 1].nama, "%s", argv[2]);
    }

    else if (strcmp(argv[1], "satuan") == 0 && argc == 3)
    {
        sprintf(data[atoi(argv[0]) - 1].satuan, "%s", argv[2]);
    }

    else if (strcmp(argv[1], "tipe") == 0 && argc == 4)
    {
        data[atoi(argv[0]) - 1].tipe = atoi(argv[2]);
        data[atoi(argv[0]) - 1].panjang = atoi(argv[3]); 
    }

    else if (strcmp(argv[1], "isi") == 0 && argc == 3)  // blm
    {  
        int index = atoi(argv[0]) - 1;
        printf("register %d : %s\r\n",index, argv[2]);
        setValueRegister(argv[2], index);
    }

    else
    {
        printf("Command Error !!!\r\n");
        return;
    }
}

void default_data()
{
    unsigned char i;
    int  d1 = 1;

    for (i = 0; i < JML_TITIK_DATA; i++) 
    {
            sprintf(data[i].nama, "data_%d", i + 1);
            data[i].id = d1;
            data[i].status = 0;  
            data[i].status_simpan = 0; 
            data[i].L = i;  
            data[i].LL = i + 0.2;
            data[i].H = i + 0.3;
            data[i].HH = i + 0.4;
            data[i].err_H = 0;  
            data[i].err_L = 0;  
            data[i].tipe = 0;  // 0 float 1 integer 32, 2 bit
            data[i].panjang = 1;  // 0 16 bit, 1 32 bite, 2 64 bit, 3 1 bit
            if (i < 9)  
            { 
              sprintf(data[i].nama, "%s",nama + i);
              data[i].status_simpan = 1; 
              data[i].status = 1; 
            }
            else {
            data[i].tipe = 0;  // 0 float 1 integer 32, 2 bit
            data[i].panjang = 1;  // 0 16 bit, 1 32 bite, 2 64 bit, 3 1 bit
            }
        d1 = d1 + 2;
        }
    printf("default data\r\n");
}

void default_dapris(int i, int j, int di)
{
    sprintf(data[i * JUMLAH_DATA_PERSUMBER + j].nama, "data_input_%d", i * JUMLAH_DATA_PERSUMBER + j + 1);
    data[i * JUMLAH_DATA_PERSUMBER + j].id = 1000 + di;
    strcpy(data[i * JUMLAH_DATA_PERSUMBER + j].satuan, "-");
    data[i * JUMLAH_DATA_PERSUMBER + j].status = 1;
    data[i * JUMLAH_DATA_PERSUMBER + j].status_simpan = 1;
}
void data_halp()
{
    printf("\r\nSETTING DATA\r\n---------------------------------------------------------------------------------------");
    printf("\r\n\r\n setting data input : set_data [no data] [opsi] [nilai]\r\n");
    printf("    [opsi]    : Nama, isi,tipe data,status monita\r\n\r\n");
    printf(" setting nama:\r\n");
    printf("    -misal : set_data 24 nama data_relay   [NB a untuk nama sumber data ke-24]\r\n");
    printf(" setting status:\r\n");
    printf("    -misal : set_data 1 status_monita 1    [NB 1 Aktif 0 Mati]\r\n");
    printf(" setting nilai:\r\n");
    printf("    -misal : set_data 1 isi 1.2            [NB untuk nilai data ke-3]\r\n");
    printf(" setting tipe data:\r\n");
    printf("    -misal : set_data 10 tipe 1 0          [NB untuk setting tipe data dan panjang data [tipe] [panjang data] >> [0:float, 1:integer, 2: double, 3:bit]   [0 = 16bit, 1 = 32bit, 2 = 64, 3  = bit data]\r\n");
    printf(" setting satuan:\r\n");
    printf("    -misal : set_data 1 satuan PH          [NB untuk memberi satuan dari data ke-1]\r\n");
    printf("-----------------------------------------------------------------------------------------\r\n\r\n");
}

void cek_data(int argc, char *argv[])
{
      printf("\r\n\r\nError Flag : %s\r\n\r\n", flagARUSERROR?RED("ERROR"):GREEN("No error"));
  printf("\r\n\r\nData Modul\r\n\r\n");
  printf("No    Register  Nama              Nilai         Tipe       Panjang    Satuan        Status Monita \r\n");
  printf("====  ========  ================  ============  =========  =========  ============  ==============\r\n");
  for (int muter = 0; muter < JML_TITIK_DATA; muter++)
    {
    getValueRegister(muter);
    printf( "  %2d  %8d  %16s  %12s  %9s  %9s  %12s  %14s\r\n", muter + 1, data[muter].id, data[muter].nama, dataReg, data_tipe + data[muter].tipe, data_length + data[muter].panjang, data[muter].satuan, (data[muter].status)?GREEN("Aktif"):RED("Mati"));
    delay(10);
    }
    printf("\r\n\r\n");
}

void setValueRegister(char *valueRegister, int n)
{
    char tipeData    = data[n].tipe;
    char panjangData = data[n].panjang;
    short regbank    = data[n].id;

    switch (tipeData)
    {
    case 0: // float
        if (panjangData == 1)
        {
            float float_t = atof(valueRegister);
            uint32_t i;
            memcpy(&i, &float_t, sizeof(float));
            data_reg[regbank - 1] = (uint16_t)i;
            data_reg[regbank] = (uint16_t)(i >> 16);
            data_reg[regbank - 1] = swapBytes(data_reg[regbank - 1]);
            data_reg[regbank] = swapBytes(data_reg[regbank]);
        }
        break;
    case 1:
        if (panjangData == 0)
        {
            short short_t = atoi(valueRegister);
            data_reg[regbank - 1] = (uint16_t)short_t;
        }
        else if (panjangData == 1)
        {
            int integer_t = atoi(valueRegister);
            // printf("   %d , %08x ", integer_t , integer_t);
            short regbank = data[n].id;
            data_reg[regbank - 1] = (uint16_t)integer_t;
            data_reg[regbank] = (uint16_t)(integer_t >> 16);
            data_reg[regbank - 1] = swapBytes(data_reg[regbank - 1]);
            data_reg[regbank] = swapBytes(data_reg[regbank]);
            // printf(" baru %04x%04x ", data_reg[regbank], data_reg[regbank - 1]);
        }
        else if (panjangData == 2)
        {
            unsigned long integer64_t = atoll(valueRegister);
            printf("%U", integer64_t);
            data_reg[regbank - 1] = (uint16_t)integer64_t;
            data_reg[regbank] = (uint16_t)(integer64_t >> 16);
            data_reg[regbank + 1] = (uint16_t)(integer64_t >> 32);
            data_reg[regbank + 2] = (uint16_t)(integer64_t >> 48);
            printf("hexa %llX, %02x%02x%02x%02x", integer64_t, data_reg[regbank + 2],data_reg[regbank + 1], data_reg[regbank], data_reg[regbank]  , data_reg[regbank - 1]);
        }
        break;
    default:
        printf("salah input !!!!!!\r\n");
        break;
    }
}

 /* DEPRECATED - Set a float to 4 bytes in a sort of Modbus format! */
void setInteger16bit(short short_t, int n)
{
    short regbank =  data[n].id;
    data_reg[regbank - 1] = (uint16_t)short_t;
}

/* DEPRECATED - Set a float to 4 bytes in a sort of Modbus format! */
void  setInteger32bit(int integer_t, int n)
{   
    // printf("   %d , %08x ", integer_t , integer_t);
    short regbank =  data[n].id;
    data_reg[regbank - 1] = (uint16_t)integer_t;
    data_reg[regbank] = (uint16_t)(integer_t >> 16);
    data_reg[regbank - 1] = swapBytes(data_reg[regbank - 1]);
    data_reg[regbank]     = swapBytes(data_reg[regbank]);
    // printf(" baru %04x%04x ", data_reg[regbank], data_reg[regbank - 1]);
}

// /* DEPRECATED - Set a float to 4 bytes in a sort of Modbus format! */
void setInteger64bit(int64_t integer64_t, int n)
{
    int regbank =  data[n].id;
    data_reg[regbank - 1] = (uint16_t)integer64_t;
    data_reg[regbank]     = (uint16_t)(integer64_t >> 16);
    data_reg[regbank + 1] = (uint16_t)(integer64_t >> 32);
    data_reg[regbank + 2] = (uint16_t)(integer64_t >> 48);
    data_reg[regbank - 1] = swapBytes(data_reg[regbank - 1]);
    data_reg[regbank]     = swapBytes(data_reg[regbank]);
    data_reg[regbank + 1] = swapBytes(data_reg[regbank + 1]);
    data_reg[regbank + 2] = swapBytes(data_reg[regbank + 2]);
}

// /* DEPRECATED - Set a float to 4 bytes in a sort of Modbus format! */
void  setFloat32bit(float float_t, int n)
{   
    uint32_t i;
    memcpy(&i, &float_t, sizeof(float));
    short regbank =  data[n].id;
    data_reg[regbank - 1] = (uint16_t)i;
    data_reg[regbank] = (uint16_t)(i >> 16);
    data_reg[regbank - 1] = swapBytes(data_reg[regbank - 1]);
    data_reg[regbank]     = swapBytes(data_reg[regbank]);
    
}

void getValueRegister(int n)
{
    char tipeData    = data[n].tipe;
    char panjangData = data[n].panjang;
    short regbank    = data[n].id;

    uint32_t intejer;
    float float_temp;

    switch (tipeData)
    {
    case 0: // float
        intejer = (((uint32_t)swapBytes(data_reg[regbank])) << 16) + swapBytes(data_reg[regbank - 1]);
        memcpy(&float_temp, &intejer, sizeof(float));
        sprintf(dataReg, "%5.3f", float_temp);
        break;
    case 1: // integer
        if (panjangData == 0)
        {
            short short_temp = data_reg[regbank - 1];
            sprintf(dataReg, "%4d", short_temp);
        }
        else if (panjangData == 1)
        {
            int integer_temp = (((uint32_t)swapBytes(data_reg[regbank])) << 16) + swapBytes(data_reg[regbank - 1]);
            sprintf(dataReg, "%8d", integer_temp);
        }
        else if (panjangData == 2)
        {       
            long integer64_temp = (((uint64_t)swapBytes(data_reg[regbank + 2])) << 48) + (((uint64_t)swapBytes(data_reg[regbank + 1])) << 32) + (((uint64_t)swapBytes(data_reg[regbank])) << 16) + swapBytes(data_reg[regbank - 1]);
        }
        break;
    case 2: // float bit
        if (panjangData == 3)
            printf("double  \r\n");
        // bit data belum dibuat ??
        break;
    case 3: // float bit
        if (panjangData == 2)
            printf("berhasil bit \r\n");
        // bit data belum dibuat ??
        break;
    default:
        break;
    }
}
