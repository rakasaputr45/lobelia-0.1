

#include "cmd_common.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void substring(char s[], char sub[], int p, int l) {
   int c = 0;

   while (c < l) {
      sub[c] = s[p+c-1];
      c++;
   }
   sub[c] = '\0';
}

void f(char *s) {
    printf("---%*s%*s---\n", 10+strlen(s)/2, s, 10-strlen(s)/2, "");
}

void center_print(const char *s, int width)     {
    int length = strlen(s);
    int i;
    for (i=0; i<=(width-length)/2; i++) {
        printf(" ");
    }
    printf(s);
    i += length;
    for (; i<=width; i++) {
        printf(" ");
    }
}
