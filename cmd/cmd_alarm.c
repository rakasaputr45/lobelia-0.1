#include <hal.h>
#include <cmd_alarm.h>
#include <portconf.h>
#include <VG.h>
#include <stdlib.h>

struct t_alarm st_alarm[JML_ALARM];

int temp, no;

void set_alarm(int argc, char *argv[])
{
  // printf("isi %s %s %s", argv[0], argv[1], argv[2]);
  if (strcmp(argv[1], "status") == 0 && argc == 3)
  {
    no = atoi(argv[0]);
    temp = atoi(argv[2]);
    st_alarm[no - 1].status = temp;
    printf("alarm %s status %s\r\n", argv[0], (st_alarm[atoi(argv[0]) - 1].status ? "Aktif" : "Mati"));
  }
  else if (strcmp(argv[1], ";") == 0 && argc == 7)
  {
    no = atoi(argv[0]);
    temp = atoi(argv[2]);
    st_alarm[no - 1].regsumber = temp;
    temp = atoi(argv[3]);
    st_alarm[no - 1].reg_setpoint = temp;
    temp = atoi(argv[4]);
    st_alarm[no - 1].reg_output = temp;
    temp = atoi(argv[5]);
    st_alarm[no - 1].alarm_type = temp;
    temp = atoi(argv[6]);
    st_alarm[no - 1].status = temp;
    printf("alarm no  %s sumber %d output %d setpoint %d status %s\r\n", argv[0], st_alarm[atoi(argv[0]) - 1].regsumber, st_alarm[atoi(argv[0]) - 1].reg_output, st_alarm[atoi(argv[0]) - 1].reg_setpoint,  (st_alarm[atoi(argv[0]) - 1].status ? "Aktif" : "Mati"));
  }
else if ((strcmp(argv[1],"alarm")==0) || (strcmp(argv[OFFSET+1],"-a")==0)) {
				char s1,s2[10],s[30],b=0;
                int aa;
                aa = atoi(argv[0] - 1);
                s1=sprintf(st_alarm[aa].form,"%s",argv[2]);

                printf("alarm : %s \r\n",st_alarm[aa].form);
                strcpy(s,argv[2]);

                sprintf(s2,"0");
                for(unsigned char i=0;i<s1;i++)
                {
                    if(s[i]==';'){
                        memset(s2,'\0', sizeof(s2));
                        b++;
                        sprintf(s2,"0");
                    }
                     else{
                        if(b==0){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_alarm[atoi(argv[0])-1].regsumber=atoi(s2);
                        }
                        else if(b==1){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_alarm[atoi(argv[0])-1].reg_setpoint=atoi(s2);
                        }
                        else if(b==2){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_alarm[atoi(argv[0])-1].reg_output=atoi(s2);
                        }
                        else if(b==3){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_alarm[atoi(argv[0])-1].alarm_type=atoi(s2);
                        }
                        else if(b==4){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_alarm[atoi(argv[0])-1].status=atoi(s2);
                        }
                    }
                }
                printf("Input %d;Setpoint %d; Relay %d; Level Alarm %d;status%d",st_alarm[atoi(argv[0])-1].regsumber,st_alarm[atoi(argv[0])-1].reg_setpoint,st_alarm[atoi(argv[0])-1].reg_output,st_alarm[atoi(argv[0])-1].alarm_type, st_alarm[atoi(argv[0])-1].status);
			}

  else if (strcmp(argv[0], "default") == 0)
  { 
    default_alarm();
  }
  else if (strcmp(argv[1], "sumber") == 0)
  {
    st_alarm[atoi(argv[0]) - 1].regsumber = atoi(argv[2]);
    printf("register data sumber  %s: %d\r\n", argv[0], st_alarm[atoi(argv[0]) - 1].regsumber);
  }
  else if (strcmp(argv[1], "output") == 0)
  {
    st_alarm[atoi(argv[0]) - 1].reg_output = atoi(argv[2]);
    printf("register output %s: %d\r\n", argv[0], st_alarm[atoi(argv[0]) - 1].reg_output);
  }
  else if (strcmp(argv[1], "set_point") == 0)
  {
    st_alarm[atoi(argv[0]) - 1].reg_setpoint = atoi(argv[2]);
    printf("register setpoint %s: %d\r\n", argv[0], st_alarm[atoi(argv[0]) - 1].reg_setpoint);
  }
  else if (strcmp(argv[1], "alarm") == 0)
  {
    st_alarm[atoi(argv[0]) - 1].alarm_type = atoi(argv[2]);
    printf("register setpoint %s: %d\r\n", argv[0], st_alarm[atoi(argv[0]) - 1].alarm_type);
  }
else if (strcmp(argv[0], "help") == 0)
  {
  help_alarm();
  }
}


void help_alarm()
{
  printf("\r\nSetting Alarm\r\n----------------------------------------------------------------------");
  printf(" \r\n setting data input :set_alarm [nomer alarm] [opsi] [nilai]\r\n");
  printf("  [opsi] : status, sumber, setpoint, alarm\r\n");
  printf(" setting status:\r\n");
  printf("   -misal : set_alarm 1 status 1                   [NB 1 aktif 0 mati]\r\n");
  printf(" setting sumber:\r\n");
  printf("   -misal : set_alarm 1 sumber 1005                [NB register untuk sumber alarm]\r\n");
  printf(" setting output:\r\n");
  printf("   -misal : set_alarm 1 output 1024                [NB register untuk relay alarm]\r\n");
  printf(" setting setpoint:\r\n");
  printf("   -misal : set_alarm 1 set_point 1020             [NB register untuk set_point alarm]\r\n");
  printf(" setting alarm type:\r\n");
  printf("   -misal : set_alarm 1 alarm 1                    [NB 1 : LL, 2 : L, 3 : H : 4 : HH ]\r\n");
  printf(" setting alarm \r\n");
	printf("   -misal : set_alarm 2 alarm 1005;1020;1024;3;1   [NB sumber;set_point;output;level alarm;status\r\n");
  printf("------------------------------------------------------------------------------------------- -\r\n\r\n");
}

void cek_alarm(int argc, char *argv[])
{
  (void)argc;
  (void)argv;
  printf("-----------------------------------------------------------------|\r\n");
  printf(" no | Input | Set_point | Output  | Alarm Level | Status | \r\n");
  printf("-----------------------------------------------------------------|\r\n");
  for (unsigned char i = 0; i < JML_ALARM; i++)
  {
    printf(" %d  | %d  |   %d    |   %d  |      %d      | %s   |\r\n", i + 1, st_alarm[i].regsumber, st_alarm[i].reg_setpoint, st_alarm[i].reg_output, st_alarm[i].alarm_type, (st_alarm[i].status ? "Aktif" : "Mati"));
  }
  printf("-----------------------------------------------------------------|\r\n");

  printf("NB Type Alarm 1 = LL, 2 = L, 3 = H, 4 = HH\r\n\r\n");
}


void default_alarm()
{
  for (unsigned char i = 0; i < JML_ALARM; i++)
  {
    memset(&st_alarm[i], '\0', sizeof(st_alarm[i]));
  }
  printf("    -> Setting default alarm\r\n");
}