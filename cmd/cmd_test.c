#include <ch.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cmd_common.h"
#inc lude "cmd_sumber.h"
#include "app_shell.h"
#include "VG.h"
#include "flash.h"
#include "cmd_test.h" 
struct t_tes *tes=0x2007C000;

void set_tes(int argc, char *argv[]) {
    
    if(strcmp(argv[0],"default")==0 ){
        default_tes();
        printf("masuk default");
    }
     if(strcmp(argv[0],"flash")==0 ){
        flash_tes();
        printf("masuk flash");
    }
    if(strcmp(argv[0],"copy")==0 ){
        copy_flash();
        printf("masuk copy");
    }
    if(strcmp(argv[0],"reset")==0 ){
        printf("reset bang");
        NVIC_SystemReset();
    }
    
}
void cek_tes(int argc, char *argv[]) {
printf (" NO |   ID   |      Nama     |   Nilai   | Nilaiint \r\n");
    for(char i=0;i<10;i++){
        for(char I=0;I<10;I++){
            printf("%3d | %6d | %-12s | %11.3f | %-6d|\r\n",tes[i*JUMLAH_DATA_PERSUMBER+I].tes1,tes[i*JUMLAH_DATA_PERSUMBER+I].tes4,tes[i*JUMLAH_DATA_PERSUMBER+I].tes0,tes[i*JUMLAH_DATA_PERSUMBER+I].tes3,tes[i*JUMLAH_DATA_PERSUMBER+I].tes2);
        }
    }
#if 0
    // flashRead(Flash_Sumber, tes, sizeof(*tes));
    printf("ini %s",tes->tes0);
    printf("ini %d",tes->tes1);
    printf("ini %d",tes->tes2);
    printf("ini %f",tes->tes3);
    printf("ini %d",tes->tes4);
#endif  

}
void default_tes(){
    for(char i=0;i<10;i++){
        for(char I=0;I<10;I++){
            memset( tes[i*JUMLAH_DATA_PERSUMBER+I].tes0,'\0', sizeof( tes[i*JUMLAH_DATA_PERSUMBER+I].tes0));
            sprintf(tes[i*JUMLAH_DATA_PERSUMBER+I].tes0,"data_%d",i*JUMLAH_DATA_PERSUMBER+I+1);
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes1=i*JUMLAH_DATA_PERSUMBER+I+1;
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes2=i*JUMLAH_DATA_PERSUMBER+I+1;
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes4=i*JUMLAH_DATA_PERSUMBER+I+1+1000;
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes3=(i*JUMLAH_DATA_PERSUMBER+I+1+1000/I)+0.56;

        }
    }

#if 0   
    memset( tes->tes0,'\0', sizeof( tes->tes0));
	sprintf( tes->tes0, "%s",BOARD_SN);
    tes->tes1=4321;
    tes->tes2=5674;
    tes->tes3=75.34;
    tes->tes4=7321;
#endif
}
// #define aman 40
void flash_tes(){
    char J=0,err;
    char *tpk=malloc(2048);
    char t[4];
    int A=0;
    memset( tpk,'\0', 2048);
    flashSectorErase(9);
    // flashErase(Flash_Sumber,200);
    for(char i=0;i<10;i++){
        for(char I=0;I<10;I++){
            for(char L=0;L<10;L++){
                tpk[A++]=tes[i*JUMLAH_DATA_PERSUMBER+I].tes0[L];
            }
            tpk[A++]=tes[i*JUMLAH_DATA_PERSUMBER+I].tes1;
            tpk[A++]=tes[i*JUMLAH_DATA_PERSUMBER+I].tes2;
            memcpy(&t,&tes[i*JUMLAH_DATA_PERSUMBER+I].tes3,4);
            tpk[A++]=t[3];
            tpk[A++]=t[2];
            tpk[A++]=t[1];
            tpk[A++]=t[0];
            memcpy(&t,&tes[i*JUMLAH_DATA_PERSUMBER+I].tes4,4);
            tpk[A++]=t[3];
            tpk[A++]=t[2];
            tpk[A++]=t[1];
            tpk[A++]=t[0];
            // if(A==40){
           
        
            
            // }
            
        }
    }
    // J=0;
    // for(int i=0;i<200;i++){
    //     printf("%d ",tpk[i]);
    //     J++;
    //     if(J==20){
    //         printf("\r\n");
    //         J=0;
    //     }

    // }
    flashWrite(Flash_Sumber, tpk, 2048);
    err=flashCompare(Flash_Sumber, tpk, 2048);
    printf("cek %d",err);
    // A=0;
    // J++;
    chThdSleepMilliseconds(1000);
     for(int i=0;i<2048;i++){
                printf("%d ",tpk[i]);
            J++;
            if(J==20){
                printf("\r\n");
                J=0;
            }
     }
     
	// flashRead(Flash_Sumber, buffer, sizeof(buffer));
    
    free(tpk);

#if 0  
    char *tpk=malloc(36);
    char t[4];
    char A=0;
    memset( tpk,'\0', 36);
    for(char i=0;i<20;i++){
        tpk[A++]=tes->tes0[i];
    }
    memcpy(&t,&tes->tes1,4);
    tpk[A++]=t[3];
    tpk[A++]=t[2];
    tpk[A++]=t[1];
    tpk[A++]=t[0];
    memcpy(&t,&tes->tes2,4);
    tpk[A++]=t[3];
    tpk[A++]=t[2];
    tpk[A++]=t[1];
    tpk[A++]=t[0];
    memcpy(&t,&tes->tes3,4);
    tpk[A++]=t[3];
    tpk[A++]=t[2];
    tpk[A++]=t[1];
    tpk[A++]=t[0];
    memcpy(&t,&tes->tes4,4);
    tpk[A++]=t[3];
    tpk[A++]=t[2];
    tpk[A++]=t[1];
    tpk[A++]=t[0];
    flashErase(Flash_Sumber,36);
    flashWrite(Flash_Sumber, tpk, 36);
    flashCompare(Flash_Sumber, tpk, 36);
	// flashRead(Flash_Sumber, buffer, sizeof(buffer));
    free(tpk);
#endif
}
void copy_flash(){
    char *cp=malloc(2048);
    int A=0;
    int tp=0;
    int *f;
    float *c;    

    // for(char i=0;i<1;i++){
    //     for(char I=0;I<10;I++){
            memset( cp,'\0', 2048);
            flashRead(Flash_Sumber, cp, 2048);
    for(char i=0;i<10;i++){
        for(char I=0;I<10;I++){
            for(char L=0;L<10;L++){
                tes[i*JUMLAH_DATA_PERSUMBER+I].tes0[L]=cp[A++];
            }
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes1=cp[A++];
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes2=cp[A++];
            tp=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
            c=(float*)&tp;
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes3=*c;
            tp=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
            f=(int*)&tp;
            tes[i*JUMLAH_DATA_PERSUMBER+I].tes4=*f;
        }
    }
            char J=0;
            for(int i=0;i<2048;i++){
                printf("%d ",cp[i]);
                J++;
                if(J==20){
                    printf("\r\n");
                    J=0;
                }
            }
            
        // }   
    // }
    free(cp);
#if 0
    char *cp=malloc(36);
    char A=0;
    int tp=0;
    int *f;
    float *c;
    printf("%x",&cp[0]);
    memset( cp,'\0', 36);
    flashRead(Flash_Sumber, cp, 36);
    for(char i=0;i<20;i++){
        tes->tes0[i]=cp[A++];
    }
    
    tp=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
    f=(int*)&tp;
    tes->tes1=*f;
    tp=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
    f=(int*)&tp;
    tes->tes2=*f;
    tp=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
    c=(float*)&tp;
    tes->tes3=*c;
    tp=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
    f=(int*)&tp;
    tes->tes4=*f;
    // tes.tes2=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
    // tes.tes3=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);
    // tes.tes4=((cp[A++] & 0xFF)<<24) | ((cp[A++]& 0xFF)<<16) | ((cp[A++] & 0xFF)<<8) | (cp[A++] & 0xFF);

    for(char i=0;i<36;i++){
        printf("%d \r\n",cp[i]);
    }
    free(cp);
#endif
}