#ifndef __CMD_RTC_H__
#define __CMD_RTC_H__


void cek_time(void);
void set_time(int argc, char *argv[]);
void cek_uptime(void);
void cek_restart(int argc, char *argv[]);
void restart(void);


#endif
