#include "hal.h"
#include "ch.h"
#include "cmd.h"
#include "chprintf.h"
#include <string.h>
// #include "rt_test_root.h"
// #include "oslib_test_root.h"



 void cmd_mem(int argc, char *argv[]) {
  size_t n, total, largest;
  n = chHeapStatus(NULL, &total, &largest);
  printf("core free memory : %u bytes\r\n", chCoreGetStatusX());
  printf("heap fragments   : %u\r\n", n);
  printf("heap free total  : %u bytes\r\n", total);
  printf("heap free largest: %u bytes\r\n", largest);
}

 void cmd_threads(int argc, char *argv[]) {
  static const char *states[] = {CH_STATE_NAMES};
  thread_t *tp;

  printf("core stklimit    stack     addr refs prio     state         name\r\n");
  tp = chRegFirstThread();
  do {
    core_id_t core_id;
        uint32_t stklimit = (uint32_t)tp->wabase;
    printf("%4lu %08lx %08lx %08lx %4lu %4lu %9s %12s\r\n",
             core_id,
             stklimit,
             (uint32_t)tp->ctx.sp,
             (uint32_t)tp,
             (uint32_t)tp->refs - 1,
             (uint32_t)tp->hdr.pqueue.prio,
             states[tp->state],
             tp->name == NULL ? "" : tp->name);
    tp = chRegNextThread(tp);
  } while (tp != NULL);
}