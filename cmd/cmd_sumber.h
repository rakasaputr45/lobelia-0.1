
#ifndef __CMD_SUMBER_H_
#define __CMD_SUMBER_H_



#include <ch.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cmd_common.h"
#include "app_shell.h"
#include "VG.h"
// #include "appconf.h"



//static
void cmd_sumber_help(void);
//static
void default_sumber(void);
void cmd_cek_sumber(void);
void cmd_set_sumber(int argc, char *argv[]);
void cmd_sumber_help2(void);
int flash_sumber1(void);
void copy_sumber(void);
void default_sumris(int i);

// typedef struct _t_sumber {
// 	char nama[20];
// 	// char alamat;		// untuk alamat modbus Power meter atau stack board (jika ada) //
// 	// unsigned int IP;	// klo sumber berupa modul monita
// 	// char modul;		// khusus modbus, jenis modul 0: PM, 1: KTA, 2:????, dst
// 	// char stack;		// jika modul berisi BANYAK_SUMBER : adc, pm, dll
// 	char status;		// tidak aktif, timeout, dll
// 	// char tipe;		// 0:native modbus 1:hart
// 	// unsigned int jmlReg;
// 	// unsigned char idSrc;
// 	// unsigned int RegSrc;
// 	// unsigned int RegDest;
// 	char cmd[80];
// 	// char validForm;
// 	// char ket[32];
// } st_sumber_t;

#endif
