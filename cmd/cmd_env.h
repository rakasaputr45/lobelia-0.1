#ifndef __CMD_ENV_H__
#define __CMD_ENV_H__

#include <ch.h>
#include "hal.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "appconf.h"
#include "portconf.h"
#include "VG.h"
#include "chversion.h"
#include "mcuconf.h"
#include "core_cm4.h"

void init_environment(void);
void set_united(void);
void united_to_env(void);
void set_env(int argc, char *argv[]);
void cek_env(void);
void default_env(void);
void help_env(void);
void terminate_thd();
void on();


// void copy_flashenv(void);
// void flash_environ(void);
// int ip_acc(void);
// #define LWIP_IPADDR(p)                      IP4_ADDR(p,st_ethernet.IP0, st_ethernet.IP1, st_ethernet.IP2, st_ethernet.IP3)

#endif
