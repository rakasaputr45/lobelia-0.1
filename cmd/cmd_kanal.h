#ifndef __CMD_KANAL_H_
#define __CMD_KANAL_H_

#include <ch.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "app_shell.h"
#include "cmd.h"
#include "VG.h"

void set_kanal(int argc, char *argv[]);
void cek_kanal(int argc);
void default_kanal(void);
void default_cekkan(int i);
void help_kanal(void);
#endif