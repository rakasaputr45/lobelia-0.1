
ifeq ($(USE_SMART_BUILD),yes)
	CMDCONF := $(strip $(shell cat $(CONFDIR)/cmd.h | egrep -e "\#define"))


	# List of all the board related files.
	CMDSRC = cmd_common.c

	# Required include directories
	CMDINC = $(CMD)

	ifneq ($(findstring CMD_USE_ENV TRUE,$(CMDCONF)),)
		CMDSRC += $(CMD)/cmd_env.c 
	endif
		CMDSRC += $(CMD)/cmd_data.c 
		CMDSRC +=  $(CMD)/cmd_kanal.c 
		CMDSRC +=  $(CMD)/cmd_sumber.c 
	    CMDSRC +=  $(CMD)/cmd_rtc.c     
	    CMDSRC +=  $(CMD)/cmd_modem.c    
	    CMDSRC +=  $(CMD)/cmd_formula.c   
		CMDSRC +=  $(CMD)/cmd_cb.c   
		CMDSRC +=  $(CMD)/cmd_alarm.c     
		CMDSRC +=  $(CMD)/cmd_flash.c     
else
	CMDSRC += $(CMD)/cmd_cb.c			\
			  $(CMD)/cmd_shell.c		\
		  	  $(CMD)/cmd_sumber.c       \
		  	  $(CMD)/cmd_rtc.c          \
			  $(CMD)/cmd_test.c
endif
