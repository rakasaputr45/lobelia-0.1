
#include "cmd.h"
#include "flash.h"

// #include "sd_card.h"
extern uint8_t data_relay;

#if MONITA_DAHLIA
extern struct t_kanal *st_kanal;
#endif
#if MONITA_AMELIA | MONITA_LOBELIA
extern struct t_adc st_kadc[];
#endif
extern struct t_data data[];

void set_flash(int argc, char *argv[]) {
    // chSysLockFromISR();
    if      (strcmp(argv[0], "sdc_restore") == 0)     SDCReadKonfig();
    else if (strcmp(argv[0], "sdc_backup") == 0)  { remove_file ("/file_setting.txt");  sdcard_write_config(); }
    else if (strcmp(argv[0], "write") == 0)        flash_write_config();
    else if (strcmp(argv[0], "read") == 0)    flash_read_config();
    else if (strcmp(argv[0], "erase") == 0)  {erase_emb_flash(FLASH_USER_START_ADDR, FLASH_USER_END_ADDR);   NVIC_SystemReset();}
    else    printf("Salah Menulis Command Line\r\n");
}

char flash_all(){
    flash_write_config();
    return 0;
}

