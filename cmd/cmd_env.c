#include "cmd.h"

#define  modul_testing 
#define HAL_VERSION                  "7.0.1"

#ifdef modul_testing
#define SN_modul "PKP-100-101-010"
#else
#define SN_modul "PKP-100-100-101"
#endif


unsigned char            panjangJWT = 179,
                         panjangOnlimo = 149,
                         panjangEthernet = 14;
struct t_env             st_env;
struct t_environment     st_environment;
struct t_ethernet 		 st_ethernet;
struct t_monita_protokol st_monita_protokol;
struct t_MQTT_protokol   st_MQTT_protokol ;
struct t_onlimo_protokol st_onlimo_protokol;
// struct t_jwt_protokol 	 st_jwt_protokol;
struct t_modbus_master   st_modbus_master;
struct t_modbusS         st_modbusS ;
struct t_memory          st_memory ;
struct t_flag            st_flag ;


uint8_t env_united[800];
int env_united_ln = 0;

void init_environment(void)
{
	memset(st_environment.v_firmware, '\0', 20);
	memset(st_environment.v_hardware, '\0', 20);
	memset(st_environment.v_chibi, '\0', 25);
	memset(st_environment.time_build, '\0', 25);
	sprintf(st_environment.v_firmware, "%s", FIRMWARE);
	sprintf(st_environment.v_hardware, "%s", HARDWARE);
	sprintf(st_environment.v_chibi, "v%s %s", CH_VERSION, CH_VERSION_NICKNAME);
	sprintf(st_environment.time_build, "%s %s", __DATE__, __TIME__);
	// set_united();
}


void set_united()
{

	int a=0;
	memset(env_united, '\0', sizeof(env_united));
	// memcpy(&env_united, &st_env, sizeof(st_env));
	for (size_t i = 0; i < sizeof(st_env.nama_board); i++)
	{
		env_united[a++]=st_env.nama_board[i];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_env.SN); i++)
	{
		env_united[a++]=st_env.SN[i];
		/* code */
	}
	
	for (size_t i = 0; i < sizeof(st_env.SNH); i++)
	{
		env_united[a++]=st_env.SNH[i];
		/* code */
	}
	env_united[a++]=st_env.ky1;
	env_united[a++]=st_env.ky2;
	//--------------------------------------------------------------------------------------
	for (size_t i = 0; i < sizeof(st_environment.v_firmware); i++)
	{
		env_united[a++]=st_environment.v_firmware[i];
				printf("%c", env_united[a]);

		/* code */
	}
	for (size_t i = 0; i < sizeof(st_environment.v_hardware); i++)
	{
		env_united[a++]=st_environment.v_hardware[i];
				printf("%c", env_united[a]);

		/* code */
	}
	for (size_t i = 0; i < sizeof(st_environment.v_chibi); i++)
	{
		env_united[a++]=st_environment.v_chibi[i];
				printf("%c", env_united[a]);

		/* code */
	}
	for (size_t i = 0; i < sizeof(st_environment.time_build); i++)
	{
		env_united[a++]=st_environment.time_build[i];
				printf("%c", env_united[a]);

		/* code */
	}
	//------------------------------------------------------------------
	env_united[a++] = st_ethernet.MAC0;
	env_united[a++] = st_ethernet.MAC1;
	env_united[a++] = st_ethernet.MAC2;
	env_united[a++] = st_ethernet.MAC3;
	env_united[a++] = st_ethernet.MAC4;
	env_united[a++] = st_ethernet.MAC5;
	env_united[a++] = st_ethernet.IP0 ;
	env_united[a++] = st_ethernet.IP1 ;
	env_united[a++] = st_ethernet.IP2 ;
	env_united[a++] = st_ethernet.IP3 ;
	env_united[a++] = st_ethernet.GW0 ;
	env_united[a++] = st_ethernet.GW1 ;
	env_united[a++] = st_ethernet.GW2 ;	
	env_united[a++] = st_ethernet.GW3 ;
	//--------------------------------------------------------------------------------------
	for (size_t i = 0; i < sizeof(st_monita_protokol.domain); i++)
	{
		env_united[a++]=st_monita_protokol.domain[i];
	}
	memcpy(&env_united[a], &st_monita_protokol.port, sizeof(st_monita_protokol.port));
	a=a+4;
	memcpy(&env_united[a], &st_monita_protokol.interval, sizeof(st_monita_protokol.interval));
	a=a+4;
	env_united[a++] = st_monita_protokol.status ;
	env_united[a++] = st_modbusS.almtSlave ;
	memcpy(&env_united[a], &st_modbusS.baud_slave, sizeof(st_modbusS.baud_slave));
	a=a+4;
	memcpy(&env_united[a], &st_modbus_master.baud, sizeof(st_modbus_master.baud));
	a=a+4;
	// for (size_t i = 0; i < sizeof(st_jwt_protokol.domain); i++)
	// {
		// env_united[a++]=st_jwt_protokol.domain[i];
	// 	/* code */
	// }
	a=a+50;
	// memcpy(&env_united[a], &st_jwt_protokol.port, sizeof(st_jwt_protokol.port));
	a=a+4;
	// memcpy(&env_united[a], &st_jwt_protokol.interval, sizeof(st_jwt_protokol.interval));
	a=a+4;
	// env_united[a++] = st_jwt_protokol.status ;
	a=a+1;

	// for (size_t i = 0; i < sizeof(st_jwt_protokol.get); i++)
	// {
	// 	env_united[a++]=st_jwt_protokol.get[i];
	// 	/* code */
	// }
	a=a+30;
	// for (size_t i = 0; i < sizeof(st_jwt_protokol.post); i++)
	// {
	// 	env_united[a++]=st_jwt_protokol.post[i];
	// 	/* code */
	// }
	a=a+30;
	// for (size_t i = 0; i < sizeof(st_jwt_protokol.uid); i++)
	// {
	// 	env_united[a++]=st_jwt_protokol.uid[i];
	// 	/* code */
	// }
	a=a+30;

	for (size_t i = 0; i < sizeof(st_onlimo_protokol.domain); i++)
	{
		env_united[a++]=st_onlimo_protokol.domain[i];
		/* code */
	}
	memcpy(&env_united[a], &st_onlimo_protokol.port, sizeof(st_onlimo_protokol.port));
	a=a+4;
	memcpy(&env_united[a], &st_onlimo_protokol.interval, sizeof(st_onlimo_protokol.interval));
	a=a+4;
	env_united[a++] = st_onlimo_protokol.status ;
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.IDStasiun); i++)
	{
		env_united[a++]=st_onlimo_protokol.IDStasiun[i];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.apikey); i++)
	{
		env_united[a++]=st_onlimo_protokol.apikey[i];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.apisecret); i++)
	{
		env_united[a++]=st_onlimo_protokol.apisecret[i];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.apiurl); i++)
	{
		env_united[a++]=st_onlimo_protokol.apiurl[i];
		/* code */
	}
	env_united_ln  = sizeof(st_env);
	env_united_ln += sizeof(st_environment);
	env_united_ln += sizeof(st_ethernet);
	env_united_ln += sizeof(st_monita_protokol);
	env_united_ln += sizeof(st_modbusS.baud_slave);
	env_united_ln += sizeof(st_modbus_master);
	env_united_ln += 152; // jwt leng
	env_united_ln += sizeof(st_onlimo_protokol);
}

void united_to_env(void)
{
	int a=0;
	// memset(env_united, '\0', sizeof(env_united));
	// memcpy(&env_united, &st_env, sizeof(st_env));
	for (size_t i = 0; i < sizeof(st_env.nama_board); i++)
	{
		st_env.nama_board[i]=env_united[a++];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_env.SN); i++)
	{
		st_env.SN[i]=env_united[a++];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_env.SNH); i++)
	{
		st_env.SNH[i]=env_united[a++];
		/* code */
	}
	st_env.ky1=env_united[a++];
	st_env.ky2=env_united[a++];
	//--------------------------------------------------------------------------------------
	for (size_t i = 0; i < sizeof(st_environment.v_firmware); i++)
	{
		a++;
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_environment.v_hardware); i++)
	{
		a++;
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_environment.v_chibi); i++)
	{
		// env_united[a++]=st_environment.v_chibi[i];
		a++;
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_environment.time_build); i++)
	{
		// env_united[a++]=st_environment.time_build[i];
		a++;
		/* code */
	}
	//------------------------------------------------------------------
	  st_ethernet.MAC0=env_united[a++];
	  st_ethernet.MAC1=env_united[a++];
	  st_ethernet.MAC2=env_united[a++];
	  st_ethernet.MAC3=env_united[a++];
	  st_ethernet.MAC4=env_united[a++];
	  st_ethernet.MAC5=env_united[a++];
	  st_ethernet.IP0 =env_united[a++];
	  st_ethernet.IP1 =env_united[a++];
	  st_ethernet.IP2 =env_united[a++];
	  st_ethernet.IP3 =env_united[a++];
	  st_ethernet.GW0 =env_united[a++];
	  st_ethernet.GW1 =env_united[a++];
	  st_ethernet.GW2 =env_united[a++];	
	  st_ethernet.GW3 =env_united[a++];
	//--------------------------------------------------------------------------------------
	for (size_t i = 0; i < sizeof(st_monita_protokol.domain); i++)
	{
		st_monita_protokol.domain[i]=env_united[a++];
		/* code */
	}
	a=a+4;
	memcpy(&st_monita_protokol.port, &env_united[a], sizeof(st_monita_protokol.port));
	a=a+4;
	memcpy(&st_monita_protokol.interval, &env_united[a], sizeof(st_monita_protokol.interval));
	a=a+4;
	st_monita_protokol.status = env_united[a++] ;
	st_modbusS.almtSlave        = env_united[a++] ;
	memcpy(&st_modbusS.baud_slave, &env_united[a], sizeof(st_modbusS.baud_slave));
	a=a+4;
	memcpy(&st_modbus_master.baud, &env_united[a], sizeof(st_modbus_master.baud));
	a=a+4;
	// for (size_t i = 0; i < sizeof(st_jwt_protokol.domain); i++)
	// {
	// 	st_jwt_protokol.domain[i]=env_united[a++];
	// 	/* code */
	// }
	int jwt_domain = 50;
	a = jwt_domain + a;
	// memcpy(&st_jwt_protokol.port, &env_united[a], sizeof(st_jwt_protokol.port));
	a=a+4;
	// memcpy(&st_jwt_protokol.interval, &env_united[a], sizeof(st_jwt_protokol.interval));
	a=a+4;
	a=a+1;

	// st_jwt_protokol.status=env_united[a++] ;
	// for (size_t i = 0; i < sizeof(st_jwt_protokol.get); i++)
	// {
		// st_jwt_protokol.get[i]=env_united[a++];
	// 	/* code */
	// }
	a = a + 30;
	// for (size_t i = 0; i < sizeof(st_jwt_protokol.post); i++)
	// {
	// 	st_jwt_protokol.post[i]=env_united[a++];
	// 	/* code */
	// }
	a = a + 30;
	// for (size_t i = 0; i < sizeof(st_jwt_protokol.uid); i++)
	// {
	// 	st_jwt_protokol.uid[i]=env_united[a++];
	// 	/* code */
	// }
	a = a + 30;
	// for (size_t i = 0; i < sizeof(st_onlimo_protokol.domain); i++)
	// {
	// 	st_onlimo_protokol.domain[i]=env_united[a++];
	// 	/* code */
	// }
	a = a + 50;
	// memcpy(&st_onlimo_protokol.port, &env_united[a], sizeof(st_onlimo_protokol.port));
	a=a+4;
	// memcpy(&st_onlimo_protokol.interval, &env_united[a], sizeof(st_onlimo_protokol.interval));
	a=a+4;
	st_onlimo_protokol.status =env_united[a++];
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.IDStasiun); i++)
	{
		st_onlimo_protokol.IDStasiun[i]=env_united[a++];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.apikey); i++)
	{
		st_onlimo_protokol.apikey[i]=env_united[a++];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.apisecret); i++)
	{
		st_onlimo_protokol.apisecret[i]=env_united[a++];
		/* code */
	}
	for (size_t i = 0; i < sizeof(st_onlimo_protokol.apiurl); i++)
	{
		st_onlimo_protokol.apiurl[i]=env_united[a++];
		/* code */
	}
	// uint8_t env_united[1000];
	// memcpy(&st_env, &env_united, sizeof(st_env));
	// env_united_ln  = sizeof(st_env);
	//memcpy(&st_environment, &env_united[env_united_ln], sizeof(st_environment));
	// env_united_ln += sizeof(st_environment);
	// memcpy(&st_ethernet, &env_united[env_united_ln], sizeof(st_ethernet));
	// env_united_ln += sizeof(st_ethernet);
	// memcpy(&st_monita_protokol, &env_united[env_united_ln], sizeof(st_monita_protokol));
	// env_united_ln += sizeof(st_monita_protokol);
	// memcpy(&st_modbus_slave, &env_united[env_united_ln], sizeof(st_modbus_slave));
	// env_united_ln += sizeof(st_modbus_slave);
	// memcpy(&st_modbus_master, &env_united[env_united_ln], sizeof(st_modbus_master));
	// env_united_ln += sizeof(st_modbus_master);
	// memcpy(&st_jwt_protokol, &env_united[env_united_ln], sizeof(st_jwt_protokol));
	// env_united_ln += sizeof(st_jwt_protokol);
	// memcpy(&st_onlimo_protokol, &env_united[env_united_ln], sizeof(st_onlimo_protokol));
	// env_united_ln += sizeof(st_onlimo_protokol);
}
void default_env()
{
    memset(st_env.SN, '\0', sizeof(st_env.SN));
	memset(st_env.nama_board, '\0', sizeof(st_env.nama_board));
	memset(st_env.SNH, '\0', sizeof(st_env.SNH));
	
	sprintf(st_env.SN, "%s", SN_modul);
	sprintf(st_env.SNH, "%s", HARDWARE);
	sprintf(st_env.nama_board, "%s", HARDWARE);

	memset(st_monita_protokol.domain, '\0', sizeof(st_monita_protokol.domain));
	sprintf(st_monita_protokol.domain, "alpha.daunbiru.com");
	st_monita_protokol.port = 1336;
	st_monita_protokol.status = 2;
	st_monita_protokol.interval = 10;
	st_flag.debug_gsm = 0;

    memset(st_MQTT_protokol.domain, '\0', sizeof(st_MQTT_protokol.domain));
    memset(st_MQTT_protokol.topik, '\0', sizeof(st_MQTT_protokol.topik));
	sprintf(st_MQTT_protokol.domain, "192.168.1.250");
	sprintf(st_MQTT_protokol.topik, "test");
	st_MQTT_protokol.port = 1883;
	st_MQTT_protokol.status = 0;
	st_MQTT_protokol.interval = 1;

	st_modbusS.almtSlave = 0;
	st_modbusS.baud_slave = 0;
	st_modbus_master.baud = 9600;

	st_memory.file = 1;
	st_memory.interval = 10;

	st_flag.RF = 0;
	st_flag.debug1 = 0;
	st_flag.debug_data = 0;
	printf("default env\r\n");
}
#if defined(STM32_HSECLK) || defined(STM32_HSE_BYPASS)
#define FREQ_CLK_MCU (1.0 * STM32_HSECLK * STM32_PLLN_VALUE / STM32_PLLM_VALUE / STM32_PLLP_VALUE / 1000000)
#else
#define FREQ_CLK_MCU (1.0 * STM32_HSICLK * STM32_PLLN_VALUE / STM32_PLLM_VALUE / STM32_PLLP_VALUE / 1000000)
#endif
void cek_env()
{
#if 1
    printf("*****************************************************\r\n");
	printf("Environment\r\n");
	printf("  Nama Board      : %s\r\n", st_env.nama_board);
	printf("  No Seri         : %s\r\n", st_env.SN);
	printf("  Nama Bluetooh   : %s\r\n", DeviceName);
	printf("  Versi Firmware  : %s\r\n", st_environment.v_firmware);
	printf("  Versi Hardware  : %s\r\n", st_environment.v_hardware);
	printf("  Clock CPU       : %d MHz\r\n", (int)FREQ_CLK_MCU);
	printf("  ChibiOS Versi   : v%s %s\r\n", CH_VERSION, CH_VERSION_NICKNAME);
	printf("  HAL Versi       : v%s%s, RTOS ChibiOS/RT v%s\r\n", CH_HAL_VERSION, CH_HAL_STABLE ? " [Stable]" : "", CH_KERNEL_VERSION);
	printf("  Compiler        : %s\r\n", PORT_COMPILER_NAME);
	printf("  Time Build      : %s %s\r\n", __DATE__, __TIME__);
	printf("\r\n*****************************************************");

	printf("\r\nKomunikasi GSM\r\n");
	printf("1 Protokol MQTT status %s\r\n", (st_MQTT_protokol.status ? "Aktif" : "Mati"));
	printf("  Domain          : %s\r\n", st_MQTT_protokol.domain);
	printf("  Port            : %d\r\n", st_MQTT_protokol.port);
	printf("  Topik           : %s\r\n", st_MQTT_protokol.topik);
	printf("  Interval        : %d menit\r\n", st_MQTT_protokol.interval);
	printf("2 Protokol monita status %s\r\n", (st_monita_protokol.status ? "Aktif" : "Mati"));
	printf("  Domain          : %s\r\n", st_monita_protokol.domain);
	printf("  Port            : %d\r\n", st_monita_protokol.port);
	printf("  Interval        : %d detik\r\n", st_monita_protokol.interval);
	printf("*****************************************************\r\n\r\n");
	printf("Komunikasi Serial\r\n");
// 	printf("1 Konfigurasi Modbus RTU Slave\r\n");
// 	printf("  ID Device       : %d\r\n", st_modbusS.almtSlave);
// 	printf("  Baud            : %d\r\n", st_modbusS.baud_slave);
// 	   switch(st_modbusS.parity) {
//       case 0 :
// 	printf("  Parity          : None\r\n");
//          break;
//       case 1 :
// 	printf("  Parity          : Odd\r\n");
//          break;
//       case 2 :
// 	printf("  Parity          : Even\r\n");
//          break;
//       default :
// 	printf("  Parity          : Tidak Cocok\r\n");
//    }
	printf("1 Konfigurasi Modbus RTU Master\r\n");
	printf("  BaudRate        : %d\r\n", st_modbus_master.baud);
	// if(st_rn2xx3.status==1){
	// 	printf("  	3. Konfigurasi LORA status AKTIF LORAWAN\r\n");
	// 	printf("\t\tDevice EUI               : %s\r\n", st_rn2xx3.deveui);
	// 	printf("\t\tDevice Address           : %s\r\n", st_rn2xx3.devaddr);
	// 	printf("\t\tApplication EUI          : %s\r\n", st_rn2xx3.appeui);
	// 	printf("\t\tNetwork session key      : %s\r\n", st_rn2xx3.nwkskey);
	// 	printf("\t\tApplication session key  : %s\r\n", st_rn2xx3.appskey);
	// 	printf("\t\tClass                    : %s\r\n", st_rn2xx3.clas);
	// 	printf("\t\tData rate                : %d\r\n", st_rn2xx3.datarate);
	// 	printf("\t\toutput power             : %d dBm\r\n", st_rn2xx3.pwridx);
	// }else{
	// 	printf("  	3. Konfigurasi LORA status MATI\r\n");
	// }
printf("\r\n*****************************************************\r\n\r\n");
printf("  Simpan File     : %s\r\n", (st_memory.file?"Aktif":"Mati"));
printf("  interval simpan : %d detik\r\n", st_memory.interval);
printf("\r\n*****************************************************\r\n\r\n");

#if MONITA_DAHLIA
	printf("  Flash dibaca : %s\r\n", (st_flag.RF ? "Aktif" : "Mati"));
#endif
#if MONITA_AMELIA
// printf("  status LoRa : %s\r\n", (st_lora.lora?"Aktif":"Mati"));
// printf("  nilai ref Vrms : %f\r\n",st_flag.vr);
// printf("  Ch triger : %d\r\n", st_flag.RF);
#endif
#endif
	// printf(" Cek_env bos\r\n");
}

void help_env(){
	printf("*********************************************************************************************\r\n\r\n");
	
	printf("1 Setting Env\r\n");
	printf("  Command   : set_env [paramater] [Nilai]\r\n");
	printf("  parameter : nama,SN\r\n");
	printf("  contoh    : a. set_env nama LOBELIA\r\n\r\n");

    printf("*********************************************************************************************\r\n\r\n");
	
	printf("setting Protokol komunikasi TCP\r\n\r\n");
	printf("1 Setting Monita\r\n\r\n");
	printf("  Command   : set_env [paramater] [Nilai] \r\n");
	printf("  paramater : server_monita,port_monita,inter_monita,status_monita \r\n");
	printf("  contoh    : a. set_env server_monita alpha.daunbiru.com\r\n");
	printf("              b. set_env port_monita 1336\r\n");
	printf("              c. set_env interval_monita 60 (dalam detik)\r\n");
	printf("              d. set_env status_monita 1  [NB 1:aktif, 0:mati]\r\n\r\n");

	printf("2 Setting MQTT \r\n\r\n");
	printf("  Command   : set_env [paramater] [Nilai] \r\n");
	printf("  paramater : server_mqtt,port_monita,inter_mqtt,status_mqtt,user,password \r\n");
	printf("  contoh    : a. set_env server_mqtt alpha.daunbiru.com\r\n");
	printf("              b. set_env port_mqtt 1883\r\n");
	printf("              c. set_env interval_mqtt 60 (dalam detik)\r\n");
	printf("              d. set_env status_mqtt 1\r\n");
	printf("              e. set_env user monita\r\n");
	printf("              f. set_env password rinjani\r\n\r\n");
    printf("*********************************************************************************************\r\n\r\n");
	printf("Seting Serial komunikasi\r\n\r\n");
	printf("1 Setting Modbus RTU Master \r\n\r\n");
	printf("  Command   : set_env [paramater] [Nilai]\r\n");
	printf("  parameter : baud_master\r\n");
	printf("  contoh    : a. set_env baud_master 9600\r\n\r\n");
    printf("*********************************************************************************************\r\n\r\n");
	printf("1 Seting Serial Memory\r\n\r\n");
	printf("  Command   : set_env [paramater] [Nilai]\r\n");
	printf("  parameter : file,interval_simpan\r\n");
	printf("  contoh    : a. set_env inter_simpan 60 (detik)\r\n");
	printf("              b. set_env file 1       [keterangan : 0 : mematikan penyimpanan Sd card  1 = mengaktifkan penyimpanan SD card]\r\n\r\n");
    printf("*********************************************************************************************\r\n");
	}
void set_env(int argc, char *argv[])
{

	switch (argc)
	{
	case (OFFSET + 0):
		// cmd_sumber_help();
		printf("Parameter Argumen kurang.");
		break;
	case (OFFSET + 1):
		if ((strcmp(argv[OFFSET + 0], "help") == 0) || (strcmp(argv[OFFSET + 0], "-h") == 0))
		{
			// cmd_sumber_help();
			help_env();
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "default") == 0)
		{
			// default_sumber();
			default_env();
			printf(">>> Data ENV : default\r\n");
			break;
		}

		printf("Parameter tidak sesuai\r\n\r\n");
		break;
#if 1
	case (OFFSET + 2):

		if (strcmp(argv[OFFSET + 0], "nama") == 0)
		{
			sprintf(st_env.nama_board, argv[1]);
			printf("\r\nNama Board : %s\r\n", argv[1]);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "SN") == 0)
		{
			sprintf(st_env.SN, argv[1]);
			printf("\r\nSN : %s\r\n", argv[1]);
			break;
		}
		else if (strcmp(argv[OFFSET+0],"baud_master")==0)  {
			st_modbus_master.baud=atoi(argv[OFFSET+1]);
			printf("\r\nSet baudrate modbus master : %d\r\n", st_modbus_master.baud);
                break;
			}
		else if (strcmp(argv[OFFSET + 0], "interval_simpan") == 0)
		{
			st_memory.interval = atoi(argv[OFFSET + 1]);
			printf("\r\ninterval simpan : %d\r\n", st_memory.interval);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "file") == 0)
		{
			st_memory.file = atoi(argv[OFFSET + 1]);
			printf("\r\nmemory file : %d\r\n", st_memory.file);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "debug1") == 0)        
		{
			st_flag.debug1 = atoi(argv[OFFSET + 1]);
			printf("\r\ndebug1 : %d\r\n", st_flag.debug1);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "debug2") == 0)
		{
			st_flag.debug2 = atoi(argv[OFFSET + 1]);
			printf("\r\ndebug2 : %d\r\n", st_flag.debug2);
			break;
		}
	    else if (strcmp(argv[OFFSET + 0], "debug_data") == 0)
		{
			st_flag.debug_data = atoi(argv[OFFSET + 1]);
			printf("\r\ndebug data : %d\r\n", st_flag.debug_data);
			break;
		}

		else if (strcmp(argv[OFFSET + 0], "debug_modem") == 0)
		{
			st_flag.debug_gsm = atoi(argv[OFFSET + 1]);
			printf("\r\ndebug gsm : %d\r\n", st_flag.debug_gsm);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "server_mqtt") == 0)
		{
			sprintf(st_MQTT_protokol.domain, argv[1]);

			break;
		}
		else if (strcmp(argv[OFFSET + 0], "port_mqtt") == 0)
		{
			// sprintf(st_MQTT_protokol.domain, argv[1]);
			st_MQTT_protokol.port = atoi(argv[OFFSET + 1]);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "status_mqtt") == 0)
		{
			// sprintf(st_MQTT_protokol.domain, argv[1]);
			st_MQTT_protokol.status = atoi(argv[OFFSET + 1]);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "topic_mqtt") == 0)
		{
			sprintf(st_MQTT_protokol.topik, argv[1]);

			break;
		}
		else if (strcmp(argv[OFFSET + 0], "interval_mqtt") == 0)
		{
			// sprintf(st_MQTT_protokol.domain, argv[1]);
			st_MQTT_protokol.interval = atoi(argv[OFFSET + 1]);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "server_monita") == 0)
		{
			sprintf(st_monita_protokol.domain, argv[1]);
			printf("\r\nserver monita :%s\r\n", st_monita_protokol.domain);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "port_monita") == 0)
		{
			st_monita_protokol.port = atoi(argv[OFFSET + 1]);
		    printf("\r\nport server monita : %d\r\n", st_monita_protokol.port);

			break;
		}
		else if (strcmp(argv[OFFSET + 0], "status_monita") == 0)
		{
			st_monita_protokol.status = 0;
			if (atoi(argv[OFFSET + 1]))  st_monita_protokol.status = 2;
			printf("\r\nstatus monita : %d\r\n", (int)st_monita_protokol.status);
			break;
		}
		else if (strcmp(argv[OFFSET + 0], "interval_monita") == 0)
		{
			st_monita_protokol.interval = atoi(argv[OFFSET + 1]);
			printf("\r\ninterval kirim monita : %d\r\n", st_monita_protokol.interval);
			break;
		}
		printf("Parameter tidak sesuai\r\n");
		// cmd_sumber_help();
		break;
#endif
	default:
		printf("Perintah: %d\r\n", argc);
		break;
	}
}
