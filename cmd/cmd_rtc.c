#include <ch.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cmd_common.h"

#include "app_shell.h"
#include "hal.h"
#include "portconf.h"
#include "cmd_rtc.h"
#include "rtc.h"
#include "VG.h"

// extern int system_uptime;
extern struct t_reset *st_reset;

void cek_time(void)     
{
	struct tm *t;
	unsigned int wk=0;
	printf("%ld\r\n",(uint32_t)GetTimeUnixSec());
	wk=GetTimeUnixSec();
	t = localtime ((const time_t *) &wk);
    printf("%d-%d-%d %d:%d:%d+%d\r\n", t->tm_year+1900,t->tm_mon + 1,t->tm_mday,t->tm_hour + timezone,t->tm_min,t->tm_sec, timezone);
}
void set_time(int argc, char *argv[]){
	SetTimeUnixSec(atoi(argv[0]));
	printf("parameter %d",argc);
}
void cek_uptime(){
	printf("data uptime %d",(int)system_uptime);
}
void cek_restart(int argc, char *argv[]){
	if (strcmp(argv[OFFSET+0],"atas")==0){
		printf("restart paling lama %d",st_reset->rst1);
	}
	else if (strcmp(argv[OFFSET+0],"bawah")==0){
		printf("restart paling bawah %d",st_reset->rst2);

	}
	else if (strcmp(argv[OFFSET+0],"td")==0){
		if(st_reset->a==1){
			printf("restart paling lama a=1 %d",st_reset->rst3);
		}else{
			printf("restart paling lama a=0 %d",st_reset->rst4);
		}
	}
	else if (strcmp(argv[OFFSET+0],"hapus")==0){		
		st_reset->rst1=0;
		st_reset->rst2=0;
		st_reset->rst3=0;
		st_reset->rst4=0;
		st_reset->a=0;
	}
	printf("parameter %d",argc);
}
void restart(){
	NVIC_SystemReset();
	// printf("data uptime %d",system_uptime);
}
