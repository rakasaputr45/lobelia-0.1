
#ifndef __CMD_DATA_H_
#define __CMD_DATA_H_

#include <ch.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cmd_sumber.h"
#include "VG.h"
#include "portconf.h"
#include <math.h>

static const char nama[11][20] = {
                             "  kanal_1",
                             "  kanal_2",
                             "  kanal_3",
                             "  kanal_4",
                             "     V_PV",
                             "   V_Batt",
                             "     I_PV",
                             "   I_Batt",
                             "timestamp"
                          };
static const char data_tipe[4][20] = {
                             "    float",
                             "  integer",
                             "   double",
                             "      bit"
                         };
static const char data_length[4][20] = {
                             "  16bit",
                             "  32bit",
                             "  64bit",
                             "   8bit"
                         };
                         
                         
void  cek_data(int argc, char *argv[]) ;
void  cek_data1(int argc, char *argv[]) ;
void  set_data(int argc, char *argv[]) ;
void  default_data(void);
void  data_halp(void);
void  default_dapris(int i,int j,int di);
#endif