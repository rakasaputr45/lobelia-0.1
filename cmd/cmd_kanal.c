#include "cmd.h"

struct t_adc st_kadc[JML_KANAL] __attribute__((section(".ram0")));

void set_kanal(int argc, char *argv[])
{
    #if MONITA_DAHLIA 
    if(strcmp(argv[1],"tipe")==0 && argc == 3){
        st_kanal[atoi(argv[0])-1].status=atoi(argv[2]);
        printf("kanal %s tipe %s \r\n",argv[0],argv[2]);
    }
    if(strcmp(argv[1],"status")==0 && argc == 3){
        st_kanal[atoi(argv[0])-1].status_H =atoi(argv[2]);
        printf("kanal %s = %s \r\n",argv[0],atoi(argv[2])?"Aktif":"Mati");
    }
    if(strcmp(argv[1],"status_RH")==0 && argc == 3){
        st_kanal[atoi(argv[0])-1].status_R=atoi(argv[2]);
        printf("kanal %s = %s \r\n",argv[0],atoi(argv[2])?"Aktif":"Mati");
    }
    if(strcmp(argv[1],"kalibrasi")==0 && argc == 4){
        st_kanal[atoi(argv[0])-1].m=atof(argv[2]);
        st_kanal[atoi(argv[0])-1].c=atof(argv[3]);
        printf("kanal %s m %s c %s\r\n",argv[0],argv[2],argv[3]);
    }
    #endif
    #if MONITA_AMELIA || MONITA_LOBELIA
    if(strcmp(argv[1],"tipe")==0 && argc == 3){
        st_kadc[atoi(argv[0])-1].tipe=atoi(argv[2]);
        printf("\r\n\r\nkanal %s tipe %s \r\n",argv[0],argv[2]);
    }
    if(strcmp(argv[1],"status")==0 && argc == 3){
        st_kadc[atoi(argv[0])-1].status=atoi(argv[2]);
        printf("\r\n\r\nkanal %s = %s \r\n",argv[0],atoi(argv[2])?GREEN("Aktif"):RED("Mati"));
    }
    if(strcmp(argv[1],"kalibrasi")==0 && argc == 4){
        st_kadc[atoi(argv[0])-1].m=atof(argv[2]);
        st_kadc[atoi(argv[0])-1].c=atof(argv[3]);
        printf("\r\n\r\nkanal %s m %s c %s\r\n",argv[0],argv[2],argv[3]);
    }
    #endif

    if(strcmp(argv[0],"default")==0){
        default_kanal();
        printf("\r\n\r\nKanal sudah diisi dengan default\r\n");
        // printf("masuk\r\n");
    }    
    if(strcmp(argv[0],"help")==0){
        help_kanal();
        printf("\r\n\r\nKanal Help\r\n");
        // printf("masuk\r\n");
    }
    // printf("masuk set kanal %d ",argc);
}

void cek_kanal(int argc)
{
    printf("\r\n\r\nKanal\r\n");
    printf("\r\n\r\nNo    M         C         Tipe  Status\r\n");
    printf("====  ========  ========  ====  =======\r\n");
    for (unsigned char i=0; i < JML_KANAL; i++)	{
    printf("%4d  %8.3f  %8.3f  %4d  %7s\r\n",(i+1),st_kadc[i].m,st_kadc[i].c,st_kadc[i].tipe,st_kadc[i].status?GREEN("Aktif"):RED("Mati"));
	}

}

void default_cekkan(int i)
{
    #if MONITA_DAHLIA
    st_kanal[i].m = 1;
	st_kanal[i].c = 0;
	st_kanal[i].status = 1;
    st_kanal[i].status_R = 0;
    st_kanal[i].status_H = 0;
    st_kanal[i].ky1 = 0xAA;
    st_kanal[i].ky2 = 0x55;
    #endif
    #if MONITA_AMELIA || MONITA_LOBELIA
    st_kadc[i].m = 1;
	st_kadc[i].c = 0;
	st_kadc[i].tipe = 1;
    st_kadc[i].status = 0;
    st_kadc[i].ky1 = 0xAA;
    st_kadc[i].ky2 = 0x55;
    #endif
}

void default_kanal()
{
    for (unsigned char i=0; i<(JML_KANAL); i++)	{
		st_kadc[i].m = 1;
		st_kadc[i].c = 0;
		st_kadc[i].tipe = 1;
        st_kadc[i].status = 0;
        st_kadc[i].ky1 = 0xAA;
        st_kadc[i].ky2 = 0x55;
        chThdSleepMilliseconds(10);
	}
    printf("default kanal\r\n");
}

void help_kanal()
{
    printf("----------------------------------------------------------\r\n");
    printf("Setting Kanal\r\n\r\n");
    printf(" setting formula set_kanal [no kanal] [opsi] [nilai]\r\n");
    printf(" setting example set_kanal 1 status 1\r\n");
    printf(" setting example set_kanal 1 kalibrasi 1 0\r\n");
    printf(" [opsi] : status,tipe,kalibrasi\r\n");
    printf("          tipe   : 1 : adc \r\n");
    printf("          status : 1 = aktif\r\n                   0 = mati \r\n\r\n");
    printf("----------------------------------------------------------\r\n");
}