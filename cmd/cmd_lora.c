#include <ch.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cmd_common.h"

#include "app_shell.h"
#include "hal.h"
#include "portconf.h"
// #include "cmd_rtc.h"
// #include "rtc.h"
#include "VG.h"


struct t_loraD *loraD=(uint8_t *)SRAM1_LORA+(1024*10);
struct t_loraM *loraM=(uint8_t *)SRAM1_LORA+(1024*10-125);





void cek_lora(void)     {

	printf("setting LORA \r\n\r\n");
    for (size_t i = 0; i < 5; i++)
    {
      printf("    Lora %d ID : %2d. ,Network : %3d, status: %s \r\n",(i+1),loraD[i].id,loraD[i].jaringan,loraD[i].status?"Aktif":"Mati");
    }   
    printf("-------------------------------------------\r\n\r\n");
}

void set_lora(int argc, char *argv[])     {
    
    
    // printf("masuk %d\r\n",argc);
    if(strcmp(argv[1],"id")==0 && argc == 3){
        loraD[atoi(argv[0])-1].id=atoi(argv[2]);
		printf("nilai id %x :",loraD[atoi(argv[0])-1].id);
        // printf("data %s = %s \r\n",argv[0],atoi(argv[2])?"Aktif":"Mati");
    }else if(strcmp(argv[1],"n")==0 && argc == 3){
        loraD[atoi(argv[0])-1].jaringan=atoi(argv[2]);
		printf("nilai id %x :",loraD[atoi(argv[0])-1].jaringan);
        // printf("data %s = %s \r\n",argv[0],atoi(argv[2])?"Aktif":"Mati");
    }else if(strcmp(argv[1],"status")==0 && argc == 3){
        loraD[atoi(argv[0])-1].status=atoi(argv[2]);
		printf("nilai id %x :",loraD[atoi(argv[0])-1].status?"Aktif":"Mati");
        // printf("data %s = %s \r\n",argv[0],atoi(argv[2])?"Aktif":"Mati");
    }
    else if(strcmp(argv[0],"default")==0 ){

        loradefault();
        printf("lora defult");

    }else if(strcmp(argv[1],"idm")==0 && argc == 3){
        loraM[atoi(argv[0])-1].id=atoi(argv[2]);
		printf("nilai id %x :",loraM[atoi(argv[0])-1].id);
        // printf("data %s = %s \r\n",argv[0],atoi(argv[2])?"Aktif":"Mati");
    }else if(strcmp(argv[1],"nm")==0 && argc == 3){
        loraM[atoi(argv[0])-1].jaringan=atoi(argv[2]);
		printf("nilai id %x :",loraM[atoi(argv[0])-1].jaringan);
        // printf("data %s = %s \r\n",argv[0],atoi(argv[2])?"Aktif":"Mati");
    }
    
}

void loradefault()   {
    
    
    for (size_t i = 0; i < 5; i++)
    {
        loraD[i].id=i+1;
        loraD[i].jaringan=1;
        loraD[i].status=0;
        
    }
    loraM->id=1;
    loraM->jaringan=1;
    
}