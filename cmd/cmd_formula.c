#include <hal.h>
#include <cmd_formula.h>
#include <portconf.h>
#include <VG.h>
#include <stdlib.h>

struct t_formula st_formula[JML_FORMULA];

void set_formula(int argc, char *argv[]){ 
    if(strcmp(argv[1],"status")==0 && argc == 3){ // restart saat disetting
      st_formula[atoi(argv[0])-1].status = atoi(argv[2]);
      printf("formula %s status %s\r\n",argv[0],(st_formula[atoi(argv[0])-1].status?"Aktif":"Mati"));
    }
    else if(strcmp(argv[0],"default")==0){ // OKE
        default_formula();
      printf("OKE 'default formula'\r\n");
    }else if(strcmp(argv[0],"help")==0){
      
      help_formula();
    
    }else if(strcmp(argv[1],"formula")==0){

      	sprintf(st_formula[atoi(argv[0])-1].form,"%s",argv[2]);
        printf("\r\n\r\nformula : %s\r\n",st_formula[atoi(argv[0])-1].form); 

    }else if(strcmp(argv[1],"parameter")==0){

      	sprintf(st_formula[atoi(argv[0])-1].parameter,"%s",argv[2]);
        printf("%s dan %s\r\n", argv[2], argv[3]);
        sprintf(st_formula[atoi(argv[0])-1].regsumber,"%s",argv[3]);
        printf("parameter (%s) : (%s)\r\n",st_formula[atoi(argv[0])-1].parameter,st_formula[atoi(argv[0])-1].regsumber);

    }else if(strcmp(argv[1],"sumber")==0){
      	// st_formula[atoi(argv[0])-1].regsumber = atoi(argv[2]);
        sprintf(st_formula[atoi(argv[0])-1].regsumber,"%s",argv[2]);
        printf("register sumber  : (%s)\r\n",st_formula[atoi(argv[0])-1].regsumber);

    }else if(strcmp(argv[1],"tujuan")==0){

      	st_formula[atoi(argv[0])-1].regtujuan = atoi(argv[2]);
        printf("register tujuan : %d\r\n",st_formula[atoi(argv[0])-1].regtujuan);
    } 
}


void cek_formula(int argc, char *argv[]){
  (void)argc;
  (void)argv;
  printf("----------------------------------------------------------------------------------------------------------|\r\n");
  printf(" no |    nama     |       parameter        |   sumber   |   tujuan  |          Formula           | status | \r\n");
	printf("----------------------------------------------------------------------------------------------------------|\r\n");
	for(unsigned char i=0;i<JML_FORMULA;i++)
  {
		printf(" %2d | Formula %d   | %-19s    | %9s  | %9d | %-19s        | %-6s |\r\n", i+1,i+1,st_formula[i].parameter,st_formula[i].regsumber,st_formula[i].regtujuan,st_formula[i].form,(st_formula[i].status==0)?"Mati":"Aktif");
	}
  printf("----------------------------------------------------------------------------------------------------------|\r\n");

}

void default_formula()
{
  for(unsigned char i=0;i<JML_FORMULA;i++){
  memset(&st_formula[i],'\0',sizeof(st_formula[i]));
	}  

}


void default_satu_formula(int i)
{
  memset(&st_formula[i],'\0',sizeof(st_formula[i]));
}

void help_formula(){
    printf("\r\nSETTING FORMULA\r\n******************************************************************************************************");
    printf(" \r\n setting data input :set_formula [nomer formula] [opsi] [nilai]\r\n");
    printf("  [opsi] : status,formula,parameter,tujuan\r\n");
    printf(" setting status:\r\n");
    printf("   -misal : set_formula 1 status 1          [NB 1 aktif 0 mati]\r\n");
    printf(" setting parameter:\r\n");
    printf("   -misal : set_formula 1 parameter a 13    [NB a untuk parameter 13 untuk sumber]\r\n");
    printf(" setting tujuan:\r\n");
    printf("   -misal : set_formula 1 tujuan 1          [NB tempat hasil formula di taruh]\r\n");
    printf(" setting formula:\r\n"); 
    printf("   -misal : set_formula 1 formula a+3/(5^4) [NB <+> penambahan <-> pengurangan </>pembagian <*>perkalian <^> pangkat]\r\n");
    printf("******************************************************************************************************\r\n\r\n");
}
