#include "cmd_sumber.h"
struct t_sumber st_sumber[JML_SUMBER_MB];

extern struct t_env st_env;
void cmd_set_sumber(int argc, char *argv[])   {
    switch (argc) {
        case (OFFSET+0) :
			cmd_sumber_help();
			printf("\r\n\r\nParameter Argumen kurang.");
			break;
        case (OFFSET+1) :
            if ((strcmp(argv[OFFSET+0],"help")==0) || (strcmp(argv[OFFSET+0],"-h")==0)) {
				cmd_sumber_help();
                break;
			}
            else if (strcmp(argv[OFFSET+0],"default")==0)    {
                default_sumber();
                printf("\r\n\r\nData sumber sudah diisi dengan default\r\n");
                break;
            }
            printf("Parameter tidak sesuai\r\n\r\n");
			break;
        case (OFFSET+3) :
            if ((strcmp(argv[OFFSET+1],"formula")==0) || (strcmp(argv[OFFSET+1],"-f")==0)) {
				char s1,s2[10],s[30],b=0;
                s1=sprintf(st_sumber[atoi(argv[0])-1].form,"%s",argv[2]);
                strcpy(s,argv[2]);
                sprintf(s2,"0");
                #if 1
                for(unsigned char i=0;i<s1;i++){
                    if(s[i]==';'){
                        memset(s2,'\0', sizeof(s2));
                        b++;
                        // printf("masuk dek");
                        sprintf(s2,"0");
                    }
                    else{
                        if(b==0){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].ID=atoi(s2);
                        }
                        else if(b==1){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].RegSrc=atoi(s2);
                        }
                        else if(b==2){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].fc=atoi(s2);
                        }
                        else if(b==3){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].jmlReg=atoi(s2);
                        }
                        else if(b==4){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].RegDest=atoi(s2);
                        }
                        else if(b==5){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].swap=atoi(s2);
                        }
                        else if(b==6){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].tipe_data=atoi(s2);
                        }
                        else if(b==7){
                            sprintf(s2,"%s%c",s2,s[i]);
                            // printf("data %d = %d\r\n",b,atoi(s2));
                            st_sumber[atoi(argv[0])-1].width=atoi(s2);
                        }
                    }
                }
                printf("\r\n\r\n%d;%d;%d;%d;%d",st_sumber[atoi(argv[0])-1].ID,st_sumber[atoi(argv[0])-1].RegSrc,st_sumber[atoi(argv[0])-1].fc,st_sumber[atoi(argv[0])-1].jmlReg,st_sumber[atoi(argv[0])-1].RegDest);
                #endif
                break;
			}
            else if (strcmp(argv[OFFSET+1],"status")==0) {
				st_sumber[atoi(argv[0])-1].status=atoi(argv[2]);
                printf("\r\n\r\nsumber %d status %s",atoi(argv[0]),(atoi(argv[2]))?GREEN("Aktif"):RED("Mati"));
                break;
			}
            else if (strcmp(argv[OFFSET+1],"mode")==0) {
				    st_sumber[atoi(argv[0])-1].mode=atoi(argv[2]);                
                break;
			}
			printf(RED("Parameter tidak sesuai\r\n"));
			cmd_sumber_help();
            break;
        default:
			break;
      }
}

void cmd_cek_sumber()
{
    printf("\r\n\r\nData Sumber\r\n\r\n");
    printf("  No        Nama  Mode     Status              Formula\r\n");
    printf("====  ==========  ====  =========  ===================\r\n");

    for ( int i = 0; i < JML_SUMBER_MB; i++)
    {
        sprintf(st_sumber[i].form, "%d;%d;%d;%d;%d;%d;%d;%d", st_sumber[i].ID, st_sumber[i].RegSrc, st_sumber[i].fc, st_sumber[i].jmlReg, st_sumber[i].RegDest, st_sumber[i].swap, st_sumber[i].tipe_data, st_sumber[i].width);
        printf("%4d  %-10s  %4d  %10s  %19s\r\n", i + 1, st_sumber[i].nama, st_sumber[i].mode, (st_sumber[i].status == 1)?GREEN("Aktif") : RED("Mati"), st_sumber[i].form);
    }
}

//static
void default_sumber(void)   {
    memset( st_sumber,'\0', sizeof(st_sumber));
    for (int i = 0; i<JML_SUMBER_MB; i++) 
    {
        sprintf(st_sumber[i].nama, "sumber_%d", i+1);
        st_sumber[i].status = 0;
        st_sumber[i].mode=0;
        st_sumber[i].port=502;
        st_sumber[i].jmlReg=0;
        st_sumber[i].RegDest=0;
        st_sumber[i].RegSrc=0;
        st_sumber[i].swap=0;
        st_sumber[i].width=0;
        st_sumber[i].fc=3;
        st_sumber[i].ID=10;
        st_sumber[i].tipe_data=1;
        st_sumber[i].ky1=0x55;
        st_sumber[i].ky2=0xAA;
    }
        printf("sumber default\r\n");

}
void cmd_sumber_help2(void)   {
    printf("tes printf dari cmd sumber\r\n");
}
//static
void cmd_sumber_help(void)   {
    //*
    printf("\r\n\r\nPerintah untuk mengubah konfig sumber !\r\n");
	printf(" Penggunaan : set_sumber [no_sumber] [nama|IP|mode|formula|status] [nilainya]\r\n");
	printf(" 1. Contoh            : set_sumber 2 formula 10;1014;3;2;10;0;0;1\r\n");
	printf("    Format formula    : ID Slave;Reg. Sensor;Command;Length;Reg. Tujuan;Swap;Tipe Data;Lebar Data\r\n");
	printf("=======================================================================================================\r\n");
	printf("      ID Slave        : ID Slave pada modul sensor \r\n");
	printf("      Reg. Sensor     : Alamat register modul Sensor\r\n");
	printf("      Command         : [3] Read Holding Register(FC03) [4] Read Input Register(FC04) \r\n ");
	printf("      Panjang Data    : Jumlah data yang akan disedot (dalam word = per 2 byte)\r\n");
	printf("      Reg. Tujuan     : Register penyimpanan di modul Rinjani (ID data)\r\n");
	printf("      Swap            : 0: Normal  , 1: Swap Byte, 2: Swap Word, 3: Swap Byte&Word \r\n");
	printf("      Tipe Data       : 0: Float   , 1: Integer\r\n");
	printf("      Lebar Data      : 0: 16 bit  , 1: 32 bit\r\n");
    printf("2. contoh set_sumber 2 IP 192 168 1 245\r\n");
}

void default_sumris(int i){
        memset( st_sumber[i].nama,'\0', 10);
        memset( st_sumber[i].form,'\0', 30);
        sprintf(st_sumber[i].nama, "sumber_%d", i+1);
        sprintf(st_sumber[i].form, " ");
        st_sumber[i].IP0=192;
        st_sumber[i].IP1=168;
        st_sumber[i].IP2=1;
        st_sumber[i].IP3=1;
        st_sumber[i].status = 0;
        st_sumber[i].mode=0;
        st_sumber[i].port=502;
        st_sumber[i].jmlReg=0;
        st_sumber[i].RegDest=0;
        st_sumber[i].RegSrc=0;
        st_sumber[i].swap=0;
        st_sumber[i].width=0;
        st_sumber[i].fc=3;
        st_sumber[i].ID=1;
        st_sumber[i].tipe_data=1;
        st_sumber[i].ky1=0x55;
        st_sumber[i].ky2=0xAA;
}
