#ifndef __VARIABEL_GLOBAL__
#define __VARIABEL_GLOBAL__

// tipe data variabel

#include "appconf.h"
extern char dataReg[10];
extern bool flagARUSERROR;

extern char DeviceName[];
extern mutex_t mmx;
#define folderMonitaGagal      "/MONITA/" 
extern char monitaRealtimeFolder[];
#define voltageBatteryMax     13.0
#define maxBufferRead         1250

#define BufferSend maxBufferRead * 2
extern bool BND;
extern bool flagCheckDataGagal, flagkirim_monita, flagSimpanData; 

extern char bufHeaderGagal[];
extern char bufdataGagal[];

extern char sendBuffer[BufferSend];

extern short LENGheader,lastHour, LENGdata;

extern short fileGagal;
extern char  bacaSD[21];
extern unsigned int waktu;

extern short DATAGAGALlen;
extern int   timezone;
extern short GAGAL;
extern char  ambilDATA; 
extern bool  kirimGagal;
extern bool  noFile;

extern short HOUR, MINUTE, SEC,YEAR, MONTH, DAY;  // simpan variabel waktu

extern  unsigned char  DATAhex[230], REGIShex[110], SNhex[40];
extern unsigned int EPOCHhex;

extern unsigned char FLAGTIME;

extern struct tm st_tm;
typedef unsigned char UCHAR;
typedef char CHAR;
// uint8_t tcp_conn;
extern int pwm_flag;
extern float system_uptime;

extern thread_t *thdb;
extern semaphore_t sem;
extern int MinDutyCycle;

extern bool predict, predict1, predict2, predict3, predict4, predict5; 

extern float maxVcharge, minVcharge,voltageBattery,pwm_cal;

extern bool
    output_Mode,           //   USER PARAMETER - 0 = PSU MODE, 1 = Charger Mode  
    bypassEnable,           // SYSTEM PARAMETER - 
    IUV, // SYSTEM PARAMETER - Input Under Voltage
    IOV, // SYSTEM PARAMETER - Input Over Voltage
    IOC, // SYSTEM PARAMETER - Input Over Current
    OUV, // SYSTEM PARAMETER - Output Under Voltage
    OOV, // SYSTEM PARAMETER - Output Over Voltage
    OOC, // SYSTEM PARAMETER - Output Over Current
    REC,           // SYSTEM PARAMETER - 
    buckEnable,           // SYSTEM PARAMETER - Buck Enable Status
    overvoltagebatt;


extern short
    ERR,           // SYSTEM PARAMETER - 
    PWM,           // SYSTEM PARAMETER -
    PPWM;           // SYSTEM PARAMETER -

extern short n;
extern float
    C,
    M,
    temp_Iin,
    temp_Iout,
    temp_Vin,
    temp_Vout,
    hitung_Vout,
    hitung_Vin;


extern float
powerInput,      // SYSTEM PARAMETER - Input power (solar power) in Watts
powerInputPrev,      // SYSTEM PARAMETER - Previously stored input power variable for MPPT algorithm (Watts)
powerOutput,      // SYSTEM PARAMETER - Output power (battery or charing power in Watts)
voltageInput,      // SYSTEM PARAMETER - Input voltage (solar voltage)
voltageInputPrev,      // SYSTEM PARAMETER - Previously stored input voltage variable for MPPT algorithm
voltageOutput,      // SYSTEM PARAMETER - Input voltage (battery voltage)
currentInput,      // SYSTEM PARAMETER - Output power (battery or charing voltage)
currentOutput,      // SYSTEM PARAMETER - Output current (battery or charing current in Amperes)
voltageDropout;     //  CALIB PARAMETER - Buck regulator's dropout voltage (DOV is present due to Max Duty Cycle Limit)



extern float
    MaxCurrentIn,
    MaxCurrentOut;
typedef unsigned char UCHAR;
typedef char    CHAR;

typedef uint16_t USHORT;
typedef int16_t SHORT;

typedef uint32_t ULONG;
typedef int32_t LONG;

#define RAM1BASE (uint8_t *)SRAM1_BASE
#define sektor_1 (__IO uint8_t *)BKPSRAM_BASE
#define RAM2BASE (uint8_t *)SRAM2_BASE
#define JUMLAH_SUMBER 13
#define JUMLAH_DATA_PERSUMBER 2 
#define JML_REGISTER (JUMLAH_SUMBER * JUMLAH_DATA_PERSUMBER * 2)
#define JML_TITIK_DATA (JUMLAH_SUMBER * JUMLAH_DATA_PERSUMBER)
#define JML_SUMBER_MB 10
#define JML_KANAL 4
#define JML_DATA_INPUT 9

#define JML_FRMA 0

#define JML_ETHERNET 1
#define JML_MQTT_PROT 1
#define JML_MONITA_PROT 1
#define JML_MODBUS_SLAVE 1
#define JML_FORMULA 4
#define JML_ALARM 16

#define RAM_DATA (RAM2BASE + (1024 * 10))

#define ALAMAT_ENV_S 0x000000
#define ALAMAT_ENV_F 0xfff
#define ALAMAT_KANAL_S 0x001000
#define ALAMAT_SUMBER_F 0x004fff
#define ALAMAT_DATA_S 0x005000
#define ALAMAT_DATA_F 0x008fff

extern float data_fL[8][50], data_f[9];
extern int *timer_con;
extern uint16_t data_reg[JML_REGISTER];

    struct t_onlimo_protokol {
	char domain[50];
	int port;
	int interval;
	char status;
	char IDStasiun[40];
	char apikey[40];
	char apisecret[40];	
	char apiurl[40];
    };

    struct  t_jwt_protokol {
	char domain[50];
	int port;
	int interval;
	char status;
	char get[30];
	char post[30];
	char uid[30];
    };

struct  t_env  {
	char nama_board[32];
	char SN[30];
    char SNH[30];
    char ky1;
    char ky2;
};

struct   t_environment  {
	char v_firmware[20];
	char v_hardware[20];
	char v_chibi[25];
	char time_build[25];
};


struct  t_ethernet {
	unsigned char MAC0;
	unsigned char MAC1;
	unsigned char MAC2;
	unsigned char MAC3;
	unsigned char MAC4;
	unsigned char MAC5;
	unsigned char IP0;
	unsigned char IP1;
	unsigned char IP2;
	unsigned char IP3;
	unsigned char GW0;
	unsigned char GW1;
	unsigned char GW2;
	unsigned char GW3;
};

struct  t_sumber 
{
char nama[10];
char mode;		    
unsigned char IP0;  
unsigned char IP1;
unsigned char IP2;
unsigned char IP3;
char swap;			   
char status;		   
char ID;			   
unsigned int jmlReg;   
unsigned char fc;	   
unsigned int RegSrc;   
unsigned int RegDest;  
char form[30];
char tipe_data;   
char width;		 
int port;
char ky1;
char ky2;
};

struct t_flag
{
	char RF;
	char debug1;
	char debug2;
	float vr;
	char fgl0;
	char debug_data;
	bool debug_gsm;
}; // 83byte


struct t_buffmb
{
	char in[50];
	unsigned char out[50];
	char tampung[256];
};

struct t_memory
{
	char file;
	char interval;
};


struct t_modbus_master
{
	int baud;
};

struct __attribute__((__packed__)) t_modbusS
{
	char almtSlave;
	int baud_slave;
	// char parity; // tambah parity
};

struct  t_triger_com {
	char rt0;
	char rt1;
	char rt2;
	char mqtt;
}; //24

struct t_monita_protokol
{
	char domain[50];
	int  port;
	int  interval;
	char status;
}; // jumah 59

struct t_MQTT_protokol
{
	char domain     [50];
	int  port;
	int  interval;
	char status;
    char user       [30];
    char password   [30];
    char topik      [30];
};

struct  t_reset
{
	int rst1;
	int rst2;
	int rst3;
	int rst4;
	char a;
}; // 33

struct  __attribute__((__packed__)) t_data
{
int id;          
char satuan[6];
char nama[10];
char status;
char status_simpan;
float L;
float LL;
float H;
float HH;
int8_t err_L;
int32_t err_H;
char tipe;
char panjang;            
};
 
#if MONITA_DAHLIA
struct t_kanal
{
	float m;
	float c;
	char status;   // tipe GPIO
	char status_R; // running hours
	char status_H; // status
	char ky1;
	char ky2;

}; // 13byte
#define jml_struct_kanal 13

#elif MONITA_AMELIA
struct t_adc
{
	float m;
	float c;
	char tipe;	 // tipe GPIO
	char status; // status adc
	char ky1;
	char ky2;
};
#define jml_struct_kanal 12
#elif MONITA_LOBELIA
struct t_adc
{
	float m;
	float c;
	char tipe;	 // tipe GPIO
	char status; // status adc
	char ky1;
	char ky2;
};
#define jml_struct_kanal 12
#endif

struct __attribute__((__packed__))  t_mbrt
{
	char id_slave;
	int fc;
	uint16_t addr_sumber;
	uint16_t data[4];
	char flage;
}; // 10

#if APP_USE_GPIO
struct  t_gpio
{
	unsigned char flag;
	int flag_RPM[JML_KANAL];
};
#endif

struct  t_alarm  {
	short alarm_type;
	short reg_setpoint;
	short regsumber;
	short reg_output;
	short status;
	short ack_button;
	short ack_status;
	short ack_delay;
	char  type;
	char form[30];
};

struct  t_ack  {
	int alarm_off_delay;
	char status;
	int delay;
	short reg;
	float data;
	char alarm_off_status;
};

struct  t_formula {
	char form[80];     // setring    
	char status;       // 
	char parameter[15];     // setring 
	char regsumber[12];     // setring
	int regtujuan;	        // setring
};

#define jml_struct_formula 89
#define jml_struct_alarm 9

#define sektor_11 0x080E0000
#define ALAMAT_ENV sektor_1
#define ALAMAT_RESET (sektor_1 + (1024 * 1) + 640)
#define ALAMAT_MEM (sektor_1 + 512)
#define ALAMAT_TIMER (sektor_1 + 612)
#define ALAMAT_DATA RAM_DATA
#define ALAMAT_KANAL (RAM_DATA + (1024 * 1))
#define Flash_Alamat sektor_11
#define sektor1_ram1 SRAM1_BASE
#define SRAM1_LORA sektor1_ram1
#define ALAMAT_WEB (RAM1BASE + (1024 * 90))		// 13000 = 13 byte
#define ALAMAT_WEB_bf (RAM1BASE + (1024 * 104)) // 13000 = 13 byte

#endif