

#ifndef _PORTCONF_H_
#define _PORTCONF_H_


#include "hal.h"
#include "ch.h"
#include <stdio.h>
#include <string.h>
#include "chprintf.h"

#define OFFSET              0               // argumen dari shell chibios mulai dari 0

#define PORTAB_GPT1 GPTD6

/* Shell Port Configuratiom */
#define SERIAL_PORT_SHELL           SD2
#define SERIAL_BITRATE_SHELL        115200

static SerialConfig SHELL_cfg = {
    SERIAL_BITRATE_SHELL,
    0,
    0,
    0,
};

static SerialConfig MODEM_config = {
    115200,
    0,
    USART_CR2_STOP1_BITS,
    0,
};

static SerialConfig BL_config = {
    115200,
    0,
    0,
    0,
};

#define printfModem(fmt, ...) \
    chprintf((BaseSequentialStream*)&SD1, fmt, ##__VA_ARGS__)

#define printf(fmt, ...)  \
    chprintf((BaseSequentialStream*)&SD2, fmt, ##__VA_ARGS__)

#define printfBT(fmt, ...)	\
    chprintf((BaseSequentialStream*)&LPSD1, fmt, ##__VA_ARGS__)
    
#define SERIAL_PORT_BT           SD1
#define SERIAL_BITRATE_BT        115200

#endif

#define PORTAB_SDCD1                SDCD1
#if defined(STM32_SDC_SDIO_UNALIGNED_SUPPORT)
#define PORTAB_UNALIGNED_SUPPORT        STM32_SDC_SDIO_UNALIGNED_SUPPORT

#elif defined(STM32_SDC_SDMMC_UNALIGNED_SUPPORT)
#define PORTAB_UNALIGNED_SUPPORT        STM32_SDC_SDMMC_UNALIGNED_SUPPORT

#else
#error "unexpected definitions"
#endif
#define SDC_BURST_SIZE      16


// /*
//  * SDIO configuration.
//  */
// static const SDCConfig sdccfg = {
//   SDC_MODE_4BIT
// };