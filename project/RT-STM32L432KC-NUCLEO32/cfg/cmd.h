#ifndef __CMD_H__
#define __CMD_H__

#define CMD_USE_SHELL TRUE
#define CMD_USE_ENV TRUE
#define CMD_USE_DATA TRUE
#define CMD_USE_SUMBER TRUE
#define CMD_USE_KANAL TRUE
#define CMD_USE_FORMULA TRUE
#define CMD_USE_ALARM TRUE
#define CMD_USE_RTC TRUE
#define CMD_USE_TEST FALSE // TRUE
#define CMD_USE_MEM FALSE
#define CMD_USE_FLASH TRUE
#define CMD_USE_LORA FALSE

#if (CMD_USE_SHELL == TRUE) || defined(__DOXYGEN__)
// #include "cmd_shell.h"
#endif

#include "colored_shell.h"

#if (CMD_USE_SUMBER == TRUE) || defined(__DOXYGEN__)
#include "cmd_sumber.h"
#endif

#if (CMD_USE_DATA == TRUE) || defined(__DOXYGEN__)
#include "cmd_data.h"
#endif

#if (CMD_USE_ENV == TRUE) || defined(__DOXYGEN__)
#include "cmd_env.h"
#endif

#if (CMD_USE_FLASH == TRUE) || defined(__DOXYGEN__)
#include "cmd_flash.h"
#endif

#if (CMD_USE_RTC == TRUE) || defined(__DOXYGEN__)
#include "cmd_rtc.h"
#endif
#if (CMD_USE_KANAL == TRUE) || defined(__DOXYGEN__)
#include "cmd_kanal.h"
#include "cmd_modem.h"
#endif

#if (CMD_USE_MEM == TRUE) || defined(__DOXYGEN__)
#include "cmd_mem.h"
#endif

#if (CMD_USE_LORA == TRUE) || defined(__DOXYGEN__)
#include "cmd_lora.h"
#endif

#if (CMD_USE_FORMULA == TRUE) || defined(__DOXYGEN__)
#include "cmd_formula.h"
#endif

#if (CMD_USE_ALARM == TRUE) || defined(__DOXYGEN__)
#include "cmd_alarm.h"
#endif

#include "cmd_cb.h"

#endif
