#ifndef __APPCONF_H__
#define __APPCONF_H__

//
// Konfig APP seharusnya ada di setiap folder projek
//   23 Nov 2017, Afrendy Bayu


#define APP_USE_BLINK       TRUE
#define APP_USE_SHELL       TRUE       // MANDATORY TRUE
#define APP_USE_GSM         TRUE
#define APP_USE_ADC         TRUE
#define APP_USE_PWM         TRUE
#define APP_USE_CHARGING    TRUE
#define APP_USE_MPPT        TRUE
#define APP_USE_USB         TRUE
#define APP_USE_BLUETOOTH   TRUE
#define APP_USE_MB_MASTER   TRUE
#define APP_USE_FORMULA     FALSE
#define APP_USE_ALARM       FALSE
#define APP_USE_SDC         TRUE



#define APP_USE_LWIP        FALSE
#define APP_USE_GPS         FALSE
#define APP_USE_AD7708      FALSE       // belum selesai
#define APP_USE_MAC         FALSE
#define APP_USE_WEB			FALSE
#define APP_USE_RTC         TRUE
#define APP_USE_MBTCP       FALSE

#define APP_USE_WDG			TRUE
// #define APP_USE_MB_SLAVE    TRUE
#define APP_USE_HART        FALSE
#define APP_USE_GPIO        TRUE  // rpm, rh. onoff, pulse signal diskret
#define APP_USE_SRAM		FALSE
#define APP_USE_DUMMY       FALSE
#define APP_USE_FLASH		TRUE
#define APP_USE_MBCRC       FALSE
#define APP_USE_PUBLISHER   TRUE
#define APP_USE_LORA        TRUE
#define APP_USE_RELAY       FALSE



/*=============================
             modul
===============================*/

#define MONITA_DAHLIA        FALSE
#define MONITA_AMELIA        FALSE
#define MONITA_LOBELIA       TRUE


enum KANAL_DATA {
    JML_KANAL_DISKRET_DATA = 0,
    JML_KANAL_ANALOG_DATA,
    JML_KANAL_RELAY_DATA,
    JML_KANAL_FORMULA_CONFIG,
    JML_KANAL_FORMULA_DATA
};

#define BOARD_NAME        "LOBELIA 1.1.0"
#define STRINGIZE2(s) #s
#define STRINGIZE(s) STRINGIZE2(s)
#define VERSION_MAJOR               1
#define VERSION_MINOR               1
#define VERSION_REVISION            1
#define VERSION_BUILD               0
#define FIRMWARE_VERSI                  STRINGIZE(VERSION_MAJOR)    \
                                    "." STRINGIZE(VERSION_MINOR)    \
                                    "." STRINGIZE(VERSION_REVISION) \
                                    "." STRINGIZE(VERSION_BUILD)    \
// setting user modul
#define FIRMWARE            "LOBEL "FIRMWARE_VERSI
#define HARDWARE            "LOBELIA 0.1"

#define SHELL_PROMPT_STR     "\e[1;31m@LOBEL:/#\e[0m "
#endif

// 1;34m