#include <stdio.h>
#include <float.h>
#include <string.h>
#include <stdlib.h>
#include "ch.h"
#include "hal.h"
#include <stdio.h>
#include "chprintf.h"
#include "app.h"
#include "app_sophia.h"

#include "cmd.h"
#include "VG.h"
#include "shell.h"
#include "portconf.h"
#include "ff.h"
#include "File_Handling.h"
#include "rtc.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "chprintf.h"
#include "flash.h"

mutex_t mmx;

struct t_mbrt st_mbrt;
int main(void)
{

  buck_Enable(0);
  halInit();
  chSysInit();
  chMtxObjectInit(&mmx);

init_environment();
#if APP_USE_SHELL
  thdShell();
#endif
#if APP_USE_BLINK
  thdblink(); // threads #1 --> blink.c
#endif
sdcard_init();
#if APP_USE_SDC
thdsdc_int();
#endif 
#if APP_USE_ADC
  thdADC();
#endif
#if APP_USE_MPPT /*sistem modbus master*/
  thdPWM();
  thdMPPT();
#endif

#if APP_USE_ALARM
thdAlarm();
#endif

#if APP_USE_FORMULA
thdFormula();
#endif

#if APP_USE_USB
thd_Sophia();
#endif

#if APP_USE_GSM
  thdgsm();
#endif

 
#if APP_USE_MB_MASTER   /* Sistem Modbus Master*/
  modbus_init();
  thdMBMaster();
#endif
// wdgStart(&WDGD1, &wdgcfg);

  while (BENER)
  {
   chThdSleepMilliseconds(1000);
  //  wdgReset(&WDGD1);
  }
}

